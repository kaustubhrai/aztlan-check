import re


tzStrings = [
    {"label":"(GMT-12:00) International Date Line West","value":"Etc/GMT+12"},
    {"label":"(GMT-11:00) Midway Island, Samoa","value":"Pacific/Midway"},
    {"label":"(GMT-10:00) Hawaii","value":"Pacific/Honolulu"},
    {"label":"(GMT-09:00) Alaska","value":"US/Alaska"},
    {"label":"(GMT-08:00) Pacific Time (US & Canada)","value":"America/Los_Angeles"},
    {"label":"(GMT-08:00) Tijuana, Baja California","value":"America/Tijuana"},
    {"label":"(GMT-07:00) Arizona","value":"US/Arizona"},
    {"label":"(GMT-07:00) Chihuahua, La Paz, Mazatlan","value":"America/Chihuahua"},
    {"label":"(GMT-07:00) Mountain Time (US & Canada)","value":"US/Mountain"},
    {"label":"(GMT-06:00) Central America","value":"America/Managua"},
    {"label":"(GMT-06:00) Central Time (US & Canada)","value":"US/Central"},
    {"label":"(GMT-06:00) Guadalajara, Mexico City, Monterrey","value":"America/Mexico_City"},
    {"label":"(GMT-06:00) Saskatchewan","value":"Canada/Saskatchewan"},
    {"label":"(GMT-05:00) Bogota, Lima, Quito, Rio Branco","value":"America/Bogota"},
    {"label":"(GMT-05:00) Eastern Time (US & Canada)","value":"US/Eastern"},
    {"label":"(GMT-05:00) Indiana (East)","value":"US/East-Indiana"},
    {"label":"(GMT-04:00) Atlantic Time (Canada)","value":"Canada/Atlantic"},
    {"label":"(GMT-04:00) Caracas, La Paz","value":"America/Caracas"},
    {"label":"(GMT-04:00) Manaus","value":"America/Manaus"},
    {"label":"(GMT-04:00) Santiago","value":"America/Santiago"},
    {"label":"(GMT-03:30) Newfoundland","value":"Canada/Newfoundland"},
    {"label":"(GMT-03:00) Brasilia","value":"America/Sao_Paulo"},
    {"label":"(GMT-03:00) Buenos Aires, Georgetown","value":"America/Argentina/Buenos_Aires"},
    {"label":"(GMT-03:00) Greenland","value":"America/Godthab"},
    {"label":"(GMT-03:00) Montevideo","value":"America/Montevideo"},
    {"label":"(GMT-02:00) Mid-Atlantic","value":"America/Noronha"},
    {"label":"(GMT-01:00) Cape Verde Is.","value":"Atlantic/Cape_Verde"},
    {"label":"(GMT-01:00) Azores","value":"Atlantic/Azores"},
    {"label":"(GMT+00:00) Casablanca, Monrovia, Reykjavik","value":"Africa/Casablanca"},
    {"label":"(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London","value":"Etc/Greenwich"},
    {"label":"(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna","value":"Europe/Amsterdam"},
    {"label":"(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague","value":"Europe/Belgrade"},
    {"label":"(GMT+01:00) Brussels, Copenhagen, Madrid, Paris","value":"Europe/Brussels"},
    {"label":"(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb","value":"Europe/Sarajevo"},
    {"label":"(GMT+01:00) West Central Africa","value":"Africa/Lagos"},
    {"label":"(GMT+02:00) Amman","value":"Asia/Amman"},
    {"label":"(GMT+02:00) Athens, Bucharest, Istanbul","value":"Europe/Athens"},
    {"label":"(GMT+02:00) Beirut","value":"Asia/Beirut"},
    {"label":"(GMT+02:00) Cairo","value":"Africa/Cairo"},
    {"label":"(GMT+02:00) Harare, Pretoria","value":"Africa/Harare"},
    {"label":"(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius","value":"Europe/Helsinki"},
    {"label":"(GMT+02:00) Jerusalem","value":"Asia/Jerusalem"},
    {"label":"(GMT+02:00) Minsk","value":"Europe/Minsk"},
    {"label":"(GMT+02:00) Windhoek","value":"Africa/Windhoek"},
    {"label":"(GMT+03:00) Kuwait, Riyadh, Baghdad","value":"Asia/Kuwait"},
    {"label":"(GMT+03:00) Moscow, St. Petersburg, Volgograd","value":"Europe/Moscow"},
    {"label":"(GMT+03:00) Nairobi","value":"Africa/Nairobi"},
    {"label":"(GMT+03:00) Tbilisi","value":"Asia/Tbilisi"},
    {"label":"(GMT+03:30) Tehran","value":"Asia/Tehran"},
    {"label":"(GMT+04:00) Abu Dhabi, Muscat","value":"Asia/Muscat"},
    {"label":"(GMT+04:00) Baku","value":"Asia/Baku"},
    {"label":"(GMT+04:00) Yerevan","value":"Asia/Yerevan"},
    {"label":"(GMT+04:30) Kabul","value":"Asia/Kabul"},
    {"label":"(GMT+05:00) Yekaterinburg","value":"Asia/Yekaterinburg"},
    {"label":"(GMT+05:00) Islamabad, Karachi, Tashkent","value":"Asia/Karachi"},
    {"label":"(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi","value":"Asia/Calcutta"},
    {"label":"(GMT+05:30) Sri Jayawardenapura","value":"Asia/Calcutta"},
    {"label":"(GMT+05:45) Kathmandu","value":"Asia/Katmandu"},
    {"label":"(GMT+06:00) Almaty, Novosibirsk","value":"Asia/Almaty"},
    {"label":"(GMT+06:00) Astana, Dhaka","value":"Asia/Dhaka"},
    {"label":"(GMT+06:30) Yangon (Rangoon)","value":"Asia/Rangoon"},
    {"label":"(GMT+07:00) Bangkok, Hanoi, Jakarta","value":"Asia/Bangkok"},
    {"label":"(GMT+07:00) Krasnoyarsk","value":"Asia/Krasnoyarsk"},
    {"label":"(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi","value":"Asia/Hong_Kong"},
    {"label":"(GMT+08:00) Kuala Lumpur, Singapore","value":"Asia/Kuala_Lumpur"},
    {"label":"(GMT+08:00) Irkutsk, Ulaan Bataar","value":"Asia/Irkutsk"},
    {"label":"(GMT+08:00) Perth","value":"Australia/Perth"},
    {"label":"(GMT+08:00) Taipei","value":"Asia/Taipei"},
    {"label":"(GMT+09:00) Osaka, Sapporo, Tokyo","value":"Asia/Tokyo"},
    {"label":"(GMT+09:00) Seoul","value":"Asia/Seoul"},
    {"label":"(GMT+09:00) Yakutsk","value":"Asia/Yakutsk"},
    {"label":"(GMT+09:30) Adelaide","value":"Australia/Adelaide"},
    {"label":"(GMT+09:30) Darwin","value":"Australia/Darwin"},
    {"label":"(GMT+10:00) Brisbane","value":"Australia/Brisbane"},
    {"label":"(GMT+10:00) Canberra, Melbourne, Sydney","value":"Australia/Canberra"},
    {"label":"(GMT+10:00) Hobart","value":"Australia/Hobart"},
    {"label":"(GMT+10:00) Guam, Port Moresby","value":"Pacific/Guam"},
    {"label":"(GMT+10:00) Vladivostok","value":"Asia/Vladivostok"},
    {"label":"(GMT+11:00) Magadan, Solomon Is., New Caledonia","value":"Asia/Magadan"},
    {"label":"(GMT+12:00) Auckland, Wellington","value":"Pacific/Auckland"},
    {"label":"(GMT+12:00) Fiji, Kamchatka, Marshall Is.","value":"Pacific/Fiji"},
    {"label":"(GMT+13:00) Nuku'alofa","value":"Pacific/Tongatapu"}
]

tzInts = [
    {"label":"(GMT-12:00) International Date Line West","value":"-12"},
    {"label":"(GMT-11:00) Midway Island, Samoa","value":"-11"},
    {"label":"(GMT-10:00) Hawaii","value":"-10"},
    {"label":"(GMT-09:00) Alaska","value":"-9"},
    {"label":"(GMT-08:00) Pacific Time (US & Canada)","value":"-8"},
    {"label":"(GMT-08:00) Tijuana, Baja California","value":"-8"},
    {"label":"(GMT-07:00) Arizona","value":"-7"},
    {"label":"(GMT-07:00) Chihuahua, La Paz, Mazatlan","value":"-7"},
    {"label":"(GMT-07:00) Mountain Time (US & Canada)","value":"-7"},
    {"label":"(GMT-06:00) Central America","value":"-6"},
    {"label":"(GMT-06:00) Central Time (US & Canada)","value":"-6"},
    {"label":"(GMT-05:00) Bogota, Lima, Quito, Rio Branco","value":"-5"},
    {"label":"(GMT-05:00) Eastern Time (US & Canada)","value":"-5"},
    {"label":"(GMT-05:00) Indiana (East)","value":"-5"},
    {"label":"(GMT-04:00) Atlantic Time (Canada)","value":"-4"},
    {"label":"(GMT-04:00) Caracas, La Paz","value":"-4"},
    {"label":"(GMT-04:00) Manaus","value":"-4"},
    {"label":"(GMT-04:00) Santiago","value":"-4"},
    {"label":"(GMT-03:30) Newfoundland","value":"-3.5"},
    {"label":"(GMT-03:00) Brasilia","value":"-3"},
    {"label":"(GMT-03:00) Buenos Aires, Georgetown","value":"-3"},
    {"label":"(GMT-03:00) Greenland","value":"-3"},
    {"label":"(GMT-03:00) Montevideo","value":"-3"},
    {"label":"(GMT-02:00) Mid-Atlantic","value":"-2"},
    {"label":"(GMT-01:00) Cape Verde Is.","value":"-1"},
    {"label":"(GMT-01:00) Azores","value":"-1"},
    {"label":"(GMT+00:00) Casablanca, Monrovia, Reykjavik","value":"0"},
    {"label":"(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London","value":"0"},
    {"label":"(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna","value":"1"},
    {"label":"(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague","value":"1"},
    {"label":"(GMT+01:00) Brussels, Copenhagen, Madrid, Paris","value":"1"},
    {"label":"(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb","value":"1"},
    {"label":"(GMT+01:00) West Central Africa","value":"1"},
    {"label":"(GMT+02:00) Amman","value":"2"},
    {"label":"(GMT+02:00) Athens, Bucharest, Istanbul","value":"2"},
    {"label":"(GMT+02:00) Beirut","value":"2"},
    {"label":"(GMT+02:00) Cairo","value":"2"},
    {"label":"(GMT+02:00) Harare, Pretoria","value":"2"},
    {"label":"(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius","value":"2"},
    {"label":"(GMT+02:00) Jerusalem","value":"2"},
    {"label":"(GMT+02:00) Minsk","value":"2"},
    {"label":"(GMT+02:00) Windhoek","value":"2"},
    {"label":"(GMT+03:00) Kuwait, Riyadh, Baghdad","value":"3"},
    {"label":"(GMT+03:00) Moscow, St. Petersburg, Volgograd","value":"3"},
    {"label":"(GMT+03:00) Nairobi","value":"3"},
    {"label":"(GMT+03:00) Tbilisi","value":"3"},
    {"label":"(GMT+03:30) Tehran","value":"3.5"},
    {"label":"(GMT+04:00) Abu Dhabi, Muscat","value":"4"},
    {"label":"(GMT+04:00) Baku","value":"4"},
    {"label":"(GMT+04:00) Yerevan","value":"4"},
    {"label":"(GMT+04:30) Kabul","value":"4.5"},
    {"label":"(GMT+05:00) Yekaterinburg","value":"5"},
    {"label":"(GMT+05:00) Islamabad, Karachi, Tashkent","value":"5"},
    {"label":"(GMT+05:30) Sri Jayawardenapura","value":"5.5"},
    {"label":"(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi","value":"5.5"},
    {"label":"(GMT+05:45) Kathmandu","value":"5.75"},
    {"label":"(GMT+06:00) Almaty, Novosibirsk","value":"6"},{"label":"(GMT+06:00) Astana, Dhaka","value":"6"},
    {"label":"(GMT+06:30) Yangon (Rangoon)","value":"6.5"},
    {"label":"(GMT+07:00) Bangkok, Hanoi, Jakarta","value":"7"},
    {"label":"(GMT+07:00) Krasnoyarsk","value":"7"},
    {"label":"(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi","value":"8"},
    {"label":"(GMT+08:00) Kuala Lumpur, Singapore","value":"8"},
    {"label":"(GMT+08:00) Irkutsk, Ulaan Bataar","value":"8"},
    {"label":"(GMT+08:00) Perth","value":"8"},
    {"label":"(GMT+08:00) Taipei","value":"8"},
    {"label":"(GMT+09:00) Osaka, Sapporo, Tokyo","value":"9"},
    {"label":"(GMT+09:00) Seoul","value":"9"},
    {"label":"(GMT+09:00) Yakutsk","value":"9"},
    {"label":"(GMT+09:30) Adelaide","value":"9.5"},
    {"label":"(GMT+09:30) Darwin","value":"9.5"},
    {"label":"(GMT+10:00) Brisbane","value":"10"},
    {"label":"(GMT+10:00) Canberra, Melbourne, Sydney","value":"10"},
    {"label":"(GMT+10:00) Hobart","value":"10"},
    {"label":"(GMT+10:00) Guam, Port Moresby","value":"10"},
    {"label":"(GMT+10:00) Vladivostok","value":"10"},
    {"label":"(GMT+11:00) Magadan, Solomon Is., New Caledonia","value":"11"},
    {"label":"(GMT+12:00) Auckland, Wellington","value":"12"},
    {"label":"(GMT+12:00) Fiji, Kamchatka, Marshall Is.","value":"12"},
    {"label":"(GMT+13:00) Nuku'alofa","value":"13"}
]


timezones = [
'(GMT-11:00) Midway Island','Pacific/Midway',
'(GMT-11:00) Samoa','Pacific/Samoa',
'(GMT-10:00) Hawaii','Pacific/Honolulu',
'(GMT-09:00) Alaska', 'US/Alaska',
'(GMT-08:00) Pacific Time (US &amp; Canada)', 'America/Los_Angeles',
'(GMT-08:00) Tijuana', 'America/Tijuana',
'(GMT-07:00) Arizona', 'US/Arizona',
'(GMT-07:00) Chihuahua', 'America/Chihuahua',
'(GMT-07:00) La Paz', 'America/Chihuahua',
'(GMT-07:00) Mazatlan', 'America/Mazatlan',
'(GMT-07:00) Mountain Time (US &amp; Canada)', 'US/Mountain',
'(GMT-06:00) Central America', 'America/Managua',
'(GMT-06:00) Central Time (US &amp; Canada)', 'US/Central',
'(GMT-06:00) Guadalajara', 'America/Mexico_City',
'(GMT-06:00) Mexico City', 'America/Mexico_City',
'(GMT-06:00) Monterrey', 'America/Monterrey',
'(GMT-06:00) Saskatchewan', 'Canada/Saskatchewan',
'(GMT-05:00) Bogota', 'America/Bogota',
'(GMT-05:00) Eastern Time (US &amp; Canada)', 'US/Eastern',
'(GMT-05:00) Indiana (East)', 'US/East-Indiana',
'(GMT-05:00) Lima', 'America/Lima',
'(GMT-05:00) Quito', 'America/Bogota',
'(GMT-04:00) Atlantic Time (Canada)', 'Canada/Atlantic',
'(GMT-04:30) Caracas', 'America/Caracas',
'(GMT-04:00) La Paz', 'America/La_Paz',
'(GMT-04:00) Santiago', 'America/Santiago',
'(GMT-03:30) Newfoundland', 'Canada/Newfoundland',
'(GMT-03:00) Brasilia', 'America/Sao_Paulo',
'(GMT-03:00) Buenos Aires', 'America/Argentina/Buenos_Aires',
'(GMT-03:00) Georgetown', 'America/Argentina/Buenos_Aires',
'(GMT-03:00) Greenland', 'America/Godthab',
'(GMT-02:00) Mid-Atlantic', 'America/Noronha',
'(GMT-01:00) Azores', 'Atlantic/Azores',
'(GMT-01:00) Cape Verde Is.', 'Atlantic/Cape_Verde',
'(GMT+00:00) Casablanca', 'Africa/Casablanca',
'(GMT+00:00) Edinburgh', 'Europe/London',
'(GMT+00:00) Greenwich Mean Time : Dublin', 'Etc/Greenwich',
'(GMT+00:00) Lisbon', 'Europe/Lisbon',
'(GMT+00:00) London', 'Europe/London',
'(GMT+00:00) Monrovia', 'Africa/Monrovia',
'(GMT+00:00) UTC', 'UTC',
'(GMT+01:00) Amsterdam', 'Europe/Amsterdam',
'(GMT+01:00) Belgrade', 'Europe/Belgrade',
'(GMT+01:00) Berlin', 'Europe/Berlin',
'(GMT+01:00) Bern', 'Europe/Berlin',
'(GMT+01:00) Bratislava', 'Europe/Bratislava',
'(GMT+01:00) Brussels', 'Europe/Brussels',
'(GMT+01:00) Budapest', 'Europe/Budapest',
'(GMT+01:00) Copenhagen', 'Europe/Copenhagen',
'(GMT+01:00) Ljubljana', 'Europe/Ljubljana',
'(GMT+01:00) Madrid', 'Europe/Madrid',
'(GMT+01:00) Paris', 'Europe/Paris',
'(GMT+01:00) Prague', 'Europe/Prague',
'(GMT+01:00) Rome', 'Europe/Rome',
'(GMT+01:00) Sarajevo', 'Europe/Sarajevo',
'(GMT+01:00) Skopje', 'Europe/Skopje',
'(GMT+01:00) Stockholm', 'Europe/Stockholm',
'(GMT+01:00) Vienna', 'Europe/Vienna',
'(GMT+01:00) Warsaw', 'Europe/Warsaw',
'(GMT+01:00) West Central Africa', 'Africa/Lagos',
'(GMT+01:00) Zagreb', 'Europe/Zagreb',
'(GMT+02:00) Athens', 'Europe/Athens',
'(GMT+02:00) Bucharest', 'Europe/Bucharest',
'(GMT+02:00) Cairo', 'Africa/Cairo',
'(GMT+02:00) Harare', 'Africa/Harare',
'(GMT+02:00) Helsinki', 'Europe/Helsinki',
'(GMT+02:00) Istanbul', 'Europe/Istanbul',
'(GMT+02:00) Jerusalem', 'Asia/Jerusalem',
'(GMT+02:00) Kyiv', 'Europe/Helsinki',
'(GMT+02:00) Pretoria', 'Africa/Johannesburg',
'(GMT+02:00) Riga', 'Europe/Riga',
'(GMT+02:00) Sofia', 'Europe/Sofia',
'(GMT+02:00) Tallinn', 'Europe/Tallinn',
'(GMT+02:00) Vilnius', 'Europe/Vilnius',
'(GMT+03:00) Baghdad', 'Asia/Baghdad',
'(GMT+03:00) Kuwait', 'Asia/Kuwait',
'(GMT+03:00) Minsk', 'Europe/Minsk',
'(GMT+03:00) Nairobi', 'Africa/Nairobi',
'(GMT+03:00) Riyadh', 'Asia/Riyadh',
'(GMT+03:00) Volgograd', 'Europe/Volgograd',
'(GMT+03:30) Tehran', 'Asia/Tehran',
'(GMT+04:00) Abu Dhabi', 'Asia/Muscat',
'(GMT+04:00) Baku', 'Asia/Baku',
'(GMT+04:00) Moscow', 'Europe/Moscow',
'(GMT+04:00) Muscat', 'Asia/Muscat',
'(GMT+04:00) St. Petersburg', 'Europe/Moscow',
'(GMT+04:00) Tbilisi', 'Asia/Tbilisi',
'(GMT+04:00) Yerevan', 'Asia/Yerevan',
'(GMT+04:30) Kabul', 'Asia/Kabul',
'(GMT+05:00) Islamabad', 'Asia/Karachi',
'(GMT+05:00) Karachi', 'Asia/Karachi',
'(GMT+05:00) Tashkent', 'Asia/Tashkent',
'(GMT+05:30) Chennai', 'Asia/Calcutta',
'(GMT+05:30) Kolkata', 'Asia/Kolkata',
'(GMT+05:30) Mumbai', 'Asia/Calcutta',
'(GMT+05:30) New Delhi', 'Asia/Calcutta',
'(GMT+05:30) Sri Jayawardenepura', 'Asia/Calcutta',
'(GMT+05:45) Kathmandu', 'Asia/Katmandu',
'(GMT+06:00) Almaty', 'Asia/Almaty',
'(GMT+06:00) Astana', 'Asia/Dhaka',
'(GMT+06:00) Dhaka', 'Asia/Dhaka',
'(GMT+06:00) Ekaterinburg', 'Asia/Yekaterinburg',
'(GMT+06:30) Rangoon', 'Asia/Rangoon',
'(GMT+07:00) Bangkok', 'Asia/Bangkok',
'(GMT+07:00) Hanoi', 'Asia/Bangkok',
'(GMT+07:00) Jakarta', 'Asia/Jakarta',
'(GMT+07:00) Novosibirsk', 'Asia/Novosibirsk',
'(GMT+08:00) Beijing', 'Asia/Hong_Kong',
'(GMT+08:00) Chongqing', 'Asia/Chongqing',
'(GMT+08:00) Hong Kong', 'Asia/Hong_Kong',
'(GMT+08:00) Krasnoyarsk', 'Asia/Krasnoyarsk',
'(GMT+08:00) Kuala Lumpur', 'Asia/Kuala_Lumpur',
'(GMT+08:00) Perth', 'Australia/Perth',
'(GMT+08:00) Singapore', 'Asia/Singapore',
'(GMT+08:00) Taipei', 'Asia/Taipei',
'(GMT+08:00) Ulaan Bataar', 'Asia/Ulan_Bator',
'(GMT+08:00) Urumqi', 'Asia/Urumqi',
'(GMT+09:00) Irkutsk', 'Asia/Irkutsk',
'(GMT+09:00) Osaka', 'Asia/Tokyo',
'(GMT+09:00) Sapporo', 'Asia/Tokyo',
'(GMT+09:00) Seoul', 'Asia/Seoul',
'(GMT+09:00) Tokyo', 'Asia/Tokyo',
'(GMT+09:30) Adelaide', 'Australia/Adelaide',
'(GMT+09:30) Darwin', 'Australia/Darwin',
'(GMT+10:00) Brisbane', 'Australia/Brisbane',
'(GMT+10:00) Canberra', 'Australia/Canberra',
'(GMT+10:00) Guam', 'Pacific/Guam',
'(GMT+10:00) Hobart', 'Australia/Hobart',
'(GMT+10:00) Melbourne', 'Australia/Melbourne',
'(GMT+10:00) Port Moresby', 'Pacific/Port_Moresby',
'(GMT+10:00) Sydney', 'Australia/Sydney',
'(GMT+10:00) Yakutsk', 'Asia/Yakutsk',
'(GMT+11:00) Vladivostok', 'Asia/Vladivostok',
'(GMT+12:00) Auckland', 'Pacific/Auckland',
'(GMT+12:00) Fiji', 'Pacific/Fiji',
'(GMT+12:00) International Date Line West', 'Pacific/Kwajalein',
'(GMT+12:00) Kamchatka', 'Asia/Kamchatka',
'(GMT+12:00) Magadan', 'Asia/Magadan',
'(GMT+12:00) Marshall Is.', 'Pacific/Fiji',
'(GMT+12:00) New Caledonia', 'Asia/Magadan',
'(GMT+12:00) Solomon Is.', 'Asia/Magadan',
'(GMT+12:00) Wellington', 'Pacific/Auckland',
'(GMT+13:00) Nuku\'alofa', 'Pacific/Tongatapu'

]




import json
# Connect to "test" database as root user.
# db = client.db('_system', username='root', password='arangodb')

# if db.has_graph('meetings'):
#     meetings = db.graph('meetings')
# else:
#     meetings = db.create_graph('meetings')


# meetings = client.db('workpeer3000', 'root', 'arangodb')


# if meetings.has_vertex_collection('third_party_creds'):
#     third_party_creds = meetings.vertex_collection('third_party_creds')
# else:
#     third_party_creds = meetings.create_vertex_collection('third_party_creds')



from arango import ArangoClient




client = ArangoClient(hosts='http://localhost:8529')

#general_db = client.db(config.HOME_DB_NAME,username=config.DB_USER,password=config.DB_PASS)

#meetings_db

'''
connects to the main datbase '_system'
'''
def conn_main_db(username, password, default_db = "_system"):
    sys_db = client.db(default_db,username=username,password=password)

    return sys_db

'''
connects to the database that has to be accessed other than main '_system' database
'''
def conn_db(db_name, username, password, default_db = "_system", admin_access = False):


    sys_db = conn_main_db(username, password)
    if sys_db is not None:
        if not sys_db.has_database(db_name):
            sys_db.create_database(db_name)

    meetings_db = client.db(db_name, username, password)

    if admin_access == True:
        return sys_db, meetings_db

    return meetings_db



'''
using the above database connections...
we will connect to the graph required for the implementation
'''
def get_graph(conn, graph_name):
    if not conn.has_graph(graph_name):
        meetings = conn.create_graph(graph_name)
        #print(False)
    else:
        meetings = conn.graph(graph_name)
        #print(True)
    return meetings


db_conn = conn_db('workpeer17', username='root', password='arangodb')
graph = get_graph(db_conn,'meetings')




if graph.has_vertex_collection('timezones'):
    zones = graph.vertex_collection('timezones')
else:
    zones = graph.create_vertex_collection('timezones')



# for ts in tzStrings:
#     #print(ts['label'])
#     time, place = ts['label'].split(') ')
#     #print(ts['label'].split(') '))
#     print(time[5:], place)

# print(len(tzStrings))

#i = 0
count = 0
for i in range(0, len(timezones), 2):
    tz = timezones[i].split(' ')[0]
    print(timezones[i+1])
    print(tz+","+timezones[i+1], tz[1:-1])
    try:
        zones.insert({"_key": timezones[i+1].replace('/', '_'), "location": tz+","+timezones[i+1], "utc_diff": tz[1:-1]})
    except:
        print("already included.....")
    count = count+1

print(count)


# while i < le(timezones):
#     print(timezones)