import uvicorn
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
import os, time
#from fastapi_sqlalchemy import DBSessionMiddleware
#from fastapi_sqlalchemy import db
#from dotenv import load_dotenv
from starlette.requests import Request
from starlette.responses import FileResponse
from fastapi.middleware.cors import CORSMiddleware
from app.routes import home_router
from app.models import models
from app.routes.v1 import meetings_router, security_router
from core.config import get_config
from core.exception import CustomException
from app.services.crud import connect_service

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
#load_dotenv(os.path.join(BASE_DIR, ".env"))

#app = FastAPI()
# app.mount("/static", StaticFiles(directory="static"), name="static")
origins = ['*']

def init_routers(app: FastAPI) -> None:
    app.include_router(home_router)
    app.include_router(meetings_router, prefix='/api/v1', tags=['Orders'])
    app.include_router(security_router, prefix='/api/v1', tags=['Security'])

def check_vertexes(app:FastAPI) -> None:
    models.code_check()
def init_listeners(app: FastAPI) -> None:
    # Exception handler
    @app.exception_handler(CustomException)
    async def custom_exception_handler(request: Request, exc: CustomException):
        return dict(
            status_code=exc.code,
            content=exc.kwargs,
        )

def create_app() -> FastAPI:
    config = get_config()
    app = FastAPI(
        title='workpeer',
        description='workpeer API',
        version='1.0.0',
        docs_url=None if config.ENV == 'production' else '/docs',
        redoc_url=None if config.ENV == 'production' else '/redoc',
    )
    init_routers(app=app)
    init_listeners(app=app)
    check_vertexes(app=app)
    #app.add_middleware(DBSessionMiddleware, db_url=config.DB_URL)
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


    return app


app = create_app()

@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    """
    a middleware to find the time required for an api to process the data and
    getting the route of the api which is being executed...
    @parameters:
    conditions to check:
    1. content headers
    2. content length



    request: contains all the details of the api requested
    ex:
        {'type': 'http', 'asgi': {'version': '3.0', 'spec_version': '2.1'}, 'http_version': '1.1', 
        'server': ('127.0.0.1', 5000), 'client': ('127.0.0.1', 61833), 'scheme': 'http', 'method': 'POST',
        'root_path': '', 'path': '/users', 'raw_path': b'/users', 'query_string': b'', 
        'headers': [(b'host', b'127.0.0.1:5000'), (b'connection', b'keep-alive'), (b'content-length', b'58'), 
        (b'accept', b'application/json'), (b'user-agent', b'Mozilla/5.0 (Windows NT 10.0; Win64; x64) 
        AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'), (b'content-type',
        b'application/json'), (b'origin', b'http://127.0.0.1:5000'), (b'sec-fetch-site', b'same-origin'), 
        (b'sec-fetch-mode', b'cors'), (b'sec-fetch-dest', b'empty'), (b'referer', b'http://127.0.0.1:5000/docs'),
        (b'accept-encoding', b'gzip, deflate, br'), (b'accept-language', b'en-US,en;q=0.9')], 
        'fastapi_astack': <contextlib.AsyncExitStack object at 0x00000185C3501088>, 
        'app': <fastapi.applications.FastAPI object at 0x00000185C08C2908>,
        'router': <fastapi.routing.APIRouter object at 0x00000185C33E8D88>, 
        'endpoint': <function create_user at 0x00000185C33CDE58>, 'path_params': {}}
    call_next: call_next starts analysing the request and the entire process of repsonse.

    @returns:
    response: just returns the api response and its execution time appended
    """
    start_time = time.time()
    response = await call_next(request)
    path = request.scope['path']#[route for route in request.scope['router'].routes if route.endpoint == request.scope['endpoint']][0].path
    #this path variable derives the route of the api that is being accessed currently
    process_time = time.time() - start_time
    print(f'Path is: {path}\nexecution time is {process_time}')
    #print("###########################################################")
    #print(process_time)
    print(request.headers)

    response.headers["X-Process-Time"] = str(process_time)
    return response



@app.get("/service-worker.js")
def renderstatic():
    return FileResponse(os.path.join(BASE_DIR, "static/service-worker.js"), headers={'content-type': 'application/javascript'})

@app.get("/notifications.js")
def renderstatic():
    #return FileResponse("static/notifications.js")
    return FileResponse(os.path.join(BASE_DIR, "static/notifications.js"), headers={'content-type': 'application/javascript'})

@app.on_event("startup")
def notifications_connect_service():
    connect_service()
