"""
This file contains all the schemas used all over the app

"""

# security : forgot passwword schemas

import datetime,re,os
from datetime import date
from fastapi import Form, File,UploadFile
from typing import Optional, List, Dict
from pydantic import BaseModel, EmailStr, ValidationError, validator
from starlette.requests import Request


# Schema dependency


class EmailSchema(BaseModel):
    """
    Pydantic Schema for email account recovery.

    Attributes
    ----------
    email : str
        The email where the recovery mail would be sent.
    Author: Priyav Kaneria
    """
    email : EmailStr
    #def __init__(self, email: str = Form(...)):
    #   """
    #    Parameters
    #    ----------
    #    email : HTML Form
    #        Would initialize the email from the submitted html form.
    #    """

    ##    #self.email = email


class InviteEmailSchema(BaseModel):
    """
    Pydantic Schema for email account recovery.

    Attributes
    ----------
    email : str
        The email where the recovery mail would be sent.
    organization : str
        The organization.
    """
    emails: str
    organization: str
    user_email: EmailStr
    '''
    def __init__(self, email: str = Form(...), organization: str = Form(...), user_email: str = Form(...)):
        """
        Parameters
        ----------
        email : HTML Form
            Would initialize the email from the submitted html form.
        """

        self.emails = email
        self.organization = organization
        self.email = user_email
    '''

class NewPassword:
    """
    Pydantic Schema for account password.

    Attributes
    ----------
    password : str
        The new password that user will enter after forgot email verification.
    """

    def __init__(self, user_password: str = Form(...)):
        """
        Parameters
        ----------
        password : HTML Form
            Would initialize the password from the submitted html form.
        """

        self.password = user_password


class SentPasscode:
    """
    Pydantic Schema for email account recovery.

    Attributes
    ----------
    passcode : str
        The passcode sent to the user on their mail.
    """

    def __init__(self, passcode: str = Form(...)):
        """
        Parameters
        ----------
        passcode : HTML Form
            Would initialize the passcode from the submitted html form.
        """

        self.passcode = passcode


class UserItem(BaseModel):
    """
    pydantic schema for new user sign-up
    """
    #def __init__(self,
                #  user_name: str = Form(...),
                # email: EmailStr,
                #  user_password: str = Form(...),
                #  user_organization: str = Form(...),
                #  user_position: str = Form(...),
                #  user_location: str = Form(...)
                # ):

        # self.username = user_name
    email: EmailStr
        # self.password = user_password
        # self.organization = user_organization
        # self.position = user_position
        # self.location = user_location




class Org:
    def __init__(self,
        organization_name: str = Form(...)):

        self.org_name = organization_name


class MeetingSummary(BaseModel):
    '''
    Pydantic Schema for summary of users after meeting is completed.
    '''
    user: str
    summary: List[str]


class MeetingItem(BaseModel):
    '''
    pydantic schema for meeting history.
    '''
    subject: str
    location: str
    link: str
    key: str
    created_date: str
    created_time: str
    date: str
    starttime: str
    duration: str
    invitees: list
    attended_y: List[str]
    attended_n: List[str]
    agenda: str
    # summary:List[meeting_summary]


class LoginUserSchema(BaseModel):
    """
    Pydantic schema for user login
    Currently the username is same as email
    """
    username: str
    name: Optional[str] = None
    email: Optional[str] = None
    disabled: Optional[bool] = None


class Token(BaseModel):
    """
    JWT token schema
    """    
    access_token: str
    token_type: str


class TokenData(BaseModel):
    """
    schema to check for payload
    """
    username: Optional[str] = None


class UserInDB(LoginUserSchema):
    """
    schema to append hashed_password to 
    login_user_schema for security.
    """
    hashed_password: str
    

class UserMeetingEdgeItem(BaseModel):
    """
    pydantic schema for user to meeting edge.
    """
    To:str
    From:str
    Date:str
    Host:str


class TeamsItem(BaseModel):
    """
    pydantic schema for newly formed team.
    """
    Team_name:str
    Created_at:str
    Disabled_yn:bool
    Members:Dict

class UserTeamEdge(BaseModel):
    """
    pydantic schema for user - team edge.
    """
    From:str
    To:str

"""
for posting active hours into active_hours vertex
"""
class ActiveHours(BaseModel):
    username: str
    start_time: str
    end_time: str

"""
for posting busy hours into busy_hours vertex
"""
class BusyHours(BaseModel):
    username: str
    meeting_date : str
    start_time: str
    end_time: str


class CommonSlots(BaseModel):
    users: List


class FileSchema(BaseModel):
    '''
    Pydantic Schema for insertion of new file into file system.
    '''
    filename : str
    file_type : str
    file_relation : str
    file_path : str


class ProjectItem(BaseModel):
    """
    pydantic schema for new project form
    """
    description:Optional[str] = None
    project_type:Optional[str] = None
    title:Optional[str] = None
    start_date: Optional[date] = None
    end_date: Optional[date] = None
    teams : Optional[str] = None
    color: Optional[str] = None
    # description: Optional[str] = None
    project_code:Optional[str] = None
       
class CreateProject(BaseModel):
    project_type:str
    title:str
    start_date: date
    end_date: date
    teams : str
    color: str
    #description: Optional[str] = None
    project_code:Optional[str] = None


################################################################
##################organizations schema Ops #####################

class InviteMembers(BaseModel):

    org_name: str
    members : List


class DeleteMembers(BaseModel):
    #org_name: str
    members : List[str]


class WebexToken(BaseModel):
    code : str
    state : str


class Create:
    def __init__(self,
        meeting_topic: str = Form(...),
        meeting_duration: str = Form(...),
        meeting_password: str = Form(...),
        zoom_datetime:str = Form(...),
        meeting_host:str = Form(...)):

        self.topic=meeting_topic
        self.duration=meeting_duration
        self.password=meeting_password
        self.zoom_datetime=zoom_datetime
        self.host=meeting_host

    @validator('zoom_datetime')
    def name_must_contain_T(cls, v):
        if 'T' not in v:
            raise ValueError('must be in correct format')
        return v.title()
        
class CreateWebex:
    def __init__(self,
        meeting_topic: str = Form(...),
        meeting_password_webex: str = Form(...),
        meeting_start_datetime:str = Form(...),
        meeting_end_datetime:str = Form(...)):

        self.topic=meeting_topic
        self.password=meeting_password_webex
        self.start_datetime=meeting_start_datetime
        self.end_datetime=meeting_end_datetime
    @validator('meeting_start_datetime')
    def name_must_contain_T(cls, v):
        if 'T' not in v:
            raise ValueError('must be in correct format')
        return v.title()

class CreateMeetingGet:
    topic: Optional[str] ="Learn API"
    duration: Optional[str]="30"
    password: Optional[str]="abcd"
    zoom_datetime:Optional[str]="T"
    host:Optional[str]=""
    title: Optional[str] ="Webex Meeting"
    password_webex: Optional[str]="abcd"
    end:Optional[str]="T"
    start:Optional[str]="T"
    invitees:Optional[str]=""

class CreateMeetingPost:
    def __init__(self,
        name_of_host: str = Form(...),
        meeting_subject: str = Form(...),
        meeting_date:str = Form(...),
        meeting_agenda:str = Form(...),
        start_time:str = Form(...),
        end_time:str = Form(...),
        custom_link:str = Form(...),
        recieved_link:str = Form(...),
        cloud_service_file_upload:str=Form(...)):
        self.hostname = name_of_host
        self.meeting_subject=meeting_subject
        self.date = meeting_date
        self.agenda = meeting_agenda
        self.start_time= start_time
        self.end_time = end_time
        self.link = custom_link
        self.linktext = recieved_link
        self.fileuploadthirdparty=cloud_service_file_upload
        

class Username:
    def __init__(self,
        user_name: str = Form(...)):

        self.username=user_name


class DeleteMember(BaseModel):
    team_name: str
    name: str



# class DeleteMember:
#     def __init__(self,
#         name_of_team: str = Form(...),
#         name_of_member:str=Form(...)):

#         self.TeamName=name_of_team
#         self.MemberName=name_of_member
class DeleteAdmin:
    def __init__(self,
        name_of_team: str = Form(...)):
        self.TeamName=name_of_team

class ModifyAdmin:
    def __init__(self,
        project_admin: str = Form(...)):
        self.admin=project_admin

class TeamEdgeProject:
    def __init__(self,
        name_of_project: str = Form(...),
        year_created: str = Form(...),
        month_created: str = Form(...),
        day_created: str = Form(...),
        name_of_team: str = Form(...)):
        self.ProjectName=name_of_project
        self.CreatedYear=year_created
        self.CreatedMonth=month_created
        self.CreatedDay=day_created
        self.TeamName=name_of_team

#class AddNewItem:
#    item_data:str

class GenerateMeetingLink:
    def __init__(self,
        name_of_host: str = Form(...),
        meeting_subject: str = Form(...),
        meeting_date:str = Form(...),
        meeting_agenda:str = Form(...),
        start_time:str = Form(...),
        end_time:str = Form(...),
        meeting_provider:str=Form(...)):
        self.hostname = name_of_host
        self.meeting_subject=meeting_subject
        self.date = meeting_date
        self.agenda = meeting_agenda
        self.start_time= start_time
        self.end_time = end_time
        self.meetingprovider=meeting_provider

class UpdateMeeting:
    def __init__(self,
        meeting_subject: str = Form(...),
        meeting_date:str = Form(...),
        meeting_agenda:str = Form(...),
        meeting_start_time:str = Form(...),
        meeting_end_time:str = Form(...)):
        self.meeting_subject=meeting_subject
        self.date = meeting_date
        self.agenda = meeting_agenda
        self.start_time= meeting_start_time
        self.end_time = meeting_end_time


class MeetingReview:
    def __init__(self,
        user_rating: int = Form(...),
        meeting_summary: str = Form(...)):
        self.rating=user_rating
        self.meeting_summary=meeting_summary


class CreateAction:
    def __init__(self,
        name_of_action: str = Form(...),
        action_description: str = Form(...),
        expected_deadline: str = Form(...),
        list_of_tasks: str = Form(...),
        assigned_to: str = Form(...)):
        self.action_title=name_of_action
        self.action_description=action_description
        self.expected_date=expected_deadline
        self.tasks=list_of_tasks
        self.assignees=assigned_to
class InsertDecision:
    def __init__(self,
        decision_list: str = Form(...)):
        self.decision=decision_list

class ModifyDecision:
    def __init__(self,
        decision_list: str = Form(...)):
        self.modified_decision=decision_list

class ModifyTask:
    def __init__(self,
        task_list: str = Form(...)):
        self.modified_task=task_list

class NewTask:
    def __init__(self,
        task_list: str = Form(...)):
        self.tasks=task_list

class AssignTask:
    def __init__(self,
        assigned_to: str = Form(...)):
        self.assignees=assigned_to

class AddComment:
    def __init__(self,
        decision_comment: str = Form(...)):
        self.comment=decision_comment

class TaskComment:
    def __init__(self,
        comment_task: str = Form(...)):
        self.comment=comment_task

class AddNotes:
    def __init__(self,
        user_notes: str = Form(...)):
        self.notes=user_notes


class UpdateGoogleEvent:
    def __init__(self,
        meeting_date: str = Form(...),
        meeting_subject: str = Form(...),
        meeting_agenda: str = Form(...),
        meeting_start_time: str = Form(...),
        meeting_end_time: str = Form(...)):
        self.date=meeting_date
        self.meeting_subject=meeting_subject
        self.agenda=meeting_agenda
        self.start_time=meeting_start_time
        self.end_time=meeting_end_time

class UpdateEventMicrosoft:
    def __init__(self,
        meeting_date: str = Form(...),
        meeting_subject: str = Form(...),
        meeting_agenda: str = Form(...),
        meeting_start_time: str = Form(...),
        meeting_end_time: str = Form(...)):
        self.date=meeting_date
        self.meeting_subject=meeting_subject
        self.agenda=meeting_agenda
        self.start_time=meeting_start_time
        self.end_time=meeting_end_time

class AddWorkspace:
    def __init__(self,
        org_name: str = Form(...)):
        self.org_name=org_name

class Createorganization(BaseModel):
    '''
    def __init__(self,
        organization_name: str = Form(...),
        state: str = Form(...),
        country: str = Form(...),
        # username: str = Form(...),
        email: str = Form(...),
        # password: str = Form(...),
        # position: str = Form(...),
        # timezone: str = Form(...),
        # invite: str = Form(...),
        # files: str = Form(...),
        #company_email: str = Form(...)
        
        ):
        self.organization=organization_name
        self.state=state
        self.country=country
        # self.username=username
        self.email=email
        # self.password=password    
        # self.position=position    
        # self.timezone=timezone    
        # self.invite=invite
        # self.files=files
    '''
    organization: str
    email: EmailStr




class AddAgenda:
    def __init__(self,
        agenda_name: str = Form(...),
        assigned_to: str = Form(...),
        deadline: str = Form(...)):
        self.new_agenda=agenda_name
        self.assignees=assigned_to    
        self.time_required=deadline    

class SearchData:
    def __init__(self,
        search_text: str = Form(...)):
        self.search_data=search_text
class Markdown:
    def __init__(self,
        mark_down: str = Form(...)):
        self.md=mark_down

class AddTeamMember:
    def __init__(self,
        name_of_team: str = Form(...),
        new_member_name: str = Form(...)):
        self.TeamName=name_of_team
        self.MemberName=new_member_name

class ModifyAgenda:
    def __init__(self,
        modified_agenda: str = Form(...),
        assigned_to: str = Form(...),
        deadline: str = Form(...)):
        self.modified_agenda=modified_agenda
        self.assignees=assigned_to    
        self.time_required=deadline



class AccountRecovery:
    def __init__(self,
        recieved_passcode: str = Form(...),
        user_email: str = Form(...)):

        self.passcode = recieved_passcode
        self.email = user_email
        

class GetAuthToken:
    def __init__(self,
        user_sub: str = Form(...),
        user_email: str = Form(...)):
        self.sub=user_sub
        self.email=user_email


class AuthenticateLogin:
    def __init__(self,
        email: str = Form(...)):
        self.email=email

class AuthenticateOTP(BaseModel):
    #def __init__(self,
    #    email: str = Form(...),
    #    otp: str = Form(...)):
    #    self.email=email
    #    self.otp=otp

    email: EmailStr
    otp: str

class AuthenticateClass:
    def __init__(self,
        email: str = Form(...),
        user_password: str = Form(...)):
        self.email=email
        self.password=user_password


class CreateTeam(BaseModel):
    team_name: str
    members: str


class UpdateTeam(BaseModel):
    team :Optional[str] = None
    members: Optional[str] = None


class ContactUs(BaseModel):
    reason: Optional[str]
    email: EmailStr
    query: Optional[str]
