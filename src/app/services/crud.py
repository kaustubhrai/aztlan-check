# importing vertices
from app.models.models import (
    user,
    meetings,
    agenda,
    orgs,
    invitations,
    notifications,
    missed_notifications,
    improve_us,
    teams,
    contact_us,
    project,
    action,
    task,
    Files,
    project_team,
    decision,   
)

# importing edges
from app.models.models import (
    graph,
    db_conn,
    user_improve_us,
    user_org,
    org_team,
    user_invitations,
    user_third_parties,
    user_calendar,
    user_meeting,
    user_team_edge,
    user_active_slots,
    user_busy_slots,
    active_slots,
    busy_slots,
    user_check_list,
    check_list,
    project_team,
    user_project,
    user_action,
    meeting_action,
    action_task,
    user_task,
    meeting_decision,
    meeting_agenda,
    user_creds,
    login_devices,
    project_org,
    project_members,
    project_meetings,
    teams_meetings,
    org_meetings
)


from app.models.models import aql

#importing views
from app.models.views import (
    meetings_view,
    actions_view
)



from app.schemas.schemas import (
    UserItem,
    MeetingItem,
    UserMeetingEdgeItem,
    NewPassword,
)

import logging, time as current_time
from os import path
from typing import Dict, List
from uuid import uuid4
from fastapi.encoders import jsonable_encoder
import datetime
from datetime import date, time, timedelta, datetime as dt
import json,hashlib
from user_agents import parse
import platform as pf
from getmac import get_mac_address
from socket import gethostbyname, gethostname
from random import randint

from app import templates


import pickle, time
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request as req
from google.oauth2.credentials import Credentials

from pusher_push_notifications import PushNotifications


string_hash_count = 100000000


def string_hasher(s):
    return str(int(hashlib.sha256(s.encode('utf-8')).hexdigest(), 16) % string_hash_count)




def init_logger_singleton(type):
    mylogger = logging.getLogger(name='admin')
    mylogger.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(name)s:%(asctime)s:%(module)s:%(lineno)s:%(levelname)s:%(message)s]')
    mypath=path.join(path.abspath(path.dirname(__file__)),'logs')
    filehandler = logging.FileHandler(path.join(mypath, f'{type}.log'))
    filehandler.setLevel(logging.INFO)
    filehandler.setFormatter(formatter)
    mylogger.addHandler(filehandler)
    return mylogger

# Logging setup -- Please read logger.py file to use this logger
mylogger = init_logger_singleton('crud')

mylogger.info(user)
mylogger.info(meetings)



######################################################################################
############################### PUSHER NOTIFICATIONS #################################
######################################################################################

# Author : Priyav Kaneria

def connect_service():
    global beams_client
    beams_client = PushNotifications(
        instance_id='13917092-3f6f-49e5-b39b-e21c89f24565',
        secret_key='476891DF53AD7EDD6CE141AA992B351E20F2B128C7576EE17799C7911D4B841B',
    )


def send_notification(ntype: str, nlist, notification_data):
    title = notification_data["title"]
    body = notification_data["message"]
    link = notification_data["link"]
    if ntype=='DeviceInterest':
        response = beams_client.publish_to_interests(
            interests=nlist,
            publish_body={
                'apns': {
                'aps': {
                    'alert': {
                    'title': title,
                    'body': body,
                    'deep_link': link
                    },
                },
                },
                'fcm': {
                'notification': {
                    'title': title,
                    'body': body,
                    'deep_link': link
                },
                },
                'web': {
                'notification': {
                    'title': title,
                    'body': body,
                    'deep_link': link
                },
                },
            })
    elif ntype=='Users':
        response = beams_client.publish_to_users(
            user_ids=nlist,
            publish_body={
                'apns': {
                'aps': {
                    'alert': {
                    'title': title,
                    'body': body, 
                    'deep_link': link
                    },
                },
                },
                'fcm': {
                'notification': {
                    'title': title,
                    'body': body,
                    'deep_link': link
                },
                },
                'web': {
                'notification': {
                    'title': title,
                    'body': body,
                    'deep_link': 'https://www.workpeer.in'+link
                },
                },
            })
    mylogger.info(response)
    return True


############# NOTIFICATIONS #################
# Author: Priyav

def missed_notify_user(email:str):
    cuser = user.get(email)
    if cuser:
        missed_notifications_data = [n for n in missed_notifications if n["user_id"]==email]
        for notification in missed_notifications_data:
            send_notification('Users',[email],notification["notification"])
            missed_notifications.delete(notification)

def notify_user(email:str, title:str, message:str, link:str, id:str = "", request:str = 'post'):
    #mylogger.info("Notifying user....",email)
    cuser = user.get(email)
    print(email)
    print(cuser)
    key = notifications.insert({"_key":f"{str(datetime.datetime.now()).replace(' ','_')}", "title":title, "link":link, "email_id":email, "request":request, "message":message})
    if id!="":
        notifications.update({"_key":key["_key"],"meeting_id":id})
    #mylogger.info(cuser)
    if cuser["login_status"] == False:
        #mylogger.info("User is logged out!! Sending to missed notification...")
        missed_notifications.insert({"user_id":email,"notification":notifications.get(key["_key"])})
    else:
        #mylogger.info("Notification is sent !!")
        send_notification('Users',[email],notifications.get(key["_key"]))


######################################################################################
########################### END OF PUSHER NOTIFICATIONS ##############################
######################################################################################


#########################################################################
###############google calendar inegration################################
scopes = ['https://www.googleapis.com/auth/calendar']#.readonly']


def get_third_party_creds():
    """
    to get the google client_service details
    """
    detail = {
		"client_id": os.getenv("GOOGLE_CLIENT_ID"),
		"project_id": os.getenv("GOOGLE_PROJECT_ID"),
		"auth_uri": os.getenv("GOOGLE_AUTH_URI"),
		"token_uri": os.getenv("GOOGLE_TOKEN_URI"),
		"auth_provider_x509_cert_url": os.getenv("GOOGLE_AUTH_PROVIDER_X509_CERT_URL"),
		"client_secret": os.getenv("GOOGLE_CLIENT_SECRET"),
		"redirect_uris": os.getenv("GOOGLE_REDIRECT_URIS").split(" ")
    }

    return detail


def json_to_credentials_object(da):
    mylogger.info(da)
    """
    https://google-auth.readthedocs.io/en/stable/reference/google.oauth2.credentials.html#google.oauth2.credentials.Credentials
    Credentials is the google native class that converts
    all the token and other details into Credentials object.
    Parameters:
    @da: Dict : contains
    a. access_token as token
    b. refresh_token
    c. token_uri
    d. client_id
    e. client_secret
    f. scopes --> access points. they can be Calendar, User details, Gmail etc.,

    returns Credentials object
    """
    details = Credentials(token=da['token'], refresh_token=da['refresh_token'],
     token_uri=da['token_uri'], client_id=da['client_id'], 
     client_secret=da['client_secret'],scopes=da['scopes'])
    return details

def credentials_obj_to_json(credentials_object):
    """
    https://google-auth.readthedocs.io/en/stable/reference/google.oauth2.credentials.html#google.oauth2.credentials.Credentials
    Converts google native Credentials object into dictionary
    Parameters:
    @credentials object: Credentials

    return: json or dictionary format
    """
    creds = credentials_object.to_json()
    creds = json.loads(creds)
    creds['expiry'] = str(credentials_object.expiry)
    creds['expired'] = credentials_object.expired
    creds['valid'] = credentials_object.valid
    return creds


def create_google_cal_json(summary, location, description, start_time, end_time, invitees, timezone = 'Asia/Kolkata'):
    """
    this function will convert the raw request into the json file
    format that a google calendar required.
    parameters:
    @summary : agenda of the meeting here
    @location: location of the meeting
    @description: about the agenda/meeting description
    @start_time: starting date and time of the meeting
    @end_time: ending date and time of the meeting
    @timezone: by default it is set for Asia/Kolkata(it has to be changed
    automatically, it will be handled manually for the time being.)

    @return : return the clean json format that is required for google calendar

    """
    attendees = [{'email':invitee} for invitee in invitees]
    event = {
        'summary': summary,
        'location': location,
        'description': description,
        'start': {
        'dateTime':start_time.strftime("%Y-%m-%dT%H:%M:%S"),
        'timeZone': timezone,
        },
        'end': {
        'dateTime': end_time.strftime("%Y-%m-%dT%H:%M:%S"),
        'timeZone': timezone,
        },
        'attendees': attendees,
        'reminders': {
            'useDefault': False,
            'overrides': [
              {'method': 'email', 'minutes': 24 * 60},
              {'method': 'popup', 'minutes': 10},
            ],
          },
    }

    return event


#########################################################################


def update_user_password(email: str, passwd: str):
    """Updates the password of a particular user.

    Parameters
    ----------
    email : str
        The email of the user to which the account is linked.
    passwd: NewPassword
        Hashed password

    Returns
    -------
    True
    """

    user.update(
        {
            "_key": email,
            "password": passwd,
        }
    )

    return True


class DuplicateKeyError(Exception):
    pass

'''
def insert_user(new_user: UserItem):
    """
    Function for inserting a new user.
    """
    if user.has(new_user.email):
        raise DuplicateKeyError(
            "key {} is already present in {} vertex collection.. ".format(
                new_user.email, user
            )
        )
    else:
        nayaa_user = jsonable_encoder(new_user)
        mylogger.info(nayaa_user)
        nayaa_user["_key"] = new_user.email
        #nayaa_user["onboarded_yn"] = False #field to check weather user has completed onboarding process or not (initially its false , it will be true when user is registederd in db along with organization)
        nayaa_user['since'] = dt.strftime(date.today(), '%Y-%m-%d')
        nayaa_user['email_verified'] = False
        nayaa_user['active_yn'] = False
        nayaa_user['recovery_passcode'] = ""
        nayaa_user['recovered_yn'] = True
        nayaa_user['login_passcode'] = ''
        nayaa_user["login_otp_gen_time"] = ''
        nayaa_user["organization"] = ""
        mylogger.info(nayaa_user)
        new_user_entered = user.insert(
            nayaa_user
        )
    return new_user_entered
'''

def insert_user(new_user: UserItem):
    """
    Function for inserting a new user.
    """
    if user.has(new_user.email):
        # raise DuplicateKeyError(
        #     "key {} is already present in {} vertex collection.. ".format(
        #         new_user.email, user
        #     )
        # )
        return 409
    else:
        nayaa_user = jsonable_encoder(new_user)
        mylogger.info(nayaa_user)
        nayaa_user["_key"] = new_user.email
        #nayaa_user["onboarded_yn"] = False #field to check weather user has completed onboarding process or not (initially its false , it will be true when user is registederd in db along with organization)
        nayaa_user['since'] = dt.strftime(date.today(), '%Y-%m-%d')
        nayaa_user['email_verified'] = False
        nayaa_user['active_yn'] = False
        nayaa_user['recovery_passcode'] = ""
        nayaa_user['recovered_yn'] = True
        nayaa_user['login_passcode'] = ''
        nayaa_user["login_otp_gen_time"] = ''
        nayaa_user["organization"] = ""
        nayaa_user['username'] = nayaa_user['email'].split('@')[0]
        mylogger.info(nayaa_user)
        new_user_entered = user.insert(
            nayaa_user
        )
    return 202



def insert_meetings(NewMeeting: MeetingItem):
    """
    Function for inserting a new meeting.
    """

    if meetings.has(NewMeeting.key):
        raise DuplicateKeyError(
            "key {} is already present in {} vertex collection.. ".format(
                NewMeeting.key, meetings
            )
        )
    else:
        new_meeting = meetings.insert(
            {
                "_key": NewMeeting.key,
                "subject": NewMeeting.subject,
                "location": NewMeeting.location,
                "link": NewMeeting.link,
                "agenda": NewMeeting.agenda,
                "created_date": NewMeeting.created_date,
                "created_time": NewMeeting.created_time,
                "date": NewMeeting.date,
                "start-time": NewMeeting.starttime,
                "duration": NewMeeting.duration,
                "invitees": NewMeeting.invitees,
                "attended_y": NewMeeting.attended_y,
                "attended_n": NewMeeting.attended_n,
                # 'summary':NewMeeting.summary
            }
        )
    return new_meeting


def insert_user_meeting_edge(UMEDGE: UserMeetingEdgeItem):

    """
    Function is for creating new edge from user to meeting

    Parameter
    ---------
    UMEDGE is pydantic model parameter
    for schema of new edge.

    Return
    ------
    Returns a new edge from user to meeting node.

    """

    new_UMEDGE = user_meeting.insert(
        {
            "_to": "meetings/" + UMEDGE.To,
            "_from": "account/" + UMEDGE.From,
            "date": UMEDGE.Date,
            "host": UMEDGE.Host,
        }
    )

    return new_UMEDGE


def insert_new_contactus_data(data):

    """
    Function just add the message from a person
    from the contact-us page.

    Parameter
    ---------
    parameter 'data' is formdata from the 
    contact-us page.

    Return
    ------
    Return a new vertex from contact-us vertex 
    collection.

    """
    return contact_us.insert(
        {
            "firstname": data["FirstName"],
            "lastname": data["LastName"],
            "email": data["Email"],
            "message": data["Message"],
        }
    )



def get_org_teams(org: str):
    """
    Function to get all teams of organization

    Parameter
    ---------
    org :str
        - The org of the logged in user
    
    Return
    ------
    Return a list of all teams
    """
    return [u["_key"].split('_')[1] for u in org_team if u["_key"].split('_')[0]==org]


def get_org_user_teams(email: str):
    org = get_user_org(email)
    print(org)
    workspace_teams = get_org_teams(org)
    my_teams = get_user_teams(email, workspace_teams)

    return my_teams



def get_team_key(team_name, org_teams):

    for team_key in org_teams:
        #if org_team.has(team_key+'_'+)
        team_det = teams.get(team_key)
        if team_det['Team_name'] == team_name:
            return team_key

    return None


def update_team(update_team, team_id):

    team_details = teams.get(team_id)
    update_team = update_team.__dict__

    if update_team['team'] is not None:
        team_details['Team_name'] = update_team['team']

    if update_team['members'] is not None:
        members_det = team_details['Members']
        now = str(datetime.datetime.now())

        for member in update_team['members'].split(','):
            if member not in team_details['Members'].keys():
                members_det[member] = {'joined_date':now, 'active_yn':True}

        team_details['Members'] = members_det

    try:
        teams.update({'_key':team_id, 'Team_name': team_details['Team_name'], 'Members':team_details['Members']})
        if update_team['members'] is not None:
            for member in update_team['members'].split(','):
                
                print(member, team_details['Members'].keys())
                #if member in team_details['Members'].keys():
                print(team_id, member)
                user_team_edge.insert({'_key':str(team_id)+'_'+member, '_from':'account/'+member, '_to':'teams/'+team_id})

        return True


    except Exception as e:
        print(e)
        return False



def get_user_teams(email: str, workspace_teams: list):
    """
    Function to get all teams data of which the user is a member or admin

    Parameter
    ---------
    email :str
        - The email of the logged in user
    
    Return
    ------
    Return a list of all teams docs
    """

    my_teams = []

    for wp_team in workspace_teams:
        print(wp_team)
        if user_team_edge.has(email+'_'+wp_team):
            team_rec = teams.get({'_key':wp_team})
            print(team_rec)
            temp_dict = {}
            if team_rec['Disabled_yn'] == False:
                members_list = []
                admins = []
                members = team_rec['Members']
                for k,v in members.items():
                    members_list.append({'username':k.split('@')[0], 'email':k})
                    if 'admin_yn' in v.keys():
                        admins.append({'username':k.split('@')[0], 'email':k})
                for k,v in members.items():
                    print('admin_yn' in v.keys())
                my_teams.append({'team_name':team_rec['Team_name'], 'members_count':len(members.keys()), 'members': members_list, 'admin':admins})

    return my_teams




# def get_user_teams(email: str):
#     """
#     Function to get all teams data of which the user is a member or admin

#     Parameter
#     ---------
#     email :str
#         - The email of the logged in user
    
#     Return
#     ------
#     Return a list of all teams docs
#     """

#     user_teams = []
#     for u in user_team_edge:
#         #if u['_key'].split('_')[0] == email:
#         print(u['_key'])
#         da = u['_key'].split('_')
#         if da[0] == email:
#             print(email)
#             user_teams.append('_'.join(da[1:]))
#     #return [teams.get(u["_key"].split('_')[1]) for u in user_team_edge if u["_key"].split('_')[0]==email]
#     return user_teams

# def check_team_admin(email: str, team: str):
#     if teams.has(team) and user.has(email):
#         MEMBERS = teams.get(team)["Members"]
#         if email in MEMBERS.keys():
#             return MEMBERS[email]["admin_yn"]
#     return False


def check_users_status(members):

    user_presence = True

    for member in members:
        if user.has(member):
            if user.get(member)['active_yn'] == False:
                user_presence = False
                break
        else:
            user_presence=False
            break

    return user_presence



def create_team(data):
    """
    Function just add new team formed in the database
    and connects user in them.

    Parameter
    ---------
    parameter 'data' is the form data from create meeting page.

    Return
    ------
    Returns newly created team.
    """
    #print(data)
    member_dict = dict()
    today = datetime.datetime.now()
    admin = data["admin"]
    mylogger.info(admin)
    organization =get_user_org(admin)
    #print(organization)

    team_key = string_hasher(organization+'_'+data['team_name'])
    # mylogger.info(jsonable_encoder(data))
    if organization is not None:
        try:
            #print(data['team_name'])
            #print(team_key)

            new_team = teams.insert(
                {
                    '_key': team_key,
                    "Team_name": data["team_name"],
                    "Created_at": str(today),
                    "Disabled_yn": False,
                    "Members": member_dict,
                }
            )
            #print("new team is created")
        except:
            #print("not able tocreate it")
            return False
        mylogger.info(organization)
        try:
            org_team.insert({
                "_key":organization+"_"+team_key,
                "_from":f"organization/{organization}",
                "_to":f"teams/{team_key}"
            })
        except:
            #print("duplicate org team insertion...")
            return False
        
        #for team admin
        this_member_dict = {}
        this_member_dict["joined_date"] = str(today)
        this_member_dict["active_yn"] = True
        this_member_dict["admin_yn"] = True
        member_dict[admin] = this_member_dict
        #del this_member_dict['admin_yn']
        try:
            user_team_edge.insert(
                    {"_key": f"{data['admin']}_{team_key}", "_from": f"account/{data['admin']}", "_to": "teams/"+team_key, 'admin_yn':True}
            )
        #print(team_admin)
        except:
            print(data['admin']+" already added")

        for member in data['members']:
            this_member_dict = {}
            this_member_dict["joined_date"] = str(today)
            this_member_dict["active_yn"] = True
            #if member == data['admin']:
            #    this_member_dict['admin_yn'] = True
            #else:
            #this_member_dict["admin_yn"] = False
            member_dict[member] = this_member_dict
            try:
                user_team_edge.insert(
                        {"_key": f"{member}_{team_key}", "_from": f"account/{member}", "_to": "teams/"+team_key}
                )
            except:
                print(f"member {member} already added")

    #print("the new team is")
    #print(new_team['_key'])
    new_team = teams.update({"_key": new_team["_key"], "Members": member_dict})
    #print(new_team)
    #print("updated...")
    
    return True



def get_org_by_workspace(name):
    #mylogger.info(name)

    details = orgs.get(name)
    #mylogger.info(details)
    if details is None:
        return False, None
    return True, details['organization']


def reset_org(email):
    user.update({'_key':email, 'organization': '', 'login_status':False})


def select_workspace_user(workspace: str, email: str):
    if email in get_users_of_org(workspace):
        try:
            user.update({"_key":email, "organization":workspace})
            return True
        except:
            return False
    return False

def delete_team(teamname):
    """
    Function just delete the team but not from the database.
    Rather just update with expired instance.

    Paramter
    --------
    parameter 'teamname' is the name of team to be deleted.

    Return
    ------
    return updated teamdata
    """

    TEAM = teams.get(teamname)
    MEMBER = TEAM["Members"]
    teamnamekey = TEAM["_key"]

    for keys in MEMBER:
        mylogger.info(keys)
        MEMBER[keys]["active_yn"] = False

    return teams.update(
        {
            "_key": teamnamekey,
            "Disabled_yn": True,
            "expired_on": str(datetime.datetime.now()),
            "Members": MEMBER,
        }
    )


def add_team_member(data):
    """
    Function add new member to respective team as filled in form.

    Parameter
    ---------
    parameter 'data' is form data obtained from the form
    for new member.

    Return
    ------
    Returns updated teamdata
    """
    teamname = data["TeamName"]
    membername = data["MemberName"]

    # cursor = db_conn.aql.execute("FOR team IN teams RETURN team")
    # TEAM = [team for team in cursor if team["Team_name"] == teamname]

    if not teams.has(teamname):
        raise ValueError("TEAM {} not registered.".format(teamname))
    elif not user.has(membername):
        raise ValueError("User {} not registered.".format(membername))
    team = teams.get(teamname)
    if team["Disabled_yn"] == True:
        raise ValueError("TEAM {} already inactive.".format(teamname))
    else:
        MEMBER = team["Members"]
        teamnamekey = team["_key"]
        if membername in MEMBER:
            raise ValueError("Member {} already exist.".format(membername))
        else:
            MEMBER[membername] = {
                "joined_date": str(datetime.datetime.now()),
                "active_yn": True,
                "admin_yn": False,
            }

            return teams.update({"_key": teamnamekey, "Members": MEMBER})


def get_team_data(my_org, teamname):
    """
    Get the team members data and return it
    
    Parameter
    ---------
    parameter teamname is the name of the team of which we want the data

    Return
    ------
    Returns team members
    """
    team_key = string_hasher(my_org+'_'+teamname)
    print(team_key)
    print(teams.get(team_key))
    if teams.has(team_key):
        team = teams.get(team_key)
        return team


def get_team_members(my_org, teamname):
    """
    Get the team members data and return it
    
    Parameter
    ---------
    parameter teamname is the name of the team of which we want the data

    Return
    ------
    Returns team members
    """
    team_key = string_hasher(my_org+'_'+teamname)
    if teams.has(team_key):
        team = teams.get(team_key)
        return team["Members"]




def delete_team_member(team_name, members):
    """
    Function remove a member from respective team as filled in form.

    Parameter
    ---------
    parameter 'data' is form data obtained from the form
    for delete member.

    Return
    ------
    Returns updated teamdata
    """
    #teamname = data["TeamName"]
    #membername = data["MemberName"]

    cursor = db_conn.aql.execute("FOR team IN teams RETURN team")
    TEAM = [team for team in cursor if team["Team_name"] == teamname]

    if len(TEAM) == 0:
        raise ValueError("TEAM {} not registered.".format(teamname))
    elif not user.has(membername):
        raise ValueError("User {} not registered.".format(membername))
    elif TEAM[0]["Disabled_yn"] == "Yes":
        raise ValueError("TEAM {} already inactive.".format(teamname))
    else:
        TEAM = TEAM[0]
        MEMBER = TEAM["Members"]
        teamnamekey = TEAM["_key"]
        if membername not in MEMBER:
            raise ValueError("User {} not in Team".format(membername))
        elif MEMBER[membername]["active_yn"] == True:
            if MEMBER[membername]["admin_yn"] == False:
                MEMBER[membername]["active_yn"] = False
                MEMBER[membername]["expired_on"] = str(datetime.datetime.now())
            else:
                raise Exception("Carefull:Team Admin to be deleted.")
                MEMBER[membername]["active_yn"] = False
                MEMBER[membername]["expired_on"] = str(datetime.datetime.now())

            return teams.update({"_key": teamnamekey, "Members": MEMBER})


def delete_team_admin(data):
    """
    Function remove the admin from respective team as filled in form.

    Parameter
    ---------
    parameter 'data' is form data obtained from the form
    for delete admin.

    Return
    ------
    Returns updated teamdata
    """
    teamname = data["TeamName"]

    cursor = db_conn.aql.execute("FOR team IN teams RETURN team")
    TEAM = [team for team in cursor if team["Team_name"] == teamname]

    if len(TEAM) == 0:
        raise ValueError("TEAM {} not registered.".format(teamname))
    elif TEAM[0]["Disabled_yn"] == "Yes":
        raise ValueError("TEAM {} is inactive.".format(teamname))
    else:
        TEAM = TEAM[0]
        MEMBER = TEAM["Members"]
        teamnamekey = TEAM["_key"]
        flag = 1
        for membername in MEMBER:
            if (
                MEMBER[membername]["admin_yn"] == True
                and MEMBER[membername]["active_yn"] == True
            ):
                MEMBER[membername]["admin_yn"] == False
                MEMBER[membername]["active_yn"] = False
                MEMBER[membername]["expired_on"] = str(datetime.datetime.now())
            elif flag == 1 and MEMBER[membername]["active_yn"] == True:
                flag = 0
                MEMBER[membername]["admin_yn"] = True

            return teams.update({"_key": teamnamekey, "Members": MEMBER})


def get_user(key: str):
    """
    This function is to get just the user if exists
    """

    if user.has(key):
        return user.get(key)
    else:
        return None


def get_user_login_cred(email: str):
    """
    This function returns a user from the accouts documents
    if it exists.
    """

    user_dict = next((u for u in user if u["_key"] == email), False)
    if user_dict:
        return user_dict
    else:
        return False

def get_all_user_orgs(username):
    """
    to get all the workspaces for the current user
    @parameters:
    username: email_id of the logged in user.
    @returns:
    returns the list of workspaces/organizations as output.
    """
    mylogger.info(username)
    user_org_status = False

    orgs = [u["_key"].split('_')[0] for u in user_org if username == u['_key'].split('_')[1]]
    #mylogger.info(orgs)
    if len(orgs) > 0:
        return orgs
    return None


def get_user_orgs(username):
    mylogger.info(username)
    user_org_status = False
    
    orgs = [u["_key"].split('_')[0] for u in user_org if username == u['_key'].split('_')[1]]
    #mylogger.info(orgs)
    if len(orgs) > 0:
        return orgs[0]
    return None
   
def get_user_org(email: str):
    cuser = user.get(email)
    if cuser:
        return cuser["organization"]
    else:
        return None

def get_users_of_org(company):
    usernames = [u['_key'].split('_')[1] for u in user_org if company in u['_key']]
    #details = user_org.edges('organization/'+company, direction='in')
    dict_filt = lambda x, y: dict([ (i,x[i]) for i in x if i in set(y)])
    fields = ('email', 'username')
    #meeting_details = dict_filt(meeting_details, fields)
    mylogger.info(usernames)
    #user_details = {dict_filt(user.get(u), fields) for u in usernames }
    user_details = {}
    for u in usernames:
        #mylogger.info(dict_filt(user.get(u)))
        #mylogger.info(user.get(u))
        #mylogger.info(type(user.get(u)))
        detail = user.get(u)
        #mylogger.info(detail)
        user_details[detail['_key']] = detail['_key']
    #mylogger.info(user_details)
    return user_details


def get_workspace_users(company):
    usernames = [u['_key'].split('_')[1] for u in user_org if company in u['_key']]
    #details = user_org.edges('organization/'+company, direction='in')
    dict_filt = lambda x, y: dict([ (i,x[i]) for i in x if i in set(y)])
    fields = ('email', 'username')
    #meeting_details = dict_filt(meeting_details, fields)
    mylogger.info(usernames)
    #user_details = {dict_filt(user.get(u), fields) for u in usernames }
    user_details = []
    admins = get_org_admins(company)
    for u in usernames:
        #mylogger.info(dict_filt(user.get(u)))
        #mylogger.info(user.get(u))
        #mylogger.info(type(user.get(u)))
        detail = user.get(u)
        #mylogger.info(detail)
        if u in admins:
            user_details.append({'username':detail['username'], 'email':detail['_key'],'scope':'admin'})
        else:
            user_details.append({'username':detail['username'], 'email':detail['_key'],'scope':'member'})
    #mylogger.info(user_details)
    return user_details



def linking(data,admin = False):
    print(data)
    email = data['admin'] if 'admin' in data.keys() else data['email']
    print(email)
    user_status = user.has(email)
    print(user_status)
    print(user.get(email))
    if not user_status:
        print(user.get(email))
        print("user not present.....")
        return False


    if(user_org.has(f"{data['organization']}_{email}")):
        print("User already linked with that organization")
    else:
        print("tyring to enter the org_data")
        user_org.insert({
             '_key': f"{data['organization']}_{email}",
            '_from': "account/"+email,
            '_to': "organization/"+data["organization"],
            "admin_yn":admin, "onboarded_yn":True,
         })
        storage_space = orgs.get(data['organization'])["storage_space"]
        orgs.update({"_key":data['organization'],"storage_space":storage_space+1})
        cuser = get_user(email)
        if cuser['organization'] == '' or cuser['organization'] == None:
            user.update({"_key":email,"organization":data["organization"]})
        if get_user_third_party(email)!="custom":
            user.update({"_key":email,"onboarded_yn":True}) #when user is added to organization it means he has completed its onboarding process

        invite_detail = invitations.get(email+'_'+data['organization'])
        if invite_detail is not None:
            print("the invite detail is...")
            print(invite_detail) 
            invitations.update({"_key":invite_detail["_key"],"Joined_yn":True,"Joined_time":str(datetime.datetime.now())})
    return True


def set_onboard(email: str):
    cuser = get_user(email)
    #mylogger.info(f"Have set {email} as on boarded")
    cuser["onboarded_yn"] = True
    user.update(cuser)


# checklist crud function

def make_check_list(username):
    """

    This function just create a vertex for a username provided.
    This vertex contains list of the records of user to do list.
    And  each record will be set of items to be performed.

    The function assumes unique username and make it key of data
    of check_list to be made.

    Parameter
    ---------
    username is the key for that user in DB

    Return
    -------
    Check list will get returned.

    """
    if not user.has(username):
        return False
    
    new_check_list = check_list.insert({'_key':username,'ToDoList':{}})
    return new_check_list

def get_check_list(usercheck_list_key):
    """

    This function just fetches the corresponding vertex for a key
    provided from check list vertex collection.
    If there is no check_list corresponding to that key then a new
    check list will be made using previous function.

    Parameter
    ---------
    usercheck_list_key is the key for that check list in DB

    Return
    -------
    Check list will get returned.

    """
    if not check_list.has(usercheck_list_key):
        found_check_list = make_check_list(usercheck_list_key)
        if found_check_list == False:
            return False
        else:
            return {}

    else:
        found_check_list = check_list.get({'_key':usercheck_list_key})
        return found_check_list['ToDoList']


def add_list_to_record(title,item_record,username):
    """
    
    This function just insert new list to record 'title'
    in the checklist of provided username.

    Parameter
    ---------
    title - title of the record
    item_record - list of all items under this record
    username - username is basically key of user

    Return
    -------
    This function returns the whole checklist of the user
    with key username.

    """
    checklist = get_check_list(username)
    if checklist == False:
        return False
    
    if not title in checklist:
        checklist[title] = item_record
        
    else:
        for item in item_record:
            checklist[title].append(item)
    
    check_list.update({
            '_key':username,
            'ToDoList':checklist
            })
    
    return checklist
    

def add_item_to_record(title,username,item):
    """
    This function just add new item to the record
    of user .

    It assumes title of the record already present 
    in the user checklist.

    Parameter
    ---------
    title - It tells under which record item to be saved
    username - key of user or check list of the user
    item - It is the new item to be inserted in the record

    Return
    -------
    This function returns the whole checklist of the user
    with key username.

    """
    checklist = get_check_list(username)
    if checklist == False:
        return False

    checklist[title].append(item)
    check_list.update({
            '_key':username,
            'ToDoList':checklist
            })
    
    return checklist

    
def delete_user_record(username,title):
    """
    This function deletes a record with key 'title'
    from the user with key username checklist.

    Parameter
    ---------
    title - It tells which record to be deleted
    username - key of user or check list of the user

    Return
    -------
    This function returns the whole checklist of the user
    with key username.

    """
    checklist = get_check_list(username)
    if checklist == False:
        return False
        
    if title in checklist:
        check_list.update({
            '_key':username,
            'ToDoList':str({})
        })
        checklist.pop(title,None)
        check_list.update({
            '_key':username,
            'ToDoList':checklist
        })


    return checklist


def delete_user_record_item(username,title,item):
    """
    This function deletes an item from record with key 'title'
    from the user with key username checklist.

    Parameter
    ---------
    title - It tells item which record to be deleted
    item - It is the item to be deleted
    username - key of user or check list of the user

    Return
    -------
    This function returns the whole checklist of the user
    with key username.

    """
    checklist = get_check_list(username)
    if checklist == False:
        return False
    
    if title in checklist:
        check_list.update({
            '_key':username,
            'ToDoList':str({})
        })
        checklist[title].remove(item)
        check_list.update({
            '_key':username,
            'ToDoList':checklist
        })

    return checklist


def get_profile_data(username):

    if user.has(username):
        user_doc = user.get(username)
    else:
        return False
    return user_doc

# Project CRUD operation







#  connecting Project and teams functions
def connect_team_project(p_key,team_key):
    """
    This function just adds an edge to team_key and p_key.
    """
    if not project.has(p_key):
        raise ValueError(
            "key {} doesnot present in {} vertex collection.. ".format(
                p_key, project
            )
        )
    elif not teams.has(team_key):
        raise ValueError(
            "key {} doesnot present in {} vertex collection.. ".format(
                team_key, teams
            )
        )
    
    else:
        return project_team.insert({
            '_from':'project/'+p_key,
            '_to':'teams/'+team_key
        })



# view upcoming meetings


def create_meeting(data,org_members,meeting_type :str,type_name :str) -> None:
    """
    Create meeting

    Parameter
    ----------
    data : dict format
    org_members : invitees

    Return
    -------
    None
    """
    try:
        status = meetings.insert(
            {
                "hostname": data["hostname"],
                "meeting_subject": data["meeting_subject"],
                "start_time": data["start_time"],
                "end_time": data["end_time"],
                "invitees": org_members,
                "agenda": data["agenda"],
                "link": data["link"],
                # "date": str(
                #     datetime.date(int(data["year"]), int(data["month"]), int(data["date"]))
                # ),
                "date": data['date'],
                "meeting_review": {},
                "meeting_ended": False,
            }
        )

        meeting_key = status["_key"]

        for u in org_members:
            if u!=data["hostname"]:
                user_meeting.insert(
                    {
                        "_key": f"{u}_{meeting_key}",
                        "_from": f"account/{u}",
                        "_to": f"meetings/{meeting_key}",
                        "creator_yn": False,
                        "accepted_yn": False,
                        "date": data['date'],
                    }
                )

        if not user.has(data["hostname"]):
            user.insert({"_key": data["hostname"]})

        user_meeting.insert(
            {
                "_key": f"{data['hostname']}_{meeting_key}",
                "_from": f"account/{data['hostname']}",
                "_to": f"meetings/{meeting_key}",
                "date": data["date"],
                "creator_yn": True,
                "accepted_yn": True,
            }
        )

        if meeting_type == "organization_MEETING":
            org_meetings.insert({
                "_key": f"{type_name}_{meeting_key}",
                "_from": f"organization/{type_name}",
                "_to": f"meetings/{meeting_key}",
                "date": data["date"],
            })
        elif meeting_type == "TEAM_MEETING":
            teams_meetings.insert({
                "_key": f"{type_name}_{meeting_key}",
                "_from": f"teams/{type_name}",
                "_to": f"meetings/{meeting_key}",
                "date": data["date"],
            })
        elif meeting_type == "PROJECT_MEETING":
            project_meetings.insert({
                "_key": f"{type_name}_{meeting_key}",
                "_from": f"project/{type_name}",
                "_to": f"meetings/{meeting_key}",
                "date": data["date"],
            })

    except Exception as e:
        mylogger.info("Exception : ",e)
        return False,""

    return True,meeting_key


def update_meeting(meeting_key:str, data):
    data["_key"] = meeting_key
    meetings.update(data)
    return True



def check_meeting_existence_and_gethost(meeting_key):
    """
    Check meeting exists or not and return host

    Parameter
    ----------
    meeting_key : int

    Return
    -------
    return host if meeting exists else false
    """
    if meetings.has(meeting_key):
        return meetings.get(meeting_key)["hostname"]
    return False

def cmp_date(doc) -> "doc['date']":
    """
    Use as comparator function
    
    Parameter
    ----------
    doc : dict format

    Return
    -------
    doc['date']
    """
    return doc["date"]


def convert_utc_to_local(start_event, end_event):
    start = start_event['dateTime']
    end = end_event['dateTime']
    #mylogger.info(ch.strftime('%s'))
    st_datetime, tz = start.split('+')
    #tz = dt.strptime(tz, '%H:%M')
    end_datetime, tz = end.split('+')
    tz = dt.strptime(tz, '%H:%M')
    st_datetime = dt.strptime(st_datetime, '%Y-%m-%dT%H:%M:%S')
    end_datetime = dt.strptime(end_datetime, '%Y-%m-%dT%H:%M:%S')
    #mylogger.info(st_datetime)
    st_datetime = st_datetime+timedelta(hours=tz.hour, minutes=tz.minute)
    end_datetime = end_datetime+timedelta(hours=tz.hour, minutes=tz.minute)
    #mylogger.info(st_datetime.date())#, st_datetime.time())

    # st_date, st_time = event['dateTime'].split('T')
    # st_date = dt.strptime(st_date, '%Y-%m-%d')
    # st_time, tz = st_time.split('+')
    # st_time = dt.strptime(st_time, '%H:%M:%S').time()
    # tz = dt.strptime(tz, '%H:%M')
    # mylogger.info(st_date, st_time, tz)
    # #seconds = timedelta(hours=tz.hour, minutes=tz.minute).seconds
    # #mylogger.info(seconds)
    # st_time = st_time+timedelta(hours=tz.hour, minutes=tz.minute)# timedelta(seconds=seconds)
    # mylogger.info(st_time)

    # return st_date, st_time
    return st_datetime.date(), st_datetime.time(), end_datetime.date(), end_datetime.time()



#with google calendar
# def get_upcoming_meetings(username):

#     service = get_service(username)
#     from_ = dt.utcnow().isoformat()+'Z'
#     events_result = service.events().list(
#     calendarId='primary', timeMin=from_,
#     #timeMax = to_,
#     maxResults=10, singleEvents=True,
#     orderBy='startTime').execute()


#     mylogger.info(events_result)
#     mylogger.info("the events are....")
#     events = events_result.get('items', [])

#     upcoming_meetings = []
#     for event in events:
#         meet = {}
#         mylogger.info("\n\n")
#         mylogger.info(event)
#         mylogger.info("\n")
#         mylogger.info(event['id'])
#         st_date, st_time, end_date, end_time = convert_utc_to_local(event['start'], event['end'])
#         meet['key'] = event['id']
#         meet['summary'] = event['summary']
#         meet['description'] = event['description']
#         meet['location'] = event['location']
#         meet['creator'] = event['creator']
#         meet['start_date'] = st_date
#         meet['end_date'] = end_date
#         meet['start_time'] = st_time
#         meet['end_time'] = end_time

#         upcoming_meetings.append(meet)
#         mylogger.info(event['summary'])
#         mylogger.info(event['description'])
#         mylogger.info(event['location'])
#         mylogger.info(event['creator'])
#         mylogger.info(event['iCalUID'])
#     upcoming_meetings = sorted(upcoming_meetings, key=lambda x:x['start_date'])
#     return upcoming_meetings




#with database
def get_upcoming_meetings(
    username,
) -> "scheduled meetings data in short and expanded version":
    """                                    
    get all scheduled meetings data
    
    Parameter
    ----------
    username : str

    Return
    -------
    upcoming meetings data in short and expanded version in dict format
    """

    cursor = user_meeting.edges(f"account/{username}", direction="out")["edges"]
    meeting_vertices = [doc["_to"] for doc in cursor if doc["accepted_yn"]]

    meeting_data = [meetings.get(meeting_vertex) for meeting_vertex in meeting_vertices]
    mylogger.info(meeting_data)

    upcoming_meetings = []
    today = str(datetime.date.today())

    for doc in meeting_data:
        if "date" in doc.keys() and today <= doc["date"]:
            # meet = {}
            # meet["key"] = doc["_key"]
            # meet["meeting_subject"] = doc["meeting_subject"]
            # meet["start_time"] = doc["start_time"]
            # meet["end_time"] = doc["end_time"]
            # meet["date"] = doc["date"]
            mylogger.info("Add")
            upcoming_meetings.append(doc)

    upcoming_meetings = sorted(upcoming_meetings, key=cmp_date)

    return upcoming_meetings


def get_meeting_details(username: str, meeting_key: str) -> "meeting details":
    """
    get all details of a specific meetings
        
    Parameter
    ----------
    username : str
    key : str

    Return
    -------
    meetings details in dict format
    """

    meeting_data = graph.traverse(
        start_vertex=f"meetings/{meeting_key}",
        direction="outbound",
        strategy="bfs",
        edge_uniqueness="global",
        vertex_uniqueness="global",
        max_depth=1,
    )

    meeting_doc = None
    action_task_doc = []

    for data in meeting_data["vertices"]:
        if data["_id"].split("/")[0] == "meetings":
            meeting_doc = data
        elif data["_id"].split("/")[0] == "action":

            task_doc = graph.traverse(
                start_vertex=f"action/{data['_key']}",
                direction="outbound",
                strategy="bfs",
                edge_uniqueness="global",
                vertex_uniqueness="global",
                min_depth=1,
                max_depth=1,
            )

            action_task_doc.append((data, task_doc["vertices"]))

    return meeting_doc, action_task_doc

def get_meeting_doc(meeting_key: str):
    return meetings.get(meeting_key)

# meeting_review


def meeting_review(form, username: str, meeting_key: str) -> None:
    """
    submit meeting review

    Parameter
    ----------
    form : review meeting response

    username : str
    
    meeting_key : str

    Return
    -------
    None
    """

    meet = meetings.get(meeting_key)

    meet["meeting_review"][username] = {}
    meet["meeting_review"][username]["rating"] = form["rating"]
    meet["meeting_review"][username]["meeting_summary"] = form["meeting_summary"]

    meetings.update(meet)


# Improve Us


def submit_improve_us(username: str, form, datetime_now) -> None:
    """
    Submit Improve Us Form

    Parameter
    ----------
    username : str
            username of user who is inputing his/her improve us form

    form : starlette request form
            form data of what user has entered
    
    datetime_now : datetime.datetime.now() in str format
                datetime when the user submits the form

    Return
    -------
    None
    """

    status = improve_us.insert(
        {
            "created_at": datetime_now,
            "feedback": form["feedback"],
            "firstname": form["firstname"],
            "surname": form["surname"],
        }
    )

    user_improve_us.insert(
        {
            "_key": username + "_" + status["_key"],
            "_from": "account/" + username,
            "_to": "improve_us/" + status["_key"],
            "created_at": datetime_now,
        }
    )


def cmp_datetime(doc) -> "doc['created_at']":
    """
    Use as comparator function
    
    Parameter
    ----------
    doc : dict format

    Return
    -------
    doc['created_at']
    """

    return doc["created_at"]


def get_improve_us() -> list:
    """
    get user entered improve us forms

    Parameter
    ----------
    None

    Return
    -------
    improve_us_doc : list
                list of docs of user submitted forms in recent to oldest order
    """

    cursor = improve_us.all()
    improve_us_doc = [doc for doc in cursor]
    improve_us_doc = sorted(improve_us_doc, key=cmp_datetime, reverse=True)

    return improve_us_doc


# Pending meetings


def get_pending_meetings(
    username,
) -> "list of pending meetings data in short and expanded version":
    """                                    
    get all pending meetings data
    
    Parameter
    ----------
    username : str

    Return
    -------
    pending meetings data in short and expanded version in dict format
    """

    cursor = user_meeting.edges(f"account/{username}", direction="out")["edges"]
    meeting_vertices = [doc["_to"] for doc in cursor if not doc["accepted_yn"]]

    meeting_data = [meetings.get(meeting_vertex) for meeting_vertex in meeting_vertices]

    pending_meetings = []
    today = str(datetime.date.today())

    for doc in meeting_data:
        if "date" in doc.keys() and today <= doc["date"]:
            meet = {}
            meet["key"] = doc["_key"]
            meet["meeting_subject"] = doc["meeting_subject"]
            meet["start_time"] = doc["start_time"]
            meet["end_time"] = doc["end_time"]
            meet["date"] = doc["date"]

            pending_meetings.append(meet)

    pending_meetings = sorted(pending_meetings, key=cmp_date)

    return pending_meetings



def get_meetinge_details_user_det(username, meeting_key):
    meeting_details = meetings.get(meeting_key)
    user_details = user.get(username)['email_id']
    mylogger.info(meeting_details)

    dict_filt = lambda x, y: dict([ (i,x[i]) for i in x if i in set(y)])
    fields = ('date','start_time', 'end_time', 'meeting_subject', 'agenda', 'link')
    meeting_details = dict_filt(meeting_details, fields)

    start_time = dt.strptime(meeting_details['date']+' '+meeting_details['start_time']+':00', "%Y-%m-%d %H:%M:%S")
    end_time = dt.strptime(meeting_details['date']+' '+meeting_details['end_time']+':00', "%Y-%m-%d %H:%M:%S")
    #summary = data['meeting_subject']
    mylogger.info(start_time, end_time)
    mylogger.info(type(start_time), type(end_time))
    event = create_google_cal_json(meeting_details['meeting_subject'], meeting_details['link'],
        meeting_details['agenda'], start_time, end_time)

    service = get_service()
    service.events().insert(calendarId= user_details, body=event).execute()




    mylogger.info(meeting_details)
    mylogger.info(user_details)




def accept_pending_meetings(username: str, meeting_key: str) -> None:
    """                                    
    use to accept pending meetings data
    
    Parameter
    ----------
    username : str
    
    meeting_key : str

    Return
    -------
    None
    """

    cursor = user_meeting.edges(f"account/{username}", direction="out")["edges"]

    for doc in cursor:
        if doc["_to"] == f"meetings/{meeting_key}" and not doc["accepted_yn"]:
            doc["accepted_yn"] = True
            #get_meetinge_details_user_det(username, meeting_key)
            user_meeting.update(doc)
            break


def reject_pending_meetings(username: str, meeting_key: str) -> None:
    """                                    
    use to reject pending meetings data
    
    Parameter
    ----------
    username : str
    
    meeting_key : str

    Return
    -------
    None
    """

    cursor = user_meeting.edges(f"account/{username}", direction="out")["edges"]

    for doc in cursor:
        if doc["_to"] == f"meetings/{meeting_key}" and not doc["accepted_yn"]:
            user_meeting.delete(doc)
            break


def end_meeting(meeting_key: str) -> bool:
    """
    function to end meeting

    Parameter
    ----------
    meeting_key : str
            meeting_key of the meeting which needs to be ended
    
    Return
    -------
    bool : True if ended successfully else False
    """

    if meetings.has(meeting_key):
        meeting_doc = meetings.get(meeting_key)
    else:
        return False

    meeting_doc["meeting_ended"] = True
    meetings.update(meeting_doc)

    return True


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime.datetime, date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))


def insert_active_hours(active_slot):
    name = active_slot["username"]
    del active_slot["username"]
    active_slot["_key"] = name
    # active_slot['start_time'] = json_serial(active_slot['start_time'])
    # active_slot['end_time'] = json_serial(active_slot['end_time'])
    # mylogger.info(active_slot)
    # mylogger.info(type(active_slot))
    active_slots.insert(active_slot)
    user_active_slots.insert(
        {
            "_key": name + "_" + active_slot["_key"],
            "_from": "account/" + name,
            "_to": "active_slots/" + active_slot["_key"],
        }
    )


def insert_busy_hours(busy_slot):
    """
    busy_slot: contains the start time and
               end_time of a schdule.
    it will contains date and time.
    before inserting it has to check 2 conditions.
    1. Is the selected times are with in the active hours?
    2. and the slot should not be interefered with any other scheduled slots.

    """
    name = busy_slot['username']
    if user.get(name) is not None:
        mylogger.info(busy_slot)
        del busy_slot['username']
        mylogger.info(busy_slot)
        if dt.strptime(busy_slot['meeting_date'], '%Y-%m-%d').date() >= datetime.date.today():

            busy_slot['_key'] = name+'_'+busy_slot['meeting_date']+'_'+busy_slot['start_time'].replace(':', '_')
            mylogger.info(busy_slot['_key'])
            busy_slots.insert(busy_slot)
            user_busy_slots.insert({
                '_key':name+'_'+busy_slot['_key'],
                '_from':'account/'+name,
                '_to':'busy_slots/'+busy_slot['_key']
                })
        else:
            mylogger.info("date is below current date..............")
    else:
        mylogger.info("username not found and busy slots cant be isnerted. please try by creating account.")


def get_active_slots(users):

    mylogger.info(users)
    if not isinstance(users, list):
        users = [users]

    aql = db_conn.aql
    active_slots_list = []
    #to get all the details of list users with a single cursor
    cursor= aql.execute(
        'For doc in active_slots FILTER doc._key in @value RETURN doc',
        bind_vars={'value':users}
    )
    #try:
    dict_filt = lambda x, y: dict([ (i,x[i]) for i in x if i in set(y) ])
    #mylogger.info(list(cursor))
    #fields = ('_key', 'start_time', 'end_time')
    fields = ('start_time', 'end_time')
    #slot = [dict_filt(cur, fields) for cur in cursor]
    for cur in cursor:
        try:
            cur['start_time'] = dt.strptime(cur['start_time'], '%H:%M').time()
            cur['end_time'] = dt.strptime(cur['end_time'], '%H:%M').time()
            active_slots_list.append(list(dict_filt(cur, fields).values()))
        except:
            active_slots_list.append([dt.strptime('00:00', '%H:%M').time()], [dt.strptime('23:59', '%H:%M').time()])
    #mylogger.info(active_slots_list)
    
    return active_slots_list


def get_each_users_busy_slots(users, dates_window, range=False):
    """
    to get all users busy slots userewise and datewise
    """
    aql = db_conn.aql
    dict_filt = lambda x, y: dict([ (i,x[i]) for i in x if i in set(y)])
    fields = ('_key', 'meeting_date','start_time', 'end_time')
    fields = ('start_time', 'end_time')


    busy_slots_dict = {}
    for attendee in users:
        print("the busy slots of........", attendee)
        data = graph.traverse(
            start_vertex = 'account/'+attendee,
            direction='outbound',
            strategy='bfs',
            edge_uniqueness='global',
            vertex_uniqueness='global')
        mylogger.info(data.keys())
        #mylogger.info(data['paths'])
        for da in data['paths']:
            #mylogger.info(da)
            mylogger.info("\n")
            if len(da['edges']) > 0:
                check = da['edges'][0]
                #mylogger.info(check)
                #mylogger.info(type(check))
                #mylogger.info(check.keys())
                if 'busy_slots' in check['_id'] and attendee in check['_key']:
                    #mylogger.info(check)
                    busy_time = busy_slots.get(check['_to'].split('/')[1])
                    #mylogger.info(busy_time)
                    
                    x = dict_filt(busy_time, fields)
                    print(x)
                    if attendee not in busy_slots_dict.keys():
                        busy_slots_dict[attendee] = {busy_time['meeting_date']:[x]}
                    else:
                        if busy_time['meeting_date'] not in busy_slots_dict[attendee].keys():
                            busy_slots_dict[attendee][busy_time['meeting_date']] = [x]
                        else:
                            busy_slots_dict[attendee][busy_time['meeting_date']].append(x)
                    mylogger.info(x)
    return busy_slots_dict




def get_busy_slots(users, dates_window, range=False):
    """
    you have to modify the structure of the record and key_values
    for proper data retrieval
    """
    aql = db_conn.aql
    dict_filt = lambda x, y: dict([ (i,x[i]) for i in x if i in set(y)])
    fields = ('_key', 'meeting_date','start_time', 'end_time')
    fields = ('start_time', 'end_time')


    busy_slots_dict = {}
    for attendee in users:
        print("the busy slots for ....   ", attendee)
        if attendee != '':
            #mylogger.info("attendees")
            #mylogger.info('account/'+attendee)
            data = graph.traverse(
                start_vertex = 'account/'+attendee,
                direction='outbound',
                strategy='bfs',
                edge_uniqueness='global',
                vertex_uniqueness='global')
            #mylogger.info(data.keys())
            #mylogger.info(data['paths'])
            for da in data['paths']:
                #mylogger.info(da)
                #mylogger.info("\n")
                if len(da['edges']) > 0:
                    check = da['edges'][0]
                    #mylogger.info(check)
                    #mylogger.info(type(check))
                    #mylogger.info(check.keys())
                    if 'busy_slots' in check['_id'] and attendee in check['_key']:
                        #mylogger.info(check)
                        busy_time = busy_slots.get(check['_to'].split('/')[1])
                        #mylogger.info(busy_time)
                        
                        x = dict_filt(busy_time, fields)
                        print(x)
                        # if attendee not in busy_slots_dict.keys():
                        #     busy_slots_dict[attendee] = {busy_time['meeting_date']:[x]}
                        # else:
                        #mylogger.info(x)
                        #mylogger.info("mylogger.infoing dates window")
                        #mylogger.info(dates_window)
                        if dt.strptime(busy_time['meeting_date'], '%Y-%m-%d').date() >= min(dates_window) and dt.strptime(busy_time['meeting_date'], '%Y-%m-%d').date() <= max(dates_window):
                            if busy_time['meeting_date'] not in busy_slots_dict.keys():
                                busy_slots_dict[busy_time['meeting_date']] = [list(x.values())]
                            else:
                                busy_slots_dict[busy_time['meeting_date']].append(list(x.values()))
                            #mylogger.info(x)
        
    #mylogger.info("the busy_slots is.......")
    #mylogger.info(busy_slots_dict)
    return busy_slots_dict


def create_action_list(hostname: str, meeting_key: str, data: Dict) -> None:
    """
    create action list for a meeting

    Parameter
    ----------
    hostname : str
            user who created the meeting
    meeting_key : str
            meeting key for which action list needs to be created
    data : Dict
            Dict of action list that needs to be created
    
    Return
    -------
    None
    """

    today = datetime.datetime.now()
    action_doc = action.insert(
        {
            "created_by": hostname,
            "created_date": str(today),
            "title": data["action_title"],
            "description": data["action_description"],
            "expected_date": data["expected_date"],
            "hostname": hostname,
            "assignees":data["assignees"]
        }
    )

    action_key = action_doc["_key"]

    user_action.insert(
        {
            "_key": f"{hostname}_{action_key}",
            "_from": f"account/{hostname}",
            "_to": f"action/{action_key}",
            "created_date": str(today),
        }
    )
    assignees = data["assignees"].split(',')
    nooftasks = len(data["tasks"].split("\r\n"))
    for assignee in assignees:
        notify_user(assignee,f"{nooftasks} New Tasks Assigned",f"{nooftasks} new tasks were assigned to you by {hostname}",f"/api/v1/meeting_details/{meeting_key}",meeting_key)
    for task_data in data["tasks"].split("\r\n"):
        task_data = task_data.split(",")

        task_doc = task.insert(
            {
                "created_by": hostname,
                "created_date": str(today),
                "title": task_data[0],
                "description": task_data[1],
                "label": task_data[2],
                "assigned": assignees,
                "comments": {},
            }
        )

        task_key = task_doc["_key"]

        if len(assignees) >= 1:
            for person in assignees:
                user_task.insert(
                    {
                        "_key": f"{task_key}_{person}",
                        "_from": f"account/{person}",
                        "_to": f"task/{task_key}",
                        "accepted_date": str(today),
                    }
                )

        action_task.insert(
            {
                "_key": f"{action_key}_{task_key}",
                "_from": f"action/{action_key}",
                "_to": f"task/{task_key}",
                "created_date": str(today),
            }
        )

    meeting_action.insert(
        {
            "_key": f"{meeting_key}_{action_key}",
            "_from": f"meetings/{meeting_key}",
            "_to": f"action/{action_key}",
            "created_date": str(today),
        }
    )


def delete_action_list(meeting_key: str, action_key: str) -> None:
    """
    function to delete action list from the meeting

    Parameter
    ----------
    action_key : str
            action_key of the action which is to be deleted
    meeting_key : str
            meeting_key of the meeting from which action list is to deleted

    Return 
    -------
    None
    """

    if action.has(action_key):
        action_task_doc = action_task.edges(f"action/{action_key}", direction="out")[
            "edges"
        ]
        action_doc = action.get(action_key)
        for assignee in action_doc["assignees"].split(","):
            notify_user(assignee,
                f"{action_doc['title']} Action Deleted",
                f"{action_doc['title']} action and the tasks in it were deleted by {action_doc['created_by']}",
                f"/api/v1/meeting_details/{meeting_key}",
                meeting_key)
        for doc in action_task_doc:
            delete_task(action_key, doc["_to"].split("/")[1])

        action.delete(action_key)

    if meeting_action.has(f"{meeting_key}_{action_key}"):
        meeting_action.delete(f"{meeting_key}_{action_key}")


def add_task(username: str, action_key: str, data: Dict) -> None:
    """
    Add task to the action list in the db

    Parameter
    ----------
    username : str
            username of user who is adding the task
    action_key : str
            action_key of the action to which this task is added

    Return
    -------
    None
    """

    today = datetime.datetime.now()
    assignees = data["assignees"].split(',')
    nooftasks = len(data["tasks"].split("\r\n"))
    meeting_key = [u["_key"].split('_')[0] for u in meeting_action if u["_key"].split('_')[1]==action_key][0]
    for assignee in assignees:
        notify_user(assignee,
            f"{nooftasks} New Tasks Assigned",
            f"{nooftasks} new tasks were assigned to you by {hostname}",
            f"/api/v1/meeting_details/{meeting_key}",
            meeting_key)
    for task_data in data["tasks"].split("\r\n"):
        task_data = task_data.split(",")

        task_doc = task.insert(
            {
                "created_by": hostname,
                "created_date": str(today),
                "title": task_data[0],
                "description": task_data[1],
                "label": task_data[2],
                "assigned": assignees,
                "comments": {},
            }
        )

        task_key = task_doc["_key"]

        if len(assignees) >= 1:
            for person in assignees:
                user_task.insert(
                    {
                        "_key": f"{task_key}_{person}",
                        "_from": f"account/{person}",
                        "_to": f"task/{task_key}",
                        "accepted_date": str(today),
                    }
                )

        action_task.insert(
            {
                "_key": f"{action_key}_{task_key}",
                "_from": f"action/{action_key}",
                "_to": f"task/{task_key}",
                "created_date": str(today),
            }
        )


def modify_task(username: str, task_key: str, data: Dict) -> None:
    """
    Modify a task in the action list

    Parameter
    ----------
    action_key : str
            action_key of the action from which task is to be modified
    task_key : str
            task_key of the task which needs to be modified

    Return
    -------
    None
    """

    modified_task = data["modified_task"].split(",")

    if task.has(task_key):
        task_doc = task.get(task_key)

    if modified_task[0]:
        task_doc["title"] = modified_task[0]
    if modified_task[1]:
        task_doc["description"] = modified_task[1]
    if modified_task[2]:
        task_doc["label"] = modified_task[2]
    for assignee in task_doc["assigned"]:
        notify_user(assignee,
            f"{task_doc['title']} Task was modified",
            f"{task_doc['title']} task was modified by {task_doc['created_by']}",
            f"/api/v1/homepage")
    task.update(task_doc)


def delete_task(action_key: str, task_key: str) -> bool:
    """
    Delete task from the action list

    Parameter
    ----------
    action_key : str
            action_key of the action from which task is to be deleted
    task_key : str
            task_key of the task which needs to be deleted

    Return
    -------
    bool : True if task deleted, False if no task found
    """

    if task.has(task_key):
        user_task_doc = user_task.edges(f"task/{task_key}", direction="in")["edges"]
        for doc in user_task_doc:
            user_task.delete(doc)
        task_doc = task.get(task_key)
        meeting_key = [u["_key"].split('_')[0] for u in meeting_action if u["_key"].split('_')[1]==action_key][0]
        for assignee in task_doc["assigned"]:
            notify_user(assignee,
                f"{task_doc['title']} Task Deleted",
                f"{task_doc['title']} task was deleted by {task_doc['created_by']}",
                f"/api/v1/meeting_details/{meeting_key}",
                meeting_key)
        task.delete(task_key)
    else:
        return False

    if action_task.has(f"{action_key}_{task_key}"):
        action_task.delete(f"{action_key}_{task_key}")
    else:
        return False

    return True


def add_comment_task(user: str,comment: str, task_key: str):
    """
    function to add comment to task
    
    Parameter
    ----------
    user : str
            user who is commenting
    comment : str
            comment which user wants to add
    task_key : str
            task_key of the task which is to be assigned to the users

    Return
    -------
    bool : True if comments are added to the task else False
    """
    today = str(datetime.datetime.now())
    task_doc = task.get(task_key)
    task_doc["comments"][today] = [user,comment]
    for assignee in task_doc["assigned"]:
        notify_user(assignee,
            f"{user} commented on your task",
            f"{user} commented on your task titled {task_doc['title']}",
            f"/api/v1/homepage")
    task.update(task_doc)
    return True

def assign_task(usernames: List, task_key: str):
    """
    function to assign action to team members
    
    Parameter
    ----------
    usernames : List
            List of username of users who are going to assign the action to themselves
    task_key : str
            task_key of the task which is to be assigned to the users

    Return
    -------
    bool : True if all users are assigned the task else False
    """

    today = datetime.datetime.now()

    task_doc = task.get(task_key)

    for username in usernames:
        if not username in task_doc["assigned"]:
            task_doc["assigned"].append(username)

            user_task.insert(
                {
                    "_key": f"{task_key}_{username}",
                    "_from": f"account/{username}",
                    "_to": f"task/{task_key}",
                    "accepted_date": str(today),
                }
            )
        else:
            return False
    for assignee in usernames:
        notify_user(assignee,
            f"{task_doc['title']} Task was assigned to you",
            f"{task_doc['title']} task was assigned to you",
            f"/api/v1/homepage")
    task.update(task_doc)
    return True


def unassign_task(usernames: List, task_key: str) -> bool:
    """
    function to unassign task to the users

    Parameter
    ----------
    usernames : List
            username of users to unassign the task
    action_key : str
            action_key of the action from which users are unassigned
    task_key : str
            task_key of the task from which users are unassigned

    Return
    -------
    bool : True if task succeeded else False
    """

    task_doc = task.get(task_key)

    for username in usernames:
        if username in task_doc["assigned"]:
            task_doc["assigned"].remove(username)
            user_task.delete(f"{task_key}_{username}")
        else:
            return False
    for assignee in usernames:
        notify_user(assignee,
            f"You were unassigned of {task_doc['title']} Task",
            f"You were unassigned of {task_doc['title']} task",
            f"/api/v1/homepage")
    task.update(task_doc)
    return True


def get_all_org_tasks(email: str, org: str):
    """
    Function to get all tasks of a user assigned in the current org
    """
    user_org_all_meetings = get_all_user_org_meetings(email, org)
    user_org_tasks_id = { m:[t["_key"].split('_')[1] for a in meeting_action if m==a["_key"].split('_')[0] for t in action_task if t["_key"].split('_')[0]==a["_key"].split('_')[1] if email in task.get(t["_key"].split('_')[1])['assigned']] for m in user_org_all_meetings }
    task_details = { tid:[task.get(t) for t in user_org_tasks_id[tid]] for tid in user_org_tasks_id}
    return task_details

def get_all_org_decisions(email: str, org: str):
    """
    Function to get all tasks of a user assigned in the current org
    """
    user_org_all_meetings = get_all_user_org_meetings(email, org)
    user_org_decision_id = { m:[d["_key"].split('_')[1] for d in meeting_decision if d["_key"].split('_')[0]==m] for m in user_org_all_meetings}
    decision_details = { did:[decision.get(d) for d in user_org_decision_id[did]] for did in user_org_decision_id}
    return decision_details

def get_all_user_org_meetings(email: str, org: str):
    """
    Function to get all tasks of a user assigned in the current org
    """
    org_projects = [p["_key"].split('_')[0] for p in project_org if p["_key"].split('_')[1]==org]
    user_projects = [p["_key"].split('_')[0] for p in project_members if p["_key"].split('_')[1]==email]
    user_org_projects = list(set(org_projects).intersection(set(user_projects)))
    user_org_project_meetings = [u["_key"].split('_')[1] for project in user_org_projects for u in project_meetings if u["_key"].split('_')[0]==project]

    org_teams = [t["_key"].split('_')[1] for t in org_team if t["_key"].split('_')[0]==org]
    user_teams = [t["_key"].split('_')[1] for t in user_team_edge if t["_key"].split('_')[0]==email]
    user_org_teams = list(set(org_teams).intersection(set(user_teams)))
    user_org_teams_meetings = [u["_key"].split('_')[1] for team in user_org_teams for u in teams_meetings if u["_key"].split('_')[0]==team]

    user_org_meetings = [u["_key"].split('_')[1] for u in org_meetings if u["_key"].split('_')[0]==org]

    user_org_all_meetings = user_org_project_meetings + user_org_teams_meetings + user_org_meetings
    return user_org_all_meetings

def insert_file(
    filename : str,
    filetype : str,
    filepath : str,
    file_relation : str
):
    """
    Function for inserting a new file.
    """
    if Files.has(filename):
        raise DuplicateKeyError(
            "key {} is already present in {} vertex collection.. ".format(
                filename, Files
            )
        )
    else:
        new_file_inserted = Files.insert(
            {
                "filename" :  filename,
                "filetype" : filetype,
                "filepath" : filepath,
                "file_relation" : file_relation
            }
        )

    return new_file_inserted
# decision task
def insert_decision(meeting_key: str, data: List, creator_name: str) -> bool:
    """
    function to insert decisions of a completed meeting

    Parameter
    ----------
    meeting_key : str
            meeting_key of the meeting in which we are going to insert
            decision
    data : List

    creator_name : str
            name of the creator who is inserting the new decision

    Return
    -------
    bool : True if insertion is successful else False
    """

    # if a meeting has decision edge then get its key else create a new one
    edge_doc = meeting_decision.edges(f"meetings/{meeting_key}", direction="out")[
        "edges"
    ]

    decision_key = None
    for doc in edge_doc:
        if doc["_to"].split("/")[0] == "decision":
            decision_key = doc["_to"].split("/")[1]
            break

    if decision_key is not None:
        decision_doc = decision.get(decision_key)
    else:
        decision_doc = decision.insert(
            {"decision": jsonable_encoder({}), "comments": jsonable_encoder({})}
        )
        decision_key = decision_doc["_key"]
        meeting_decision.insert(
            {"_from": f"meetings/{meeting_key}", "_to": f"decision/{decision_key}"}
        )
        decision.update(decision_doc)

    decision_doc = decision.get(decision_doc["_key"])
    for decisions in data:
        doc = {}
        doc["created_by"] = creator_name
        doc["liked_by"] = []
        doc["downvoted_by"] = []
        decision_doc["decision"][str(decisions)] = jsonable_encoder(doc)

    decision.update(decision_doc)

    return True


def delete_decision(meeting_key, decision_key: str, decision_name: str) -> bool:
    """
    function to delete a decision

    Parameter
    ----------
    decision_key : str
            decision_key of the decision from which we need
            to delete the decision
    decision_name : str
            decision name which needs to be deleted
    
    Return
    -------
    bool : True if decision is deleted successfully else False
    """

    if decision.has(decision_key):
        decision_doc = decision.get(decision_key)
    else:
        return False

    doc = decision_doc["decision"]
    if decision_name in doc.keys():
        del doc[decision_name]
        decision_doc["decision"] = jsonable_encoder(doc)

    decision.delete(decision_doc)
    decision.insert(decision_doc)
    decision_key = decision_doc["_key"]
    meeting_decision.insert(
        {"_from": f"meetings/{meeting_key}", "_to": f"decision/{decision_key}"}
    )

    return True


def get_decision(meeting_key: str) -> Dict:
    """
    function to retrieve all decisions and comments for that meeting

    Parameter
    ----------
    meeting_key : str
            meeting_key of the meeting from which all the
            decisions need to be extracted

    Return
    -------
    dict of all decision and comments
    """

    edge_doc = meeting_decision.edges(f"meetings/{meeting_key}", direction="out")[
        "edges"
    ]

    decision_key = None
    for doc in edge_doc:
        if doc["_to"].split("/")[0] == "decision":
            decision_key = doc["_to"].split("/")[1]
            break

    if decision_key is not None:
        decision_doc = decision.get(decision_key)
    else:
        return {}

    return decision_doc


def modify_decision(
    meeting_key: str, decision_key: str, decision_name: str, data: Dict
) -> bool:
    """
    function to modify a decision in a meeting

    Parameter
    ----------
    meeting_key : str
            meeting_key of the meeting from which
            we are modifying the decision
    decision_key : str
            decision_key of the decision in which
            we have to do modification
    decision_name : str
            decision_name which needs to be modified
    data : Dict
            data which is modified decision name
    
    Return
    -------
    bool : True if modification is successful else False
    """

    if decision.has(decision_key):
        decision_doc = decision.get(decision_key)
    else:
        return False

    if decision_name in decision_doc["decision"].keys():
        decision_doc["decision"][str(data["modified_decision"])] = decision_doc[
            "decision"
        ].pop(decision_name)
    else:
        return False

    decision.delete(decision_doc["_key"])
    decision.insert(decision_doc)
    decision_key = decision_doc["_key"]
    meeting_decision.insert(
        {"_from": f"meetings/{meeting_key}", "_to": f"decision/{decision_key}"}
    )

    return True


def add_upvote(decision_key: str, decision_name: str, username: str) -> bool:
    """
    function to add upvote to the list of upvotes

    Parameter
    ----------
    decision_key : str
            decision_key of the decision in which user wants to upvote
    decision_name : str
            decision_name of the decision to which user wants to upvote
    username : str
            username of the user who is upvoting
    
    Return
    -------
    bool : True if upvoted successfully else False
    """

    decision_doc = decision.get(decision_key)

    if not username in decision_doc["decision"][decision_name]["liked_by"]:
        decision_doc["decision"][decision_name]["liked_by"].append(username)
    else:
        return False

    decision.update(decision_doc)
    return True


def remove_upvote(decision_key: str, decision_name: str, username: str) -> bool:
    """
    function to remove upvote to the list of upvotes

    Parameter
    ----------
    decision_key : str
            decision_key of the decision in which user wants to upvote
    decision_name : str
            decision_name of the decision to which user wants to upvote
    username : str
            username of the user who is upvoting
    
    Return
    -------
    bool : True if upvoted successfully else False
    """

    decision_doc = decision.get(decision_key)

    if username in decision_doc["decision"][decision_name]["liked_by"]:
        decision_doc["decision"][decision_name]["liked_by"].remove(username)
    else:
        return False

    decision.update(decision_doc)
    return True


def add_downvote(decision_key: str, decision_name: str, username: str) -> bool:
    """
    function to add downvote to the list of downvotes

    Parameter
    ----------
    decision_key : str
            decision_key of the decision in which user wants to downvote
    decision_name : str
            decision_name of the decision to which user wants to downvote
    username : str
            username of the user who is upvoting
    
    Return
    -------
    bool : True if downvoted successfully else False
    """

    decision_doc = decision.get(decision_key)

    if not username in decision_doc["decision"][decision_name]["downvoted_by"]:
        decision_doc["decision"][decision_name]["downvoted_by"].append(username)
    else:
        return False

    decision.update(decision_doc)
    return True


def remove_downvote(decision_key: str, decision_name: str, username: str) -> bool:
    """
    function to remove downvote to the list of downvotes

    Parameter
    ----------
    decision_key : str
            decision_key of the decision in which user wants to downvote
    decision_name : str
            decision_name of the decision to which user wants to downvote
    username : str
            username of the user who is upvoting
    
    Return
    -------
    bool : True if downvoted successfully else False
    """

    decision_doc = decision.get(decision_key)

    if username in decision_doc["decision"][decision_name]["downvoted_by"]:
        decision_doc["decision"][decision_name]["downvoted_by"].remove(username)
    else:
        return False

    decision.update(decision_doc)
    return True


def add_comment(decision_key: str, username: str, data: str) -> bool:
    """
    function to add comment to the decision of a meeting

    Parameter 
    ----------
    decision_key : str
            decision_key of the decision to which comment is added
    username : str
            username of the user who is commenting to a decision
    data : str
            comment
    
    Return
    -------
    bool : True if comment added successfully else False
    """

    today = datetime.datetime.now()

    if decision.has(decision_key):
        decision_doc = decision.get(decision_key)
    else:
        return False

    decision_doc["comments"][str(today)] = jsonable_encoder({})
    decision_doc["comments"][str(today)][username] = data

    decision.update(decision_doc)
    return True


############# MEETING NOTES #################
# Author: Priyav

def add_notes(meeting_key:str, path:str) -> bool:
    """
    function to add notes to the meeting

    Parameter 
    ----------
    meeting_key : str
            meeting_key of the meeting to which note is added
    path : str
            path to notes
    
    Return
    -------
    bool : True if notes added successfully else False
    """
    if meetings.has(meeting_key):
        meeting_doc = meetings.get(meeting_key)
    else:
        return False
    
    meeting_doc["notespath"] = jsonable_encoder({})
    meeting_doc["notespath"] = path

    meetings.update(meeting_doc)
    return True


def check_host(givenhost:str,meeting_key:str) -> bool:
    """
    function to check givenhost is host of the meeting

    Parameter 
    ----------
    meeting_key : str
            meeting_key of the meeting
    
    Return
    -------
    bool : True if host is correct else False
    """
    if meetings.has(meeting_key):
        meeting_doc = meetings.get(meeting_key)
    else:
        return False
    try:
        if meeting_doc["hostname"]==givenhost:
            return True
        else:
            return False
    except:
        return False


def check_invitee(givenuser:str,meeting_key:str) -> bool:
    """
    function to check givenuser is invitee of the meeting

    Parameter 
    ----------
    meeting_key : str
            meeting_key of the meeting
    
    Return
    -------
    bool : True if invitee is correct else False
    """
    if meetings.has(meeting_key):
        meeting_doc = meetings.get(meeting_key)
    else:
        return False
    try:
        if givenuser in meeting_doc["invitees"]:
            return True
        else:
            return False
    except:
        return False


def check_notes(meeting_key:str) -> bool:
    """
    function to check notes exist in the meeting

    Parameter 
    ----------
    meeting_key : str
            meeting_key of the meeting in which notes is checked
    
    Return
    -------
    bool : True if notes exist else False
    """
    if meetings.has(meeting_key):
        meeting_doc = meetings.get(meeting_key)
    else:
        return False
    try:
        if meeting_doc["notespath"]:
            return True
        else:
            return False
    except:
        return False

def meeting_going_on(meeting_key:str) -> bool:
    """
    function to check meeting is going on or not

    Parameter 
    ----------
    meeting_key : str
            meeting_key of the meeting
    
    Return
    -------
    bool : True if meeting is going on else False
    """
    if meetings.has(meeting_key):
        meeting_doc = meetings.get(meeting_key)
    else:
        return False
    # try:
    meetingstarttime = datetime.datetime.strptime(meeting_doc["date"]+" "+meeting_doc["start_time"],"%Y-%m-%d %H:%M")
    meetingendtime = datetime.datetime.strptime(meeting_doc["date"]+" "+meeting_doc["end_time"],"%Y-%m-%d %H:%M")
    mylogger.info(meetingstarttime,meetingendtime)
    if (datetime.datetime.now()>=meetingstarttime and datetime.datetime.now()<=meetingendtime):
        return True
    else:
        return False
    # except:
    #     return False

def get_path_notes(meeting_key:str) -> str:
    """
    function to check meeting is going on or not

    Parameter 
    ----------
    meeting_key : str
            meeting_key of the meeting
    
    Return
    -------
    str : path if meeting is going on else ""
    """
    if meetings.has(meeting_key):
        meeting_doc = meetings.get(meeting_key)
    else:
        return ""
    host = meeting_doc["hostname"]
    if user.has(host):
        user_doc = user.get(host)
    else:
        return ""
    organization = user_doc["organization"]
    # project = user_doc["project"]
    project = "1on1meetings"
    # team = user_doc["team"]
    team = "backend"
    return f"{organization}\\{project}\\{team}"



######################################################
#################organization Ops#####################

def create_org(data,email: str):
    cuser = user.get(email)
    if cuser['active_yn']==False or current_time.time() - cuser['login_otp_gen_time'] > 900:
        user.delete({"_key": email})
        return False
    data['_key'] = data['organization']
    data['storage_space'] = 1
    data['used_space'] = 0
    data['since'] = dt.strftime(data['since'] if 'since' in data.keys() else date.today(), '%Y-%m-%d')
    if not orgs.has(data['_key']):
        org = orgs.insert(data)
    
    return True

def check_org_admin(org: str, email: str):
    """
    while creating the project, team etc., we will this function as admin checker. If it returns none, then the user cant create  anything.
    """
    if user_org.has(f"{org}_{email}"):
        return user_org.get(f"{org}_{email}")["admin_yn"]

def check_team_admin(team_name: str, email: str):
    """
    Here we check if he user has access by checking the user's admin status.
    if the user(email) is not an admin in org or team, it returns None
    """
    org = get_user_org(email)
    org_admin_yn = check_org_admin(org, email)
    team_admin_yn = False
    team_key=str(hash(org+'_'+team_name)%100000000)
    if user_team_edge.has(f"{team_key}_{email}"):
        team_admin_yn = user_team_edge.get(f"{team_key}_{email}")["admin_yn"]

    if org_admin_yn or team_admin_yn:
        return True
    return None


def check_project_admin(project_name: str, email: str):
    """
    Here we check if he user has access by checking the user's admin status.
    if the user(email) is not an admin in org or project, it returns None
    """
    org = get_user_org(email)
    org_admin_yn = check_org_admin(org, email)
    project_admin_yn = False
    project_key = string_hasher(org+'_'+project_name)

    if user_project_edge.has(f"{project_key}_{email}"):
        project_admin_yn = user_project_edge.get(f"{project_key}_{email}")["admin_yn"]
    if org_admin_yn or project_admin_yn:
        return True
    return None




def adding_members_to_org(org_name, members):
    for member in members:
        try:
            user_org.insert({'_key':org_name+'_'+member, "_from": f"organization/{org_name}", 
                "_to": f"account/{member}", 'admin_yn':False})
        except Exception as e:
            mylogger.info(e.message)
            return DuplicateKeyError(
            "key {} is already present in {} vertex collection.. ".format(
                member, org_name
            )
        )


# def add_user_to_invitation(admin:str,emaillist:list):
#     try:
#         status = user_invitations.insert({"_key":f"{admin}_{invite}","_from":f"account/{admin}","_to":f"invitations/{invite}"})


        


def deleting_members_to_org(org_name, members):
    for member in members:
        #try:
        user_org.delete('works_at/'+org_name+'_'+member)
        # except Exception as e:
        #     mylogger.info(e.message)

def add_user_invitation(admin:str,emaillist:list, organization: str):
    keys = {}
    for email in emaillist:
        #mylogger.info(email)
        #mylogger.info(type(time.time()))
        try:
            invite = invitations.insert({'_key': email+'_'+organization,
            "Inviter_User_ID":admin,
            "Invitee_email":email,
            "Joined_yn":False,
            "Joined_time":"",
            })
            #mylogger.info(invite)
        except Exception as e:
            print(email+"_"+organization+" already added to invitations table")
        invite = email+"_"+organization
        try:
            user_invitations.insert({"_key":admin+"_"+email+"_"+organization,"_from":f"account/{admin}","_to":f"invitations/{invite}"})
        except Exception as e:
            print("e -->", "user_already_present")
        keys[email] = invite
    return keys



############# AUTHENTICATE AND CREATE USER #################
# Author: Akshat(akshat.dak@students.iiit.ac.in)
# maintainer: Krishna

def authenticate_user_email(email: str):
    """
    """

    if user.has(email):
        return user.get(email)

def create_user(id_info: Dict):
    """
    """

    if not user.has(id_info['email']):
        user_result = user.insert({
            "_key": id_info['email'],
            'email': id_info['email'],
            'password': id_info['password'],
            "email_verified": False,
            'position': id_info['position'],
            'since' : dt.strftime(date.today(), '%Y-%m-%d'),
            "username": id_info['username'],
            "picture": id_info['picture'] if 'picture' in id_info.keys() else None,
            "organization": id_info['organization'],
            #"onboarded_yn":False,
            "login_status":False,
            "active_yn": False,
        })

        #new_user = user.get(id_info['email'])

        #mylogger.info("in crud.py new user = ", user_result)
        return user_result



############# AUTHENTICATE USING EMAIL AND PASSWORD #################
# Author: Priyav
# Co-author: Sejal

def authenticate_user_username_password(username: str,password: str):
    """
    Authenticate user using username and password
    """
    mylogger.info(username)
    if user.has(username):
        curr_user = user.get(username)
    else:
        return None
    mylogger.info(curr_user)
    if(password == curr_user["password"]):
        return curr_user
    else:
        return None


def get_add_user_calendar(email: str, calendar: str = "", access_token: str = "", refresh_token: str = ""):
    """
    Add a user calendar details to vertex
    """
    cal_status = user_calendar.has(email)
    if cal_status and access_token=="":
        return user_calendar.get(email)
    elif not cal_status:
        #if access_token=="":
        #return None
        user_calendar.insert({"_key": email,"cal_type": calendar,"access_token": access_token,"refresh_token": refresh_token})
        return user_calendar.get(email)
    else:
        user_calendar.update({"_key":email,"access_token": access_token})

def email_successfully_verified(email: str):
    if user.has(email):
        curr_user = user.get(email)
        if curr_user["email_verified"]:
            return False
        else:
            curr_user["email_verified"] = True
            curr_user["active_yn"] = True
            user.update(curr_user)
            return True
    else:
        return False

############# AUTHENTICATE USING MICROSOFT #################
# Author: Priyav

def create_user_microsoft(username: str, email: str, oid: str):
    if not user.has(email):
        domain = email.split('@')[1].split('.')[0]
        user.insert({
            "_key": email,
            "email_verified": True,
            "domain_type":domain,
            "full_name": username,
            "onboarded_yn":False,
            "active_yn":False,
            'recovery_passcode':"",
            'recovered_yn':True
        })
        if domain!='outlook':
            user.update({"_key":email,"outlook_mail":f'outlook_{oid.upper()}@outlook.com'})
        new_user = user.get(email)
        return new_user
    return user.get(email)

############# THIRD PARTY STUFF #################
# Author: Priyav

def add_third_party(email: str, party: str):
    if not user_third_parties.has(email):
        user_third_parties.insert({"_key":f"{email}","_from":f"account/{email}", "_to":f"third_party_creds/{party}"})

def get_user_third_party(email: str):

    device = list(pf.uname())
    
    mylogger.info(device)
    if user_third_parties.has(email):
        return user_third_parties.get(email)["_to"].split("/")[1]
    else:
        return "custom"
    
    #if login_devices.has(f"{email}_{device[2]}"):
        #return login_devices.get(f"{email}_{device[2]}")["login_type"]

def logged_in(email: str, log_type: str, request):
    cuser = get_user(email)
    device = list(pf.uname())
    platform_keys = ['device_type', 'device_name', 'release', 'version', 'machine', 'processor']
    device_details =dict(zip(platform_keys, device)) 
    device_details['ip_address'] = gethostbyname(gethostname())
    device_details['mac_address'] = get_mac_address()
    device_details['_key'] = email+'_'+device_details['device_name']
    device_details['login_time'] = str(datetime.datetime.now())
    device_details["login_type"] = log_type
    device_details["login_status"] = True
    user_agent = parse(request.headers["user-agent"])
    device_details["browser"] = user_agent.browser.family+user_agent.browser.version_string

    if cuser:
        if login_devices.has(device_details['_key']):
            #login_devices.update({"_key":device_details['_key'],"device_name":device,"login_time":f"{datetime.datetime.now()}","login_type":log_type})
            login_devices.update(device_details)
        else:
            #login_devices.insert({"_key":device_details['_key'],"device_name":device,"login_time":f"{datetime.datetime.now()}","login_type":log_type})
            login_devices.insert(device_details)
        cuser["login_status"] = True
        user.update(cuser)
        if cuser["login_status"] == True:
            missed_notify_user(email)

def logged_out(email: str):
    cuser = get_user(email)
    device = list(pf.uname())
    if cuser:
        if login_devices.has(f"{email}_{device[2]}"):
            login_devices.update({"_key":f"{email}_{device[2]}","login_status":False})


######################################################################################
############################### MEETING AGENDA CRUD ##################################
######################################################################################

# Author : Priyav Kaneria

def get_meeting_agendas(meeting_key: str):
    """
    Get the sub-agendas of the meeting
    """
    return [agenda.get(a["_key"].split('_')[1]) for a in meeting_agenda if a["_key"].split('_')[0]==meeting_key]

def add_meeting_agenda(meeting_key:str, data: dict):
    """
    add the agenda
    """
    if meetings.has(meeting_key):
        agenda_doc = agenda.insert({
            "assignee":data["assignees"],
            "Links":"",
            "Comments":"",
            "created_timestamp":str(datetime.datetime.now()),
            "time_required": data["time_required"],
            "agenda_desc": data["new_agenda"]
        })
        meeting_agenda.insert({
            "_key":f"{meeting_key}_{agenda_doc['_key']}",
            "_from":f"meetings/{meeting_key}",
            "_to":f"agenda/{agenda_doc['_key']}"
        })
    return True

def modify_meeting_agenda(agenda_key:str, data: dict):
    """
    modify the agenda
    """
    if agenda.has(agenda_key):
        agenda_doc = agenda.get(agenda_key)
        agenda_doc["assignee"] = data["assignees"]
        agenda_doc["time_required"] = data["time_required"]
        agenda_doc["agenda_desc"] = data["modified_agenda"]
        agenda.update(agenda_doc)
    return True



######################################################################################
############################# PROJECT CRUD FUNCTIOS  #################################
######################################################################################


def get_org_projects(workspace):
    cursor = aql.execute(
        'FOR doc in project_org FILTER doc._to == @value RETURN doc',
        bind_vars={'value':'organization/'+workspace}
    )

    projects = [ rec['_from'].split('/')[1] for rec in cursor]
    return projects


def get_project_key(workspace, project_name):
    org_projects= get_org_projects(workspace)

    for org_project in org_projects:
        if project.has(org_project):
            if project.get(org_project)['project_data']['title'] == project_name:
                #break;
                return org_project

    return None


def get_project_data(my_org, projectname):
    """
    Get the project data and return it
    
    Parameter
    ---------
    parameter name of project

    Return
    ------
    Returns project info
    project_details_dict contains....
    @FieldType: field type of project
    @ProjectName : name of project
    @ExpectedEndDate: end date of the project
    @ProjectDescription: description of project
    @StartedOn: start date of project
    @ActiveYN: boolean stating project is active or not
    @Members: list of project members
    """
    project_details={}
    #project_key = string_hasher(my_org+'_'+projectname)
    org_projects = get_org_projects(my_org)
    #project_key = string_hasher(my_org+'_'+projectname)
    project_key = ''
    for my_project in org_projects:
        detail = project.get(my_project)['project_data']
        if detail['title'] == projectname:
            project_key = my_project
            break
    if project.has(project_key):
        project_data = project.get(project_key)['project_data']#[pm['project_data'] for pm in project if projectname in pm['_key']]
        #members = [pm['_to'].split('/')[1] for pm in project_members if projectname in pm['_key']]
        # project_details = {
        #     "project_data" : project_data,
        #     "members" : members
        # }
        if project_data['active_yn'] == True:
            project_members = []
            for admin in project_data['Admins']:
                member = user.get(admin)
                project_members.append({'email':admin, 'username':member['username']})
            project_data['Admins'] = project_members
        else:
            project_data = None
        return project_data

def get_project_members(projectname):
    """
    Get the project name data return project members list
    
    Parameter
    ---------
    parameter projectname is the name of the project of which we want the data

    Return
    ------
    Returns project members
    """
    if project.has(projectname):
        members = [pm['_to'].split('/')[1] for pm in project_members if projectname in pm['_key']]
        return members


def link_project_to_users(my_org, project_teams, project):
    project_teams = project_teams.split(',')
    members = []
    for team in project_teams:
        team_key = string_hasher(my_org+'_'+team)
        members.extend(list(teams.get(team_key)['Members'].keys()))#get_team_data(team)
        print(members)
        print("printing")
    for member in set(members):
        try:
            user_project.insert({'_key':project+'_'+member, '_from':'project/'+project, '_to':'account/'+member, 'disabled_yn':False})
        except:
            print("already added")




def display_org_projects(org_name):
    """
    Get the organization name and return all projects of that org
    
    Parameter
    ---------
    parameter org_name is the name of the organization of which we want the projects

    Return
    ------
    Returns project names
    """
    for po in project_org:
       if org_name in po['_key']:
           mylogger.info(po['_key'])
   
    projects = [po['_from'].split('/')[1] for po in project_org if org_name in po['_key']]
    projects_list = []
    for project_id in projects:
        #print(project.get(project_id))
        project_data = project.get(project_id)['project_data']
        projects_list.append({'title':project_data['title'], 'created_date':project_data['start_date']})

    return projects_list



def get_user_projects(user, org_projects):
    """
    Get all the projects of current user is part of
    
    Parameter
    ---------
    parameter user that is signed in

    Return
    ------
    Returns all the projects of the user
    """
    # my_projects=[]
    # for my_project in project_members:
    #    if user in my_project['_key']:
    #        key = my_project['_key'].split('_')[0]
    #        curr_project = project.get(key)
    #        mylogger.info(curr_project)
    #        if(curr_project["project_data"]["CurrentAdminEmail"]==user):
    #            curr_project["project_data"]['status'] = 'ADMIN'
    #        else:
    #            curr_project["project_data"]['status'] = 'MEMBER'
    #        my_projects.append(curr_project)
   
    # mylogger.info(my_projects)
    my_projects = []
    for project in org_projects:
        if user_project.has(project+'_'+user):
            my_projects.append(project)

    return my_projects

def create_project(project_data_dict):
    """
    This function takes the data from create_project post
    request in the form dict and store in the data specified.


    Parameter
    ---------
    project_data_dict - The project data dict is this parameter

    Return
    -------
    Return the stored project data from the DB
    """
    project_data_dict = jsonable_encoder(project_data_dict)
    print(project_data_dict)
    project_data_dict["active_yn"]=True
    project_name = project_data_dict['title']
    project_admin_emails =  project_data_dict['Admins']

    org = get_user_org(project_data_dict['created_by'])
    project_key = string_hasher(org+'_'+project_name)
    if project_data_dict['project_code'] is None or project_data_dict['project_code'] == '':
        project_data_dict['project_code'] = project_key
    

    tm = datetime.datetime.now()
    #project_key= project_key

    if project.has(project_key):
        # raise DuplicateKeyError(
        #     "Project {} is already present in {} vertex collection...".format(
        #         project_data_dict['ProjectName'], project
        #     )
        # )
        print('already has projec_key')
        return False

    
    if org is not None or org != '':
        #mylogger.info(org ,project_admin_email)
        p_teams = project_data_dict["teams"]
        p_data = project.insert({
            '_key':project_key,
            'project_data':project_data_dict
        })

        if link_project_org(org,project_key) == False:
            return False
        if link_user_project(org, project_admin_emails,project_key) == False:
            return False
        if link_project_teams(org, p_teams,project_key) == False:
            return False
        link_project_to_users(org,p_teams, project_key)
        return True
    else:
        #mylogger.info("Admin not linked to any organization")
        return None


def check_teams_status(my_org, project_teams):
    # project_teams = project_teams.split(',')
    # team_presence = True
    # for team in project_teams:
    #     #print(team)
    #     #print(teams.get(team))

    #     team_key = str(hash(my_org+'_'+team)%100000000)
    #     #team_key = str(hash(organization+'_'+data['team_name'])%100000000)
    #     print(my_org)
    #     print(team, team_key)
    #     if teams.has(team_key):
    #         if teams.get(team_key)['Disabled_yn'] == True:
    #             team_presence = False
    #             break;
    #     else:
    #         team_presence = False

    # return team_presence
    project_teams= set(project_teams.split(','))
    my_org_teams = set(get_org_teams(my_org))
    print(my_org_teams)
    org_teams_list = []
    for team_key in my_org_teams:
        if teams.has(team_key):
            org_teams_list.append(teams.get(team_key)['Team_name'])

    print(org_teams_list)
    print(project_teams)
    if set(org_teams_list).intersection(project_teams) == project_teams:
        return True
    return False



def link_project_org(org_key,project_key):
    """
    link organization and project
    
    Parameter
    ---------
    parameter:
    org_key is the name of the organization 
    project_key is the name of the project 
    """
    try:
        mylogger.info(org_key+" " +project_key)
        project_org.insert({
                '_key':project_key+"_"+org_key,
                '_from':"project/"+project_key,
                '_to':"organization/"+org_key
            })
        mylogger.info("linked")
        return True
    except:
        return False


def get_team_keys_from_team_names(my_org, new_teams):

    teams_list = []
    for team in new_teams:
        team_key = string_hasher(my_org+'_'+team)
        teams_list.append(team_key)

    return teams_list




def link_project_teams(my_org, new_teams, project_key):
    """
    link project with teams working on it
    
    Parameter
    ---------
    parameter:
    teams are the teams working on the project 
    project_key is the name of the project 
    """
    new_teams = new_teams.split(',')
    teams_list = get_team_keys_from_team_names(my_org, new_teams)
    print(teams_list)
    for team in teams_list:
        print(team, my_org)
        #team_key = str(hash(my_org+"_"+str(team))%100000000)
        #print(team_key)
        #try:
        if not project_team.has(project_key+'_'+team):
            project_team.insert({
                '_key':project_key+'_'+team,
                '_from':"project/"+project_key,
                '_to':"teams/"+team
            })
        #except:
        #    return False

def get_org_admins(org):
    """
    collects the admins of the given org 
    params:
    ------------------------------------
    input:
    org: organization name
    output:
    """
    cursor = aql.execute(
        'FOR doc in user_org FILTER doc._to == @value RETURN doc',
        bind_vars={'value':'organization/'+org}
    )

    admins = [ rec['_from'].split('/')[1] for rec in cursor if rec['admin_yn'] == True]
    return admins


def link_user_project(my_org, user_keys,project_key):
    """
    link project and user
    
    Parameter
    ---------
    parameter:
    user_key is the username of the current user 
    project_key is the name of the project 
    """
    try:
        for user_key in user_keys:
            mylogger.info(user_key+" " +project_key)
            project_members.insert({
                    '_key':project_key+"_"+user_key,
                    '_from':"project/"+project_key,
                    '_to':"account/"+user_key,
                    'admin_yn':True
                })
            mylogger.info("linked")
        return True
    except:
        return False

def modify_project_admin(project_name,data,user):
    """
    modify the project admin
    
    Parameter
    ---------
    parameter:
    user is the username of the current user 
    project_name is the name of the project 
    data is the name of new project admin

    Logic:First of all only the current admin
    has authority to change project admin.
    If current user is admin then the field @CurrentProjectAdmin
    is updated.
    """
    curr_project = project.get(project_name)
    mylogger.info(curr_project)
    adminsList = [user,data["admin"]]
    if curr_project["project_data"]["CurrentAdminEmail"] == user:
        project.update({
        "_key":project_name,
        "project_data":{
            "Admins":adminsList,
            "CurrentAdminEmail":data["admin"]
         }
        
     })
        mylogger.info("changed")
    else:
        mylogger.info("You are not authorized to change admin")
    
   
    return None


def delete_project(project_name):
    """
    This function deletes (basically update with
    DeletedOn key) the project's last instance from the DB.

    And insert it as a new document in the collection
    and delete the actual key data i.e.whole doc.

    Parameter
    ---------
    project_name:str
    This function takes the project name as parameter
    to delete that project's last instance data from the DB.

    Return
    -------
    Return the stored project data from the DB
    """

    p_key = project_name 

    if not project.has(p_key):
        raise ValueError(
            "key {} doesnot present in {} vertex collection.. ".format(
                project_name, project
            )
        )
    
    p_doc = project.get(p_key)
    p_doc['project_data']['DeletedOn'] = str(datetime.datetime.now())
    p_doc['project_data']['ActiveYN'] = "No"

    p_data = project.update({
        '_key':p_key,
        'project_data':p_doc['project_data']
    })

    return p_data


def modify_project(project_dict, project_key):
    """
    This function modifies the project's 
    last instance from the DB.

    Parameter
    ---------
    project_data_dict:
    This function takes the project data dict as parameter
    to modify that project's last instance data from the DB.
    project_data_dict contains....
    @FieldType: field type of project
    @ProjectName : name of project (unchangeable)
    @ExpectedEndDate: end date of the project
    @ProjectDescription: description of project
    @StartedOn: start date of project
    @ActiveYN: boolean stating project is active or not

    Return
    -------
    Return the stored project data from the DB
    """
    #project_data_dict = jsonable_encoder(project_data_dict)
    #project_key = project_data_dict['ProjectName']
    final_update = {}
    print(project_dict)

    if project.has(project_key):
        try:
            p_doc = project.get(project_key)['project_data']
            print("the details is")
            print(p_doc)

            if project_dict['description'] is not None:
                final_update['description'] = project_dict['description']
            else:
                final_update['description'] = ''

            if project_dict['project_type'] is not None:
                final_update['project_type'] =project_dict['project_type']
            else:
                final_update['project_type'] = p_doc['project_type']

            if project_dict['title'] is not None:
                final_update['title'] = project_dict['title']
            else:
                final_update['title'] = p_doc['title']

            if project_dict['start_date'] is not None:
                final_update['start_date'] = str(project_dict['start_date'])
            else:
                final_update['start_date'] = p_doc['start_date']

            if project_dict['end_date'] is not None:
                final_update['end_date'] = str(project_dict['end_date'])
            else:
                final_update['end_date'] = p_doc['end_date']

            if project_dict['teams'] is not None:
                if project_dict['teams'] != p_doc['teams']:
                    teams_det = ','.join(list(set((p_doc['teams']+','+project_dict['teams']).split(','))))
                    #teams_det.extend(p_doc['teams'].split(','))
                    final_update['teams'] = teams_det
                else:
                    final_update['teams'] = ','.join(list(set(p_doc['teams'].split(','))))

            else:
                final_update['teams'] = ','.join(list(set(p_doc['teams'].split(','))))

            if project_dict['project_code'] is not None:
                final_update['project_code'] = project_dict['project_code']
            else:
                final_update['project_code'] = p_doc['project_code']


            if project_dict['color'] is not None:
                final_update['color'] = project_dict['color']
            else:
                final_update['color'] = p_doc['color']

            #project_data_dict.pop('ProjectName')
            project.update({
                '_key':project_key,
                'project_data':final_update
            })

            return True
        except Exception as e:
            print(e)
            return False

    else:
        return None




def add_projectmates(data):
    """
    This function add new members to project

    Parameter
    ---------
    data
    This function takes the project members list as parameter
    and add them to project

    """
    
    mylogger.info(data)

    for items in data:
        if items != "ProjectName":
            mylogger.info(data[items])
            project_members.insert({
                "_key":data["ProjectName"] + "_" +data[items],
                "_from":"project/"+data["ProjectName"],
                "_to":"account/"+data[items]
            })
            

    return None



def display_project_members(projectname):
    """
    This function display list of project members

    Parameter
    ---------
    projectname
    This function takes the project name as parameter
    and returns the members list from db

    Return
    -------
    Return the list of member dict
    """
    members = [pm['_to'].split('/')[1] for pm in project_members if projectname in pm['_key']]
    memberNames = []
    for member in members:
        name = user.get(member)
        if "full_name" in name:
            name = name["full_name"]
        else:
            name = member
        memberNames.append({"email":member,"name":name})
    return memberNames

def delete_project_member(project_name,member):
    """
    This function delete the member from the project

    Parameter
    ---------
    projectname,member
    This function takes the project name and member username 
    as parameter and delete member from list of members of that project

    """
    if(project_members.has(project_name+"_"+member)):
        project_members.delete({
            "_key":project_name+"_"+member
        })

def isProjectAdmin(current_user,project_name):
    """
    This function tells whether current user is 
    admin of the project or not

    Logic: As Project Vertex is Storing username of Project Admin,
    so it is compared with username of current user who is signed in,
    if both matches then yes current user is admin else not.

    Parameter
    ---------
    projectname,current_user
    This function takes the project name and current_user 
    as parameter and tells weather current_user is admin or not

    """
    curr_project = project.get(project_name)
    mylogger.info(curr_project)
    if curr_project["project_data"]["CurrentAdminEmail"] == current_user:
        return True
    else:
       return False

def create_project_meeting(data,project_members) -> None:
    """
    Create meeting

    Parameter
    ----------
    data : dict format
    org_members : invitees

    Return
    -------
    None
    """
    #mylogger.info("list",list(users_with_org.keys()))
    try:
        status = meetings.insert(
            {
                "hostname": data["hostname"],
                "meeting_subject": data["meeting_subject"],
                "start_time": data["start_time"],
                "end_time": data["end_time"],
                "invitees": project_members,
                "agenda": data["agenda"],
                "link": data["link"],
                # "date": str(
                #     datetime.date(int(data["year"]), int(data["month"]), int(data["date"]))
                # ),
                "date": data['date'],
                "meeting_review": {},
                "meeting_ended": False,
            }
        )

        meeting_key = status["_key"]

        for u in project_members:
            user_meeting.insert(
                {
                    "_key": f"{u}_{meeting_key}",
                    "_from": f"account/{u}",
                    "_to": f"meetings/{meeting_key}",
                    "creator_yn": False,
                    "accepted_yn": False,
                    "date": data['date'],
                }
            )

        if not user.has(data["hostname"]):
            user.insert({"_key": data["hostname"]})

        user_meeting.insert(
            {
                "_key": f"{data['hostname']}_{meeting_key}",
                "_from": f"account/{data['hostname']}",
                "_to": f"meetings/{meeting_key}",
                "date": data["date"],
                "creator_yn": True,
                "accepted_yn": True,
            }
        )
    except Exception as e:
        mylogger.info("Exception : ",e)
        return False,""

    return True,meeting_key
    

######################################################################################
########################## END OF PROJECT CRUD FUNCTIONS #############################
######################################################################################




######################################################################################
################################## ARANGO SEARCH #####################################
######################################################################################

# Author : Sejal Gupta


def search_meetings_by_meeting_subject(data):
    """
    function to search meeting by meeting subject

    Parameter 
    ----------
    data : str
            form data that contains the keyword to be searched
    
    Return
    -------
    meetings: list of results found
    """
    search_words = data["search_data"]
    cursor = db_conn.aql.execute('FOR doc IN meetings_view SEARCH PHRASE(doc.meeting_subject ,"'+search_words+'","text_en") RETURN doc')
    meetings = [document for document in cursor]
    return meetings


def search_meetings_by_agenda(data):
    """
    function to search meeting by meeting agenda

    Parameter 
    ----------
    data : str
            form data that contains the keyword to be searched
    
    Return
    -------
    meetings: list of results found
    """
    search_words = data["search_data"]
    cursor = db_conn.aql.execute('FOR doc IN meetings_view SEARCH PHRASE(doc.agenda ,"'+search_words+'","text_en") RETURN doc')
    meetings = [document for document in cursor]
    return meetings

def search_meetings_by_invitees(data):
    """
    function to search meeting by meeting invitees

    Parameter 
    ----------
    data : str
            form data that contains the keyword to be searched
    
    Return
    -------
    meetings: list of results found
    """
    search_words = data["search_data"]
    cursor = db_conn.aql.execute('FOR doc IN meetings_view SEARCH PHRASE(doc.invitees ,"'+search_words+'","text_en") RETURN doc')
    meetings = [document for document in cursor]
    return meetings

def search_actions_by_title(data):
    """
    function to search actions by title

    Parameter 
    ----------
    data : str
            form data that contains the keyword to be searched
    
    Return
    -------
    actions: list of results found
    """
    search_words = data["search_data"]
    cursor = db_conn.aql.execute('FOR doc IN actions_view SEARCH PHRASE(doc.title ,"'+search_words+'","text_en") RETURN doc')
    actions = [document for document in cursor]
    return actions


def search_actions_by_description(data):
    """
    function to search actions by description

    Parameter 
    ----------
    data : str
            form data that contains the keyword to be searched
    
    Return
    -------
    actions: list of results found
    """
    search_words = data["search_data"]
    cursor = db_conn.aql.execute('FOR doc IN actions_view SEARCH PHRASE(doc.description ,"'+search_words+'","text_en") RETURN doc')
    actions = [document for document in cursor]
    return actions

def global_search_meetings(data,return_dict):
    """
    function to search all the tables (till now meetings and actions table)

    Parameter 
    ----------
    data : str
            form data that contains the keyword to be searched
    
    Return
    -------
    search_results: dictionary that contains serach results from all tables
    """

    search_words = data["search_data"]

    cursor = db_conn.aql.execute('FOR doc IN meetings_view SEARCH PHRASE(doc.meeting_subject ,"'+search_words+'","text_en") OR PHRASE(doc.agenda ,"'+search_words+'","text_en") OR PHRASE(doc.invitees ,"'+search_words+'","text_en")RETURN doc')
    return_dict["meeting_results"] = [document for document in cursor]


def global_search_actions(data,return_dict):
    """
    function to search all the tables (till now meetings and actions table)

    Parameter 
    ----------
    data : str
            form data that contains the keyword to be searched
    
    Return
    -------
    search_results: dictionary that contains serach results from all tables
    """

    search_words = data["search_data"]

    cursor = db_conn.aql.execute('FOR doc IN actions_view SEARCH PHRASE(doc.title ,"'+search_words+'","text_en") OR PHRASE(doc.description ,"'+search_words+'","text_en") RETURN doc')
    return_dict["action_results"] = [document for document in cursor]


######################################################################################
#################################### ONE DRIVE #######################################
######################################################################################

# Author : Priyav Kaneria

def get_user_pto(user: str):
    user_org_teams = {}
    user_organizations = get_all_user_orgs(user)
    user_org_projects = {}
    for org in user_organizations:
        user_org_projects[org] = display_org_projects(org)
        user_org_teams[org] = get_org_teams(org)
    return {"organizations":user_organizations, "teams":user_org_teams, "projects":user_org_projects}



######################################################################################
################################ LOGIN OTP PASSCODE ###################################
######################################################################################

# Author : KrishNa's

def set_login_code(email: str, code: str):
    try:
        user.update({"_key": email, "login_passcode": code, "login_otp_gen_time": current_time.time()})
        return True
    except:
        return False

def set_signup_time(email: str):
    try:
        user.update({"_key": email, "login_otp_gen_time": current_time.time()})
        return True
    except:
        return False

def get_login_code(email: str):
    try:
        mylogger.info(email)
        cuser = user.get(email)
        return cuser['login_passcode']
    except:
        return False

def get_org_det_for_workspaces(org):
    data = user_org.edges('organization/'+org, direction='in')['edges']
    org_det = {}
    count = 0
    first_letters = []

    for edge in data:
        count = count+1
        first_letters.append(edge['_from'].split("/")[1][0])
        #print(type(edge))

    print(count)
    org_det['count'] = count
    org_det['logos'] = first_letters
    org_det['workspace'] = org
    return org_det

def get_pending_invitations(uname):
    aql= db_conn.aql
    cursor = db_conn.aql.execute(
    'FOR doc IN invitations RETURN doc'#,
    #bind_vars={'value': 19}
    )
    pending_dict = dict()#[doc['_key'] for doc in cursor if uname in doc['_key']]
    for doc in cursor:
        print(doc)
        org = doc['_key'].split('_')[1]
        if uname in doc['_key'] and doc['Joined_yn'] == False:
            pending_dict[org] = 'https://localhost:8000/api/v1/invite/'+org+'/'+doc['_key']
    print(pending_dict)
    return pending_dict


def otp_expired_yn(email: str):
    """
    Here we are checking if the password is expired or not.
    the time limit is 300 seconds
    """
    try:
        cuser = user.get(email)
        print("user : ",cuser)
        if current_time.time() - cuser['login_otp_gen_time'] > 300:
            print("Invalid OTP")
            print("Current time : ",current_time.time(),"OTP gen time : ",cuser['login_otp_gen_time'])
            return True
        else:
            print("Valid OTP")
            return False
    except Exception as e:
        print(e)
        return True




######################################################################################
################################ RECOVERY PASSCODE ###################################
######################################################################################

# Author : Priyav Kaneria

def set_recovery_code(email: str, code: str):
    try:
        user.update({"_key": email, "recovery_passcode": code, "recovered_yn": False})
        return True
    except:
        return False

def get_recovery_code(email: str):
    try:
        mylogger.info(email)
        cuser = user.get(email)
        return cuser['recovery_passcode']
    except:
        return False

def user_recovered(email: str):
    try:
        user.update({"_key": email, "recovered_yn": True})
        return True
    except:
        return False


def post_contact_us(data):
    """
    This function takes the data from contact_us post
    request in the form dict and store in the data specified.


    Parameter
    ---------
    data - The contact us data dict is this parameter

    Return
    -------
    Return the stored contact data from the DB
    """
    try:
        tm = datetime.datetime.now()

        check_beta_status = contact_us.has(data['email'])
        check_user_status = user.has(data['email'])
        if check_user_status:
            if check_beta_status:
                return False
        else:
            if check_beta_status:
                return False


        if data["query"] is None:
            contact_data = contact_us.insert({
            '_key':data["email"],#+"_"+tm.strftime("%d-%m-%YT%H:%M:%S"),
              'reason':'beta-list',
            'email':data["email"],
            'request_date': tm.strftime("%d-%m-%YT%H:%M:%S"),
            'beta_since': tm.strftime("%d-%m-%YT%H:%M:%S")
            })
        else:
            contact_data = contact_us.insert({
                '_key':data["email"]+"_"+tm.strftime("%d-%m-%YT%H:%M:%S"),
                'reason':data["reason"],
                'email':data["email"],
                'query':data["query"],
                'request_date': tm.strftime("%d-%m-%YT%H:%M:%S"),
                'resolved_date': '',
                'resolved_yn': False

                })
        return True
    except Exception as e:
        print(e)
        return None


def get_beta_list():
    cursor = aql.execute(
        'FOR doc in contact_us RETURN doc'
    )
    
    beta_list = [cu['email'] for cu in cursor]
    return beta_list
