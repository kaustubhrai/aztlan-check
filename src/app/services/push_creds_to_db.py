import json, os
print(os.getcwd())
dir_path = os.path.dirname(os.path.realpath(__file__))
print(dir_path)
# Connect to "test" database as root user.
# db = client.db('_system', username='root', password='arangodb')

# if db.has_graph('meetings'):
#     meetings = db.graph('meetings')
# else:
#     meetings = db.create_graph('meetings')


# meetings = client.db('workpeer16', 'root', 'arangodb')


# if meetings.has_vertex_collection('third_party_creds'):
#     third_party_creds = meetings.vertex_collection('third_party_creds')
# else:
#     third_party_creds = meetings.create_vertex_collection('third_party_creds')



from arango import ArangoClient




client = ArangoClient(hosts='http://localhost:8529')

#general_db = client.db(config.HOME_DB_NAME,username=config.DB_USER,password=config.DB_PASS)

#meetings_db

'''
connects to the main datbase '_system'
'''
def conn_main_db(username, password, default_db = "_system"):
    sys_db = client.db(default_db,username=username,password=password)

    return sys_db

'''
connects to the database that has to be accessed other than main '_system' database
'''
def conn_db(db_name, username, password, default_db = "_system", admin_access = False):


    sys_db = conn_main_db(username, password)
    if sys_db is not None:
        if not sys_db.has_database(db_name):
            sys_db.create_database(db_name)

    meetings_db = client.db(db_name, username, password)

    if admin_access == True:
        return sys_db, meetings_db

    return meetings_db



'''
using the above database connections...
we will connect to the graph required for the implementation
'''
def get_graph(conn, graph_name):
    if not conn.has_graph(graph_name):
        meetings = conn.create_graph(graph_name)
        #print(False)
    else:
        meetings = conn.graph(graph_name)
        #print(True)
    return meetings



db_conn = conn_db('workpeer32', username='root', password='arangodb')
graph = get_graph(db_conn,'meetings')


# if graph.has_edge_definition('user_org'):
#     user_org = graph.edge_collection('user_org')


# data = user_org.edges('organization/workpeer', direction='in')
# print(data['edges'])

# for edge in data['edges']:
#     print(edge)

# if graph.has_vertex_collection('invitations'):
#     invitations = graph.vertex_collection('invitations')

#print(invitations.get({'Invitee_email':'krishnamohan541@gmail.com'}))


# def get_user_count_and_first_letters(data):
#     org_det = {}
#     count = 0
#     first_letters = []

#     for edge in data:
#         count = count+1
#         first_letters.append(edge['_from'].split("/")[1][0])
#         #print(type(edge))

#     print(count)
#     org_det['count'] = count
#     org_det['logos'] = first_letters
#     print(org_det)

#get_user_count_and_first_letters(data['edges'])





# uname = 'krishna'
# def get_pending_invitations(uname):
#     aql= db_conn.aql
#     cursor = db_conn.aql.execute(
#     'FOR doc IN invitations RETURN doc'#,
#     #bind_vars={'value': 19}
#     )
#     pending_dict = dict()#[doc['_key'] for doc in cursor if uname in doc['_key']]
#     for doc in cursor:
#         org = doc['_key'].split('_')[1]
#         print(org)
#         if uname in doc['_key'] and doc['Joined_yn'] == True:
#             pending_dict[org] = 'https://api.workpeer.in/api/v1/invite/'+org+'/'+doc['_key']
#     print(pending_dict)
#     return pending_dict

# get_pending_invitations(uname)

if graph.has_vertex_collection('third_party_creds'):
    third_party_creds = graph.vertex_collection('third_party_creds')
else:
    third_party_creds = graph.create_vertex_collection('third_party_creds')



path = "/root/aztlan/production/meetings/1-on-1-meetings/results_workpeer.json"
# with (path, 'r') as file:
#     data = file.read()
#     details = json.loads(data)
#     print(details)


file = open(path, 'r')
data = file.read()
details = json.loads(data)
print(details[0])
print(type(details[0]))
file.close()

for detail in details:
#    if detail['_key'] !='microsoft':
    third_party_creds.insert(detail)


ms_details = third_party_creds.get({'_id':'third_party_creds/microsoft'})
g_details = third_party_creds.get({'_id':'third_party_creds/google'})
print(ms_details)
print(g_details)
