"""
this file contains
1. all the details and links related to the product those
   can be accessible to users and non-users(not registered).
2.


"""


# third party imports
# -------------------
import os
from fastapi import APIRouter,Request,Form,status
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse,RedirectResponse, Response
from typing import Optional

# local application imports
# -------------------------
from core.config import Config
from app.models.models import user, meetings, orgs, graph, db_conn
from app.services import crud, eventlogger
import datetime

home_router = APIRouter()


# extracing the template paths
# ----------------------------
# give the relative path to the templates for jinja2 rendering
template_dir = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))

# Now join the path to the actual template containing folder
template_dir = os.path.join(template_dir, "templates")

# Intilize templates variable by defining the containing directory
templates = Jinja2Templates(directory=template_dir)


@home_router.get('/')
def home():
    return {'status': 'under-construction, coming soon..!!!\n go/add to src/app/routes/healthy file to add landing page'}

'''
@home_router.get('/contact-us',response_class = HTMLResponse)
def getContactUsPage(request:Request,redirected:Optional[bool]= False,name:Optional[str]= None):
    	"""
		
		This function is for contact-us page.

		Parameter
		---------
		parameter 'redirected' is to check whether this route
		is accessed from post request or not.
		And parameter 'name' is the name of person who has
		submitted the contact us form.

		Return
		------
		Return the contact us page as a HTTP GET request.

		"""
    	if redirected == True:
    			return templates.TemplateResponse('landingpage_templates/contactus.html',{
					'request':request,
					'name':name
				})

    	return templates.TemplateResponse('landingpage_templates/contactus.html',{
			'request':request
		})



@home_router.post('/contact-us',response_class = RedirectResponse)
async def getContactUspage(request:Request):

    """

    This route function is for post the contact us  form.
    And stores the form data in database.

    Parameter
    ---------
    No parameter other than trivial request one.

    Return
    ------
    It just redirects to route contact-us having GET HTTP-request.

    """

    formdata = await request.form()
    name = formdata["FirstName"]+" "+formdata["LastName"]
    event_data = crud.insert_new_contactus_data(formdata)
    eventlogger.event_processor("ContactUsData", event_data)
    return RedirectResponse(url='/contact-us?redirected=true&name='+name, status_code=status.HTTP_303_SEE_OTHER)

# 	"""
	
# 	This route function is for post the contact us  form.
# 	And stores the form data in database.

# 	Parameter
# 	---------
# 	No parameter other than trivial request one.
	# formdata = await request.form()
	# name = formdata["FirstName"]+" "+formdata["LastName"]
	# event_data = crud.insert_new_contactus_data(formdata)
    # eventlogger.event_processor("ContactUsData",event_data)
	# return RedirectResponse(url='/contact-us?redirected=true&name='+name, status_code=status.HTTP_303_SEE_OTHER)

# 	Return
# 	------
# 	It just redirects to route contact-us having GET HTTP-request.

# 	"""

# 	formdata = await request.form()
# 	name = formdata["FirstName"]+" "+formdata["LastName"]
#     event_data = crud.insert_new_contactus_data(formdata)
#     eventlogger.event_processor("ContactUsData",event_data)
#     return RedirectResponse(
#         url='/contact-us?redirected=true&name='+name,
#         status_code=status.HTTP_303_SEE_OTHER
#     )


@home_router.get('/improve-us')
def getImproveUsPage():
	return {'status':'go to src/app/routes/healthy file to got/add to improve-us page'}



@home_router.get("/improve_us/{username}", response_class=HTMLResponse)
async def improve_us(username: str, request: Request) -> "Jinja2Templates":
    """
    Improve Us Feedback Form

    Parameter
    ----------
    username : str
                user who wants to submit Improve Us Form
    request : Request
    
    Return
    -------
    Jinja2Templates Response
    """

    return templates.TemplateResponse(
        "meetings-templates/improve_us.html", {"request": request, "username": username}
    )


@home_router.post("/improve_us_submission/{username}")
async def improve_us_submission(username: str, request: Request) -> str:
    """
    Improve Us Feedback Form Submission Endpoint

    Parameter
    ----------
    username : str
                user who is submitting the form
    request : Request

    Return
    -------
    str Response
    """

    datetime_now = datetime.datetime.now()
    form = await request.form()
    event_data = crud.submit_improve_us(username, form, str(datetime_now))
    eventlogger.event_processor("ImproveUs",event_data)

    return "Thank You !! For Your Feedback"


@home_router.get("/improve_us_log", response_class=HTMLResponse)
async def improve_us_log(request: Request) -> "Jinja2Templates":
    """
    View Improve Us Log

    Parameter
    ----------
    request : Request

    Return
    -------
    Jinja2Templates Response
    """

    data = crud.get_improve_us()

    return templates.TemplateResponse(
        "meetings-templates/improve_us_log.html", {"request": request, "data": data}
    )


@home_router.get('/careers')
def getCareersPage():
	return {'status':'go to src/app/routes/healthy file to see/add careers page'}


@home_router.get('/blog', response_class=Response)
def getBlogsPage():
	return RedirectResponse('/careers')
	#return {'status':'go to src/app/routes/healthy file to see/add blogs and editorials by workpeer'}




@home_router.get('/settings')
def getSettingsPage():
	return {'status': 'go to src/app/routes/healthy file to see/add settings page'}
'''



@home_router.get('/config')
def getConfig():
	config = Config()
	#return {'status': config.DB_USER}
	return {'status': user}
