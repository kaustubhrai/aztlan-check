"""
This file contains,
1. authentication
2. authorisation

related modules.


1. authentication:
a. signup
b. signin/login
c. logout/
d. delete


this includes third-party authorisations.


2. authorisation:
a. permissions related to creator and invitee related priviliges
b. restricting the modifications related to settings, accounts etc., can be added on the go.

"""

# standard library imports
# ------------------------
import pickle
import random
import string
import os
import sys
import json
from datetime import datetime, timedelta
from typing import Optional
from urllib import parse

# third party imports
# -------------------
from fastapi import APIRouter, status
from fastapi import BackgroundTasks, Request, Form, Depends
from fastapi_mail.fastmail import FastMail, MessageSchema, ConnectionConfig
from fastapi import Header, File, Body, Query, UploadFile
from fastapi.encoders import jsonable_encoder
from fastapi.templating import Jinja2Templates
from typing import Optional
from fastapi.staticfiles import StaticFiles
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.encoders import jsonable_encoder
from fastapi.security.oauth2 import (
    OAuth2,
    OAuthFlowsModel,
    get_authorization_scheme_param,
)

from starlette.status import HTTP_403_FORBIDDEN
from starlette.responses import RedirectResponse, JSONResponse, HTMLResponse
from starlette.requests import Request

from jwt import PyJWTError
from jose import JWTError, jwt
from app.models import models

from starlette.responses import JSONResponse, RedirectResponse, HTMLResponse

from pydantic import BaseModel, EmailStr

from passlib.context import CryptContext


# Push Notification Imports
# -----------------------------
from pusher_push_notifications import PushNotifications

# Google Authentication Imports
# -----------------------------
from google.oauth2 import id_token
from google.auth.transport import requests
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request as request

# Microsoft Authentication Imports
# --------------------------------
import msal
import requests as rq

# local application imports
# -------------------------

# forgot password - pydantic schemas and new_user schema
from app.schemas.schemas import EmailSchema, SentPasscode, UserItem, NewPassword, AccountRecovery, GetAuthToken,AuthenticateClass,AuthenticateLogin,AuthenticateOTP

# new user function
from app.services import crud
from app.services.crud import insert_user, update_user_password
from app.services.eventlogger import event_processor

from .logger import init_logger_singleton

# Get the router for the security file
security_router = APIRouter()

# Logging setup -- Please read logger.py file to use this logger
mylogger = init_logger_singleton('security')

from core.config import BASE_DIR
from dotenv import load_dotenv, dotenv_values

load_dotenv(os.path.join(BASE_DIR, "third_party_creds.env"))
print(dotenv_values(os.path.join(BASE_DIR, "third_party_creds.env")))

"""
The following is for forgot password - email sending and verification
-------------------------------------------------------------------------------
"""

"""
For sending recovery or other mails
company email and password is defined here.
Has to be filled with valid credentials when using
"""
company_email = ""
company_email_password = ""
# -------------------------------------------------


# extracting the template paths
# ----------------------------
# give the relative path to the templates for jinja2 rendering
template_dir = os.path.dirname(
    os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
)

# Now join the path to the actual template containing folder
template_dir = os.path.join(template_dir, "templates")
template_dir = os.path.join(template_dir, "security_templates")

# Initialize templates variable by defining the containing directory
templates = Jinja2Templates(directory=template_dir)

beams_client = PushNotifications(
    instance_id = "13917092-3f6f-49e5-b39b-e21c89f24565",
    secret_key = "476891DF53AD7EDD6CE141AA992B351E20F2B128C7576EE17799C7911D4B841B",
)

conf = ConnectionConfig(
    MAIL_USERNAME="contact@aztlan.in",
    MAIL_PASSWORD="Aztlan@123",
    MAIL_PORT=587,
    #MAIL_FROM = "contact@aztlan.in",
    MAIL_SERVER="smtp.office365.com",
    MAIL_TLS=True,
    MAIL_SSL=False
)

def get_random_alphanumeric_string(length):
    """Generates a random alphanumeric string of given length.

    Parameters
    ----------
    length : int
        The length of random string to be generated.

    Returns
    -------
    result_str : string
        A random string of the length given.
    """

    letters_and_digits = string.digits
    result_str = "".join((random.choice(letters_and_digits) for i in range(length)))
    return result_str


def send_email(background_tasks: BackgroundTasks, email: str, code: str, passcode_type='recovery password'):
    """Sends an email with a defined template containing the passcode.

    Email is intialized at '/enter_recovery_email' endpoint as global.
    You have to fill in here your email and password from which you want
    to send the mail (GMAIL).

    Parameters
    ----------
    background_tasks : BackgroudTasks
        For sending the mail in the background.
    email : str
        Email of the user
    code : str
        Alphanumeric string passcode

    Returns
    -------
    template : Jinaja Template
        Returns the template "after_email_sent_response.html".
    """

    template = f"""
        <html>
        <body>
        <p>Hi !!!
        <br>Thanks for using Workeeper</p>
        <p> Your passcode is : {code} </p>
        </body>
        </html>
        """
    fm = FastMail(conf)
    message = MessageSchema(
        subject=passcode_type,
        recipients=[email],
        body=template,
        subtype="html"
        )
    # await fm.send_message(message)
    background_tasks.add_task(fm.send_message, message)
    return True


@security_router.post("/send_login_mail")
async def send_mail(
    background_tasks: BackgroundTasks,
    request: Request,
    email_schema: EmailSchema = Depends(), redirect_url:Optional[str]=None
):
    """End-point to send the mail.

    Generates the security-code and then calls the send_email function.

    Parameters
    ----------
    background_tasks : BackgroudTasks
        For sending the mail in the background.
    request : Request
        For using JinJaTemplates as a response.
    email_schema : EmailSchema
        Schema to get the email of user.

    Returns
    -------
    template : Jinaja Template
        Calls the send_email funtion which returns the template.
    """

    formdata = email_schema.__dict__
    email = formdata['email']
    valid_user = crud.get_user(email)
    print(valid_user)
    #authenticated_user = authenticate_user(formdata["email"],formdata["password"])
    if valid_user is None:
        #raise HTTPException(status_code=401, detail="Invalid username or password")
        return JSONResponse({
            'status_code':status.HTTP_401_UNAUTHORIZED,
            'detail':"Invalid username or password"
            })

    if valid_user["active_yn"]==False:
        #raise HTTPException(status_code=401, detail="User Not Activated. Please verify email")
        return JSONResponse({
            'status_code':status.HTTP_401_UNAUTHORIZED,
            'detail':"User Not Activated. Please verify email"
            })

    code = get_random_alphanumeric_string(6)
    if crud.set_login_code(email,code):
        if send_email(background_tasks, email, code, passcode_type='login otp'):
            #return templates.TemplateResponse("login_otp.html", {"request": request, "email": email, "redirect": redirect_url})
            return JSONResponse({
                'status_code':status.HTTP_100_CONTINUE,
                'email':email,
                'url':redirect_url
                })
    else:
        return JSONResponse({
            'status_code':status.HTTP_417_EXPECTATION_FAILED,
            'detail':"Could not update database"})


@security_router.get("/check_email/{email}")
async def check_mail(request: Request, email: str):
    """Check email in user database"""
    user = crud.get_user(email)
    if user:
        return True
    else:
        #raise HTTPException(status.HTTP_404_NOT_FOUND,"User not found")
        return JSONResponse({
            'status_code':status.HTTP_404_NOT_FOUND,
            'detail':"User not found"
            })


# @security_router.get("/enter_recovery_email")
# async def get_email(request: Request):
#     """Returns the homepage template where you enter your email - 'enter_email_for_recovery.html'"""

#     return json.dumps({"request": request})


# @security_router.post("/send_mail")
# async def send_mail(
#     background_tasks: BackgroundTasks,
#     request: Request,
#     email_schema: EmailSchema = Depends(),
# ):
#     """End-point to send the mail.

#     Generates the security-code and then calls the send_email function.

#     Parameters
#     ----------
#     background_tasks : BackgroudTasks
#         For sending the mail in the background.
#     request : Request
#         For using JinJaTemplates as a response.
#     email_schema : EmailSchema
#         Schema to get the email of user.

#     Returns
#     -------
#     template : Jinaja Template
#         Calls the send_email funtion which returns the template.
#     """

#     formdata = email_schema.__dict__
#     email = formdata['email']
#     code = get_random_alphanumeric_string(5)
#     if crud.set_recovery_code(email,code):
#         if send_email(background_tasks, email, code):
#             #return TemplateResponse("login_otp.html", {"request": request})
#             return JSONResponse({
#                 'status_code':status.HTTP_100_CONTINUE,
#                 'url':''#redirect_url
#                 })
#     else:
#         return JSONResponse({
#             'status_code':status.HTTP_417_EXPECTATION_FAILED,
#             'detail':"Could not update database"})


# @security_router.get("/send_mail_again/{email}")
# def send_mail_again(background_tasks: BackgroundTasks, request: Request, email: str):
#     """Resends the mail when user clicks in the resend email button."""
#     code = crud.get_recovery_code(email)
#     return send_email(background_tasks, request, email, code)


# @security_router.post("/account_recovery/")
# async def _passcode(request: Request,ar:AccountRecovery=Depends()):
#     """Checks if the passcode entered by the user is correct or not.

#     Parameters
#     ----------
#     request : Request
#         For using JinJaTemplates as a response.

#     Returns
#     -------
#     template : Jinaja Template
#         Returns the "failed_verification.html" or
#         "successful_verification_result.html" templates.
#     """

#     result = ""
#     formdata = ar.__dict__
#     email = formdata['email']
#     code = crud.get_recovery_code(email)
#     mylogger.info(formdata["passcode"])
#     mylogger.info(code)
#     if formdata['passcode'] == code:
#         result = "successful"
#     else:
#         result = "failed"
#     return json.dumps({"request": request, "result": result, "email": email})


# @security_router.get("/re_enter_passcode")
# async def re_enter_passcode(request: Request):
#     """Redirects to "after_email_sent_response.html for re-entering of the passcode."""

#     return json.dumps({"request": request})


# @security_router.get("/check_links")
# async def check_links():
#     return {"this is merge checking purpose"}


# @security_router.get("/change_password/{email}")
# async def after_successful_verification(request: Request, email: str):
#     """Redirects to 'enter_new_password.html' for taking the new password
#         of the user after forgot password email verification successful"""
#     user = crud.get_user(email)
#     if not user['recovered_yn']:
#         crud.user_recovered(email)
#         return json.dumps({"request": request, "email":email})
#     else:
#         return "User not verified to change password"


# @security_router.post("/update_user_password/{email}")
# async def update_password(email: str, new_password_schema: NewPassword = Depends()):
#     """Calls the update function for the password from the crud module.

#     Parameters
#     ----------
#     new_password_schema : NewPassword
#         schema to get the password entered by the user.

#     Returns
#     -------
#     For now just a json response to say updation successful.
#     Later will be used to redirect it to the HOME PAGE of
#     user's account at workeeper.
#     """

#     update_user_password(email, get_password_hash(new_password_schema.password))
#     event_dict = {}
#     event_dict['email']=email
#     #event_processor("PasswordUpdation",event_dict)

#     return {"password updation": "successful"}


# """
# The following is for new user signup
# -------------------------------------------------------------------------------
# """


# @security_router.get("/new_user_signup")
# async def enter_new_user(request: Request, redirect_url: Optional[str]=None):
#     """Redirects to the New user sign up page"""

#     return templates.TemplateResponse("NewUserSignUp.html", {"request": request, "redirect_url": redirect_url})

# '''
# @security_router.post("/new_user")
# async def newUser(user: UserItem = Depends(), redirect_url:Optional[str]=None):
#     """
#     parameters:
#     input:
#     redirect_url: optional input --> can be
#     """
#     try:
#         # user.password = get_password_hash(user.password)
#         inserted_user = insert_user(user)
#         #mylogger.info(inserted_user)
#         print("new user inserted")
#     except:
#         print("user already existed..........")
#         return JSONResponse({
#             'status_code':status.HTTP_409_CONFLICT,
#             'detail':'duplicate user data'
#             })

#     # if redirect_url == '' or redirect_url is None:
#     #     return JSONResponse({
#     #     'status_code':status.HTTP_303_SEE_OTHER,
#     #     'redirect_url':f"/api/v1/verify-email-signup/{user.email}?redirect_url={redirect_url}"
#     #     }) 
#     # else:

#     return JSONResponse({
#         'status_code':status.HTTP_303_SEE_OTHER,
#         'redirect_url':f"/api/v1/verify-email-signup/{user.email}?redirect_url={redirect_url}"
#         }) 
#     #RedirectResponse(url=f"/api/v1/verify-email-signup/{user.email}?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
# '''

@security_router.post("/new_user")
async def newUser( background_tasks: BackgroundTasks, request: Request, user_item: UserItem = Depends(), redirect_url:Optional[str]=None):
    """
    parameters:
    input:
    redirect_url: optional input --> can be
    """
    #try:
    # user.password = get_password_hash(user.password)
    print(user_item)
    print(user_item.__dict__)
    if crud.otp_expired_yn(user_item.email):
        signup_data = crud.get_user(user_item.email)
        if signup_data is not None and signup_data['active_yn'] == False:
            models.user.delete({'_key':user_item.email})
            print("the expired record is deleted...")

    inserted_user = insert_user(user_item)
    #mylogger.info(inserted_user)
    #print("new user inserted")
    if inserted_user == 202:
        code = get_random_alphanumeric_string(6)
        print(code)
        if crud.set_login_code(user_item.email,code):
            print('123')
            if send_email(background_tasks, user_item.email, code, passcode_type='signup otp'):
                print('123')
                return JSONResponse({
                    'status_code':status.HTTP_100_CONTINUE,
                    'email':user_item.email,
                    'url':redirect_url
                    })
            else:
                return JSONResponse({
                    'status_code':status.HTTP_408_REQUEST_TIMEOUT,
                    'detail':'otp email is not sent'
                    })
        else:
            return JSONResponse({
                'status_code':status.HTTP_201_CREATED,
                'email':user_item.email,
                'url':redirect_url,
                'detail':'not able to create otp.please try again'
                })
    else:
        print("user already existed..........")
        return JSONResponse({
            'status_code':status.HTTP_409_CONFLICT,
            'detail':'duplicate user data'
            })



# @security_router.get("/verify-email-signup/{email}")
# async def email_verify_signup(email: str, background_tasks: BackgroundTasks, redirect_url:Optional[str]=None):
#     valid_user = crud.get_user(email)
#     print(valid_user)
#     #authenticated_user = authenticate_user(formdata["email"],formdata["password"])
#     if valid_user is None:
#         return JSONResponse({
#             'status_code':status.HTTP_401_UNAUTHORIZED,
#             'detail':"Invalid username or password"})
#     code = get_random_alphanumeric_string(6)
#     print(code)
#     if crud.set_login_code(email,code):
#         print('123')
#         if send_email(background_tasks, email, code, passcode_type='signup otp'):
#             print('123')
#             return JSONResponse({
#                 'status_code':status.HTTP_100_CONTINUE,
#                 'email':email,
#                 'url':redirect_url
#                 })
#             #return templates.TemplateResponse("signup_otp.html", {"request": request, "email":email, "redirect_url":redirect_url})
#     else:
#         return JSONResponse({
#             'status_code':status.HTTP_417_EXPECTATION_FAILED,
#             'detail':"Could not update database"})

# @security_router.get("/verify-email-successful/{email}")
# async def email_verify_signup(email :str, redirect_url:Optional[str]=None):
#     crud.email_successfully_verified(email)
#     crud.set_signup_time(email)
#     user = crud.get_user(email)
#     if 'invite/' in redirect_url:
#         return JSONResponse({
#             'status_code':status.HTTP_100_CONTINUE,
#             'url':"/api/v1/{redirect_url}",
#             'detail':'moving directly to invite after signup'
#             })
#         #return RedirectResponse(url=f"/api/v1/{redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

#     # if redirect_url is not None:
#     #     return RedirectResponse(url=f"/api/v1/{redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
#     #return RedirectResponse(url=f"/api/v1/add-organization/{email}", status_code=status.HTTP_303_SEE_OTHER)
#     return JSONResponse({
#         'status_code':status.HTTP_100_CONTINUE,
#         'url':"/api/v1/add-organization/{email}",
#         'detail':'moving to add-organization once signup withoout any invite'
#         })


"""
The following is for user login - USING fastapi-login
This is third party package, so we do not have much functionality
Like we cannot create JWT tokens ourselves with this.
-------------------------------------------------------------------------------
"""
# from app.schemas.schemas import login_user_schema
# from app.services.crud import get_user_login_cred
# from fastapi_login import LoginManager
# from fastapi.security import OAuth2PasswordRequestForm
# from fastapi_login.exceptions import InvalidCredentialsException

# SECRET = "7ff8f44c419861f95ff39d0f157d41f2446b92a9868df2501c2e66061cdd8c74"

# # setup the login manager which will handle the process of
# # encoding and decoding JWTs
# manager = LoginManager(SECRET, tokenUrl="/api/v1/auth/token", use_cookie=True)


# # Give the login manager to load a user
# @manager.user_loader
# def load_user(email: str):
#     user = get_user_login_cred(email)
#     return user


# @security_router.post("/auth/token")
# async def authenticate_user_login_credentilas(
#     data: OAuth2PasswordRequestForm = Depends()
# ):
#     """
#     API to authenticate/verify user credentials.

#     Parameters
#     ----------
#     OAuth2 form

#     Returns
#     a dictionary constaining the access token and its type.
#     -------
#     """
#     email = data.username
#     password = data.password

#     user = load_user(email)
#     if not user:
#         raise InvalidCredentialsException
#     elif password != user['password']:
#         raise InvalidCredentialsException

#     access_token = manager.create_access_token(
#         data = dict(sub=email)
#     )

#     return {'access_token': access_token,
#             'token_type': 'bearer'}


# @security_router.get("/protected")
# def protected_route(user = Depends(manager)):
#     '''
#     just an example for checking user logged in
#     '''

#     return {'user': 'logged in'}


"""
USER LOGIN
----------
The following is for user login - from scratch
It has the functions get_current_user and get_current_active_user.
Also functions for creating the access token.
-------------------------------------------------------------------------------
"""
# schema and function imports
from app.schemas.schemas import LoginUserSchema, Token, TokenData, UserInDB
from app.services.crud import get_user_login_cred

# define some parameters for generatinf access token
JWT_SECRET_KEY = "7ff8f44c419861f95ff39d0f157d41f2446b92a9868df2501c2e66061cdd8c74"
ALGORITHM = "HS256"

# give the time for each token.
# Note: it is in minutes.
ACCESS_TOKEN_EXPIRE_MINUTES = 30


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="api/v1/token")


def verify_password(plain_password, hashed_password):
    """This verifies that the hashed_password in DB is same as what user enters."""
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    """Generates hash for the password."""
    return pwd_context.hash(password)


def get_user(username):
    """
    Takes in username.
    Uses get_user_login_cred from crud to get the particular user's dictionary
    Creates a new dictionary and returns it appending with UserInDB.
    """
    user = get_user_login_cred(username)
    if user:
        user_class = {
            "username": user["email"],
            "email": user["email"],
            "hashed_password": get_password_hash(user["password"]),
            "diabled": False,
        }
        return UserInDB(**user_class)


def authenticate_user(username: str, password: str):
    """
    First gets the user with get_user function, then
    verifies its password woth the password entered at the front end.

    Parameters
    ----------
    username : str
        The username that the user entered.
    password : str
        The password entered by the user.

    Returns
    -------
    user : UserInDB
        The user info.
    """
    user = crud.get_user(username)
    if not user:
        return None
    if not verify_password(password, user['password']):
        return None
    return user


# def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
#     """
#     Creates JWT access tokens used by user for access.
#     Appends the time declared from the current time.

#     Parameters
#     ----------
#     data : dict
#         contains the username if the user.
#     espires_delta : timedelta
#         defines the time that the token will be active for.

#     Returns
#     -------
#     encoded_jwt : jwt
#         the jwt token created in an encoded format.
#     """
#     to_encode = data.copy()
#     if expires_delta:
#         expire = datetime.utcnow() + expires_delta
#     else:
#         expire = datetime.utcnow() + timedelta(minutes=15)
#     to_encode.update({"exp": expire})
#     encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
#     return encoded_jwt


async def get_current_user(token: str = Depends(oauth2_scheme)):
    """
    This checks that jwt payload has the same users by decoding it.

    Parameters
    ----------
    token : str
        oauth2_scheme dependency token.

    Returns
    -------
    user : UserInDB
        pydanic schema containing all the user information.
    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    # try:
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    username = payload.get("sub")
        # if username is None:
        #     raise credentials_exception
        # token_data = TokenData(username=username)
    # except JWTError:
    #     raise credentials_exception
    # return crud.get_user(username)
    return username


def make_api_call(method, url, token, body=None):

    """
    function to make api calls for outlook calender

    Parameter 
    ----------
    method : specify which method it is from 'GET'

    url : url of api to which request is made

    token : access token provided by microsoft of the us

    body : request body 
    
    Return
    -------
    response in json form
    """

    # Send these headers with all API calls
    headers = {
        'Authorization' : '{0}'.format(token),
        'Content-Type': 'application/json'
    }
    if method == "GET":
        response = rq.get(url, headers = headers)

    elif method == "POST":
        response = rq.post(url,  data = body, headers = {'Content-Type': 'application/x-www-form-urlencoded'})

    return response.json()

async def get_current_microsoft_user(request: Request):
    """
    This checks that user is logged in via microsoft or not.

    Parameters
    ----------
    none

    Returns
    -------
    user : UserInDB
        pydanic schema containing all the user information.
    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    access_token = request.cookies.get("Authorization")
    if not access_token:
        return None
    # if access_token.split(' ')[1][0:3].lower()=='ewb': ######### this is jugaad and has to be changed #############
    r = make_api_call('GET', "https://graph.microsoft.com/v1.0/me", access_token)
    if "error" in r:
        return None
    myuser = crud.get_user(r["userPrincipalName"])
    if not myuser:
        return None
    return User(email=myuser["_key"], full_name=myuser["full_name"], picture=myuser.get("picture"))


# async def get_current_active_user(
#     current_user: login_user_schema = Depends(get_current_user),
# ):
#     """
#     Function to check if the user if active or not.
#     Takes in the dependency for get_active_user and returns the current user.
#     """
#     if current_user.disabled:
#         raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Inactive user")
#     return current_user


@security_router.post("/token", response_model=Token)
async def login_for_access_token(
    request: Request,
    form_data: OAuth2PasswordRequestForm = Depends()
):
    """
    Takes in the OAuth2PasswordRequestForm and returns the access token.
    """
    #user = authenticate_user(form_data.username, form_data.password)
    user = crud.get_user(form_data.username)
    if not user:
        return None
        '''
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
        '''
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    
    return {"access_token": access_token, "token_type": "bearer"}

    # return RedirectResponse(url="/api/v1/user/me")

    '''
    Returning the below template which shows user info is giving error,
    This function compulsarily has to return access token.

    return templates.TemplateResponse("after_login_successful.html", 
                                        {"request": request,
                                         "user": user
                                        }
                                    )
    '''


######################################################################################
########################### GOOGLE SIGNIN AUTHENTICATION #############################
######################################################################################

# Author : Akshat (akshat.dak@students.iiit.ac.in)
# Co-author : Ayush (ayush.sharma@students.iiit.ac.in)

COOKIE_AUTHORIZATION_NAME = os.getenv('COOKIE_AUTHORIZATION_NAME')     # SHOULD BE MADE NO CHANGES TO THIS !! UNLESS U KNOW WHAT U R DOING :)
COOKIE_DOMAIN = os.getenv('COOKIE_DOMAIN')

PROTOCOL = os.getenv('PROTOCOL')
FULL_HOST_NAME = os.getenv('FULL_HOST_NAME')
PORT_NUMBER = os.getenv('PORT_NUMBER')

#CLIENT_ID = "832502697558-dr0s5dcsg3j0oj273epsg1rssv4rmh9k.apps.googleusercontent.com"
CLIENT_ID = os.getenv('GOOGLE_CLIENT_ID')

#CLIENT_SECRETS_JSON = "client_secret_832502697558-dr0s5dcsg3j0oj273epsg1rssv4rmh9k.apps.googleusercontent.com.json"
CLIENT_SECRETS_JSON = "client_secret_"+CLIENT_ID

API_LOCATION = os.getenv('API_LOCATION')
SWAP_TOKEN_ENDPOINT = os.getenv('SWAP_TOKEN_ENDPOINT')
SUCCESS_ROUTE = os.getenv('SUCCESS_ROUTE')
ERROR_ROUTE = os.getenv('ERROR_ROUTE')

#SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
SECRET_KEY = os.getenv('GOOGLE_CLIENT_SECRET')

ALGORITHM = os.getenv('ALGORITHM')
ACCESS_TOKEN_EXPIRE_HOURS = os.getenv('ACCESS_TOKEN_EXPIRE_HOURS')

# pydantic
class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str = None
    email: str = None


class User(BaseModel):
    email: str = None
    full_name: str = None
    picture: str = None


class OAuth2PasswordBearerCookie(OAuth2):
    def __init__(
        self,
        tokenUrl: str,
        scheme_name: str = None,
        scopes: dict = None,
        auto_error: bool = False,
    ):
        if not scopes:
            scopes = {}
        flows = OAuthFlowsModel(password={"tokenUrl": tokenUrl, "scopes": scopes})
        super().__init__(flows=flows, scheme_name=scheme_name, auto_error=auto_error)

    async def __call__(self, request: Request) -> Optional[str]:
        header_authorization: str = request.headers.get("Authorization")
        cookie_authorization: str = request.cookies.get("Authorization")

        header_scheme, header_param = get_authorization_scheme_param(
            header_authorization
        )
        cookie_scheme, cookie_param = get_authorization_scheme_param(
            cookie_authorization
        )

        if header_scheme.lower() == "bearer":
            authorization = True
            scheme = header_scheme
            param = header_param

        elif cookie_scheme.lower() == "bearer":
            authorization = True
            scheme = cookie_scheme
            param = cookie_param

        else:
            authorization = False

        if not authorization or scheme.lower() != "bearer":
            if self.auto_error:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED, detail="Not authenticated"
                )
            else:
                return None
        return param


oauth2_scheme = OAuth2PasswordBearerCookie(tokenUrl="/api/v1/token")

def create_access_token(*, data: dict, expires_delta: timedelta = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        
        expire = datetime.utcnow() + timedelta(hours=3)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def get_current_google_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=HTTP_403_FORBIDDEN, detail="Could not validate credentials"
    )
    if token is not None:
        try:
            payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
            email: str = payload.get("sub")
            if email is None:
                return None#credentials_exception
        except:
            return None
        if email is not None:
            authenticated_user = crud.authenticate_user_email(email)
            user = authenticated_user
            return User(email=user.get("_key"), organization = user.get("organization")) #user      # in case this is None we ask the user to login


async def get_current_active_user(current_user: User = Depends(get_current_google_user)):#, current_microsoft_user: User = Depends(get_current_microsoft_user)):
    # , current_google_user: User = Depends(get_current_google_user)
    """
    """
    return current_user #or current_microsoft_user



@security_router.post(f"{SWAP_TOKEN_ENDPOINT}", response_model=Token, tags=["security"])
async def swap_token(request: Request = None):
    if not request.headers.get("X-Requested-With"):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect headers")

    google_client_type = request.headers.get("X-Google-OAuth2-Type")
    if google_client_type == 'server':
        try:
            body_bytes = await request.body()
            auth_code = jsonable_encoder(body_bytes)

            credentials = client.credentials_from_clientsecrets_and_code(
                CLIENT_SECRETS_JSON, ["profile", "email"], auth_code
            )

            http_auth = credentials.authorize(httplib2.Http())

            email = credentials.id_token["email"]

        except:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Unable to validate social login")


    if google_client_type == 'client':
        body_bytes = await request.body()
        auth_code = jsonable_encoder(body_bytes)
        try:
            idinfo = id_token.verify_oauth2_token(auth_code, requests.Request(), "291912999391-dcnsqft0ved9dmoihlmqlamh8cokukph.apps.googleusercontent.com")
            # Or, if multiple clients access the backend server:
            # idinfo = id_token.verify_oauth2_token(token, requests.Request())
            # if idinfo['aud'] not in [CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]:
            #     raise ValueError('Could not verify audience.')
            
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
    
            # If auth request is from a G Suite domain:
            # if idinfo['hd'] != GSUITE_DOMAIN_NAME:
            #     raise ValueError('Wrong hosted domain.')
            
            if idinfo['email'] and idinfo['email_verified']:
                email = idinfo.get('email')

            else:
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Unable to validate social login")
            mylogger.info("Idinfo",idinfo)
        except:
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Unable to validate social login")

    authenticated_user = crud.authenticate_user_email(email)

    if authenticated_user is None:
        authenticated_user = crud.create_user(idinfo)  # create a new user here

    access_token_expires = timedelta(hours=ACCESS_TOKEN_EXPIRE_HOURS)
    access_token = create_access_token(
        data={"sub": authenticated_user['_key']}, expires_delta=access_token_expires
    )
    
    crud.add_third_party(email,'google')

    #################### SUCCESSFULLY LOGED IN USING GOOGLE DETAILS ######################
    crud.logged_in(email,"google",request)

    token = jsonable_encoder(access_token)
    response = JSONResponse({"access_token": token, "token_type": "bearer"})

    response.set_cookie(
        key=COOKIE_AUTHORIZATION_NAME,
        value=f"Bearer {token}",
        domain=COOKIE_DOMAIN,
        httponly=True,
        max_age=10800,          # 3 hours
        expires=10800,          # 3 hours
        )
    return response


@security_router.get("/google_signin", tags=["security"])
async def google_sign_in_button(request: Request, redirect_url: Optional[str]=None) -> "html page":
    """
    Just present the page for html google button.
    """
    if redirect_url is None:
        redirect_url = "homepage"

    return json.dumps({"request":request, "redirect_url": redirect_url})


# logout system needs to be improved (probably)
@security_router.get("/logout_user")
async def route_logout_and_remove_cookie(request: Request) -> "RedirectResponse":
    response = RedirectResponse(url="/api/v1/login", status_code=status.HTTP_303_SEE_OTHER)
    response.delete_cookie(key=COOKIE_AUTHORIZATION_NAME, domain=COOKIE_DOMAIN)
    return response


@security_router.get("/logout_google_user")
async def my_page(request: Request) -> "Response":
    return json.dumps({"request": request})
# end of logout system

# THIS MODEL SHOULD BE REPLICATED IN meetings.py
@security_router.get("/test_create_meeting")
async def test_create_meeting(request: Request, current_user: User = Depends(get_current_active_user)) -> "Jinja2Template":
    if current_user is None:
        response = RedirectResponse(url="/api/v1/google_signin?redirect_url=test_create_meeting",
                                    status_code=status.HTTP_303_SEE_OTHER)
    else:
        response = json.dumps({"request":request})
    return response


@security_router.get("/test_example")
async def test_example(request: Request, current_user: User = Depends(get_current_active_user)) -> "str Response":
    if current_user is None:
        return RedirectResponse(url="/api/v1/google_signin?redirect_url=test_example",
                                status_code=status.HTTP_303_SEE_OTHER)
    return "testing here !! :)"


######################################################################################
####################### END OF GOOGLE SIGNIN AUTHENTICATION ##########################
######################################################################################


######################################################################################
###################### USER EMAIL AND PASSWORD LOGIN AND SIGNUP ######################
######################################################################################


# @security_router.get("/login")
# def login_user_page(request: Request, redirect_url: Optional[str]=None):
#     """Redirects to the user login or sign in page"""
#     if redirect_url is None:
#         redirect_url = "homepage"
#     auth_url = _build_auth_url(scopes=SCOPE,state="/"+redirect_url)
#     #mylogger.info("Redirect url",redirect_url)
#     return templates.TemplateResponse("user_login.html", {"request": request, "redirect":redirect_url, "auth_url": auth_url})
    



@security_router.get("/profile")
async def get_profile(request : Request, current_user: User = Depends(get_current_active_user)):
    if not current_user:
        return "Not authorized"
    data = jsonable_encoder(current_user)
    mylogger.info(data)
    return templates.TemplateResponse("User_Profile.html", {"request": request, "username": data['full_name']})#, "position": data["position"],
                                        #"location": data["location"], "email": data["email"], "company": data["company"]})


##################################### EMAIL AND PASSWORD NOT BEING USED #################################################

# @security_router.post("/authenticate", response_model=Token, tags=["security"])
# async def check_user_and_make_token(request: Request,auth:AuthenticateClass=Depends()):
#     formdata = auth.__dict__
#     #mylogger.info(request)
#     #mylogger.info(formdata)
#     authenticated_user = authenticate_user(formdata["email"],formdata["password"])
#     if authenticated_user is None:
#         raise HTTPException(status_code=401, detail="Invalid username or password")

#     if authenticated_user["active_yn"]==False:
#         raise HTTPException(status_code=401, detail="User Not Activated. Please verify email")

#     access_token_expires = timedelta(hours=ACCESS_TOKEN_EXPIRE_HOURS)
    
#     access_token = create_access_token(
#         data={"sub": authenticated_user['_key']}, expires_delta=access_token_expires
#     )

#     #################### SUCCESSFULLY LOGED IN USING CUSTOM DETAILS ######################
#     crud.logged_in(authenticated_user["_key"],"custom",request)

#     token = jsonable_encoder(access_token)
#     response = JSONResponse({"access_token": token, "token_type": "bearer"})
    
#     response.set_cookie(
#         key=COOKIE_AUTHORIZATION_NAME,
#         value=f"Bearer {token}",
#         domain=COOKIE_DOMAIN,
#         httponly=True,
#         max_age=10800,          # 3 hours
#         expires=10800,          # 3 hours
#     )
#     return response

@security_router.post("/authenticate_otp", response_model=Token, tags=["security"])
async def check_user_and_make_token(request:Request, auth:AuthenticateOTP=Depends()):
    formdata = auth.__dict__
    #mylogger.info(request)
    #mylogger.info(formdata)
    email = formdata['email']
    otp = formdata['otp']
    print(email)
    valid_user = crud.get_user(email)
    print(valid_user)
    #authenticated_user = authenticate_user(formdata["email"],formdata["password"])
    if valid_user is None:
        return JSONResponse({
            'status_code':status.HTTP_401_UNAUTHORIZED,
             'detail':"Invalid email"})

    if valid_user["active_yn"]==False:
        return JSONResponse({
            'status_code':status.HTTP_401_UNAUTHORIZED,
             'detail':"User Not Activated. Please verify email"
             })
        #HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="User Not Activated. Please verify email")
    
    if not crud.otp_expired_yn(email):
        if not crud.get_login_code(email)==otp:
            print(crud.get_login_code(email),otp)
            #raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Wrong otp")
            return JSONResponse({
            'status_code':status.HTTP_406_NOT_ACCEPTABLE,
             'detail':"Wrong otp"
             })
    else:
        #raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid OTP")
        return JSONResponse({
            'status_code':status.HTTP_406_NOT_ACCEPTABLE,
            'detail':"Wrong otp"
             })

    access_token_expires = timedelta(hours=ACCESS_TOKEN_EXPIRE_HOURS)

    access_token = create_access_token(
        data={"sub": valid_user['_key']}, expires_delta=access_token_expires
    )

    #################### SUCCESSFULLY LOGED IN USING CUSTOM DETAILS ######################
    crud.logged_in(valid_user['_key'],"custom",request)

    token = jsonable_encoder(access_token)
    response = JSONResponse({"access_token": token, "token_type": "bearer"})

    response.set_cookie(
        key=COOKIE_AUTHORIZATION_NAME,
        value=f"Bearer {token}",
        domain=COOKIE_DOMAIN,
        httponly=True,
        max_age=10800,          # 3 hours
        expires=10800,          # 3 hours
    )
    return response

'''
@security_router.post("/authenticate_signup_otp", response_model=Token, tags=["security"])
async def check_signup_user_and_make_token(request:Request, auth:AuthenticateOTP=Depends()):
    formdata = auth.__dict__
    #mylogger.info(request)
    #mylogger.info(formdata)
    email = formdata['email']
    otp = formdata['otp']
    print(email)
    valid_user = crud.get_user(email)
    print(valid_user)
    #authenticated_user = authenticate_user(formdata["email"],formdata["password"])
    if valid_user is None:
        return JSONResponse({
            'status_code':status.HTTP_401_UNAUTHORIZED,
             'detail':"Invalid email"})

    if not crud.otp_expired_yn(email):
        if not crud.get_login_code(email)==otp:
            print(crud.get_login_code(email),otp)
            # raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Wrong otp")
            return JSONResponse({
            'status_code':status.HTTP_406_NOT_ACCEPTABLE,
             'detail':"Wrong otp"
             })
    else:
        #raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid OTP")
        return JSONResponse({
            'status_code':status.HTTP_406_NOT_ACCEPTABLE,
             'detail':"Wrong otp"
             })
    # access_token_expires = timedelta(hours=ACCESS_TOKEN_EXPIRE_HOURS)

    # access_token = create_access_token(
    #     data={"sub": valid_user['_key']}, expires_delta=access_token_expires
    # )
    return JSONResponse({'status_code':status.HTTP_202_ACCEPTED,
        'detail':'otp matched'})
'''


@security_router.post("/authenticate_signup_otp", response_model=Token, tags=["security"])
async def check_signup_user_and_make_token(request: Request,auth:AuthenticateOTP=Depends(), redirect_url:Optional[str]=None):
    formdata = auth.__dict__
    #mylogger.info(request)
    #mylogger.info(formdata)
    email = formdata['email']
    otp = formdata['otp']
    print(email)
    print("the signed up email above")
    valid_user = crud.get_user(email)
    print(valid_user)
    #authenticated_user = authenticate_user(formdata["email"],formdata["password"])
    if valid_user is None:
        return JSONResponse({
            'status_code':status.HTTP_401_UNAUTHORIZED,
             'detail':"Invalid email"})

    if not crud.otp_expired_yn(email):
        if not crud.get_login_code(email)==otp:
            print(crud.get_login_code(email),otp)
            # raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Wrong otp")
            return JSONResponse({
            'status_code':status.HTTP_406_NOT_ACCEPTABLE,
             'detail':"Wrong otp"
             })
        else:
            crud.email_successfully_verified(email)
            crud.set_signup_time(email)
            print(redirect_url)
            if redirect_url is None:
                return JSONResponse({
                'status_code':status.HTTP_100_CONTINUE,
                'url':"/api/v1/add-organization/{email}",
                'detail':'moving to add-organization once signup withoout any invite'
                })
            if 'invite/' in redirect_url or redirect_url is not None:
                return JSONResponse({
                    'status_code':status.HTTP_100_CONTINUE,
                    'url':"/api/v1/{redirect_url}",
                    'detail':'moving directly to invite after signup'
                    })

    else:
        #raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid OTP")
        return JSONResponse({
            'status_code':status.HTTP_406_NOT_ACCEPTABLE,
             'detail':"otp expired"
             })
    # access_token_expires = timedelta(hours=ACCESS_TOKEN_EXPIRE_HOURS)

    # access_token = create_access_token(
    #     data={"sub": valid_user['_key']}, expires_delta=access_token_expires
    # )
    return JSONResponse({'status_code':status.HTTP_202_ACCEPTED,
        'detail':'otp matched'})


#Rest Logout api's are not required , only with this api every user can be logged out.
#As for Microsoft it is only required to delete cookie and for google it first logout user from google and then deletes the cookie

@security_router.get("/logout")
async def logout_and_remove_cookie(request: Request, current_user: User = Depends(get_current_active_user)) -> "RedirectResponse":
    response = JSONResponse({
        'status_code':status.HTTP_401_UNAUTHORIZED,
        'url':'login',
        'detail':'not logged in to logout.'
        })
    #RedirectResponse(url="/api/v1/login", status_code=status.HTTP_303_SEE_OTHER)
    #print(current_user)
    #print(current_user.__dict__)
    if current_user is None:
        print("the current user is NOne")
        return response
    usertype = crud.get_user_third_party(current_user.email)
    crud.reset_org(current_user.email)
    if usertype=="google":
        return json.dumps({"request": request})
    else:
        print("entering into deletion")
        response = JSONResponse({
        'status_code':status.HTTP_204_NO_CONTENT,
        'url':'go to login',
        'detail':'successfully logged out'
        })
        response.delete_cookie(key=COOKIE_AUTHORIZATION_NAME, domain=COOKIE_DOMAIN)
        crud.logged_out(current_user.email)
        # return templates.TemplateResponse("logout.html",{"request":request, "instanceid":"13917092-3f6f-49e5-b39b-e21c89f24565"})
        #return response
        return response



######################################################################################
#################### END OF EMAIL PASSWORD SIGNIN AUTHENTICATION #####################
######################################################################################


######################################################################################
########################## MICROSOFT SIGNIN AUTHENTICATION ###########################
######################################################################################

# Author : Priyav Kaneria (priyavkaneria@gmail.com)
# Co-author : Sejal Gupta

CLIENT_ID = os.getenv("MICROSOFT_CLIENT_ID")
MICROSOFT_OBJECT_ID = os.getenv("MICROSOFT_OBJECT_ID")
API_LOCATION = os.getenv("MICROSOFT_API_LOCATION")
TOKEN_ENDPOINT = os.getenv("MICROSOFT_TOKEN_ENDPOINT")
SWAP_TOKEN_ENDPOINT = os.getenv("MICROSOFT_SWAP_TOKEN_ENDPOINT")
SUCCESS_ROUTE = os.getenv("MICROSOFT_SUCCESS_ROUTE")
ERROR_ROUTE = os.getenv("MICROSOFT_ERROR_ROUTE")
TENANT_NAME = os.getenv("MICROSOFT_TENANT_NAME")
AUTHORITY = os.getenv("MICROSOFT_AUTHORITY")
SCOPE = os.getenv("MICROSOFT_SCOPE").split(" ")
CLIENT_SECRET = os.getenv("MICROSOFT_CLIENT_SECRET")

################################# MSAL ########################################

def _load_cache():
    cache = msal.SerializableTokenCache()
    # if session.get("token_cache"):
    #     cache.deserialize(session["token_cache"])
    return cache

# def _save_cache(cache):
#     if cache.has_state_changed:
#         session["token_cache"] = cache.serialize()

def _build_msal_app(cache=None, authority=None):
    return msal.ConfidentialClientApplication(
        CLIENT_ID, authority=authority or AUTHORITY,
        client_credential=CLIENT_SECRET, token_cache=cache)

def _build_auth_url(authority=None, scopes=None, state=None):
    return _build_msal_app(authority=authority).get_authorization_request_url(
        scopes or [],
        state=state,
        redirect_uri=API_LOCATION+"/api/v1"+TOKEN_ENDPOINT)

def _get_token_from_cache(scope=None):
    cache = _load_cache()  # This web app maintains one cache per session
    cca = _build_msal_app(cache=cache)
    accounts = cca.get_accounts()
    if accounts:  # So all account(s) belong to the current signed-in user
        result = cca.acquire_token_silent(scope, account=accounts[0])
        # _save_cache(cache)
        return result


################################# MICROSOFT SIGNIN API'S ########################################


@security_router.get(f"{TOKEN_ENDPOINT}", response_model=Token, tags=["security"])
async def get_auth_user(request: Request = None, code: Optional[str]=None, state: Optional[str]=None):
    """
    function to get the user data and access_token from authentication code

    Parameter 
    ----------
    request : Request

    code : string
               This will automatically passed as parameter by microsoft
    
    Return
    -------
    Template response that will take to microsoft proxy that makes cookie and redirects
    """
    mylogger.info(state,"State")
    # raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Unable to validate social login")
    if "error" in request:  # Authentication/Authorization failure
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Unable to validate social login")
    # mylogger.info(request.get("code"))
    # mylogger.info(request.values)
    if code:
        cache = _load_cache()
        cca = _build_msal_app(cache=cache)
        result = cca.acquire_token_by_authorization_code(
            code,
            scopes=SCOPE,  # Misspelled scope would cause an HTTP 400 error here
            redirect_uri=API_LOCATION+"/api/v1"+TOKEN_ENDPOINT)
        mylogger.info(result)
        mylogger.info("Scopes : ",SCOPE)
        if "error" in result:
            mylogger.info(result)
            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Unable to validate social login")
        token_to_encode = result.get("id_token_claims")
        accounts = cca.get_accounts()
        token = cca.acquire_token_silent(SCOPE,account=accounts[0])
        real_token = token["access_token"]
        refresh_token = result["refresh_token"]
    else:
        mylogger.info("NO CODE")
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="Unable to validate social login")
    
    try:
        email = token_to_encode["preferred_username"]
        username = token_to_encode["name"]  #username is not actual username; it is just full name; actual username is email
    except:
        username = email.split("@")[0]
    
    # https://stackoverflow.com/questions/29895279/get-profile-picture-from-azure-active-directory LINK FOR PROFILE PICTURE FOR LATER USE
    authenticated_user = crud.authenticate_user_email(email)
    oid = token_to_encode['oid'].split('-')
    oid = oid[-2]+oid[-1]

    if authenticated_user is None:
        authenticated_user = crud.create_user_microsoft(username, email, oid)  # create a new user here if not exists
    crud.add_third_party(email,'microsoft')
    return json.dumps({"request":request, "token":real_token, "email":email, "redirect_url":state, "reftoken":refresh_token})


@security_router.post(f"{SWAP_TOKEN_ENDPOINT}", response_model=Token, tags=["security"])
async def get_auth_token(request: Request = None,gat:GetAuthToken=Depends()):
    """
    function to make and set cookie of the access token

    Parameter 
    ----------
    request : Request
    
    Return
    -------
    returns a JSON response which includes the cookie details and sets the cookie
    """
    formdata = gat.__dict__
    token = formdata["sub"]
    response = JSONResponse({"access_token": token, "token_type": "bearer"})

    ###################### SUCCESSFULLY LOGED IN USING MICROSOFT ########################
    crud.logged_in(formdata["email"],"microsoft",request)

    response.set_cookie(
        key=COOKIE_AUTHORIZATION_NAME,
        value=f"Bearer {token}",
        domain=COOKIE_DOMAIN,
        httponly=True,
        max_age=3600,          # 1 hours
        expires=3600,          # 1 hours
    )
    return response


def get_new_microsoft_token(refresh_token: str = ""):
    ################################# Somehow get new token ######################################
    r = make_api_call('POST','https://login.microsoftonline.com/common/oauth2/v2.0/token',refresh_token,body={'client_id':CLIENT_ID,'grant_type':'refresh_token','scope':SCOPE,'refresh_token':refresh_token,'redirect_uri':API_LOCATION+"/api/v1"+TOKEN_ENDPOINT,'client_secret':CLIENT_SECRET})
    mylogger.info(r)
    if "error" in r:
        return ""
    token = r["access_token"]
    return token


def get_update_microsoft_service(email: str, calendar:dict):
    access_token = calendar["access_token"]
    r = make_api_call('GET', "https://graph.microsoft.com/v1.0/me/calendar", access_token)
    if "error" in r:
        # try to make new token using refresh_token
        if calendar["refresh_token"]=="":
            return {"SOMETHING IS GOING SERIOUSLY WRONG IN API"}
        new_token = get_new_microsoft_token(calendar["refresh_token"])
        if new_token == "":
            ################### re sign in ###############################
            auth_url = _build_auth_url(scopes=SCOPE,state="/add-microsoft-calendar")
            auth_url = parse.quote(str(auth_url))
            mylogger.info(auth_url)
            return RedirectResponse(url=f"/api/v1/add-calendar?auth_url={auth_url}&ltype=microsoft",status_code=status.HTTP_303_SEE_OTHER)
        r = make_api_call('GET', "https://graph.microsoft.com/v1.0/me/calendar", new_token)
        mylogger.info(r)
        if "error" in r:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Unable to validate social login")
        calendar = crud.get_add_user_calendar(current_user["_key"], "microsoft", new_token)   


# @security_router.get("/microsoft_signin", tags=["security"])
# async def microsoft_sign_in_button(request: Request, redirect_url: Optional[str]=None) -> "html page":
#     """
#     Just present the page for html microsoft button.
#     """

#     #IF REDIRECT URL is not present means user has custom signed in so redirect to add organization page else redirect to home page
#     if redirect_url is None:
#         redirect_url = '/add-organization'
#     else:
#         redirect_url = '/'
#     auth_url = _build_auth_url(scopes=SCOPE,state=redirect_url)
#     return templates.TemplateResponse("microsoft_signin.html", {"request":request, "redirect_url": redirect_url, "auth_url": auth_url})

# logout system needs to be improved (probably)
@security_router.get("/logout_microsoftuser")
async def route_logout_and_remove_cookie(request: Request) -> "RedirectResponse":
    """
    To delete the cookie from web and also sign out from microsoft
    """
    response = RedirectResponse(url=AUTHORITY + "/oauth2/v2.0/logout?post_logout_redirect_uri=/api/v1/login", status_code=status.HTTP_303_SEE_OTHER)
    response.delete_cookie(key=COOKIE_AUTHORIZATION_NAME, domain=COOKIE_DOMAIN)
    return response


@security_router.get("/microsoftusers/me")
async def my_page(request: Request, current_user: User = Depends(get_current_active_user)) -> "Response":
    """
    Just a temporary page for successfull signin
    """
    return json.dumps({"request": request})


######################################################################################
####################### END OF MICROSOFT SIGNIN AUTHENTICATION #######################
######################################################################################



######################################################################################
######################## PUSHER NOTIFICATION AUTHENTICATION ##########################
######################################################################################

# Author : Priyav Kaneria
@security_router.get('/pusher/beams-auth')
def beams_auth(current_user: User = Depends(get_current_active_user)):
    # Do your normal auth checks here 🔒
    if not current_user:
        return 'Inconsistent request', status.HTTP_401_UNAUTHORIZED
    user_id = current_user.email # get it from your auth system
    beams_token = beams_client.generate_token(user_id)
    return jsonable_encoder(beams_token)


######################################################################################
#################### END OF PUSHER NOTIFICATION AUTHENTICATION #######################
######################################################################################

######################################################################################
################################ EMAIL VERIFICATION ##################################
######################################################################################
conf = ConnectionConfig(
    MAIL_USERNAME="contact@aztlan.in",
    MAIL_PASSWORD="Aztlan@123",
    MAIL_PORT=587,
    #MAIL_FROM = "contact@aztlan.in",
    MAIL_SERVER="smtp.office365.com",
    MAIL_TLS=True,
    MAIL_SSL=False
)


def get_message_template(token:str):
    html = f"""
    <html> 
    <body>
    <p>Hi This test verification mail
    <br>Welcome to workpeer</p> 
    <br>use below link to verify your email id
        <a href ="{'www.workpeer.in/api/v1/verify-email-token/'+token}">Verify email</a>
    </body> 
    </html>
    """
    return html

@security_router.get("/verify-email/{email}")
async def send_verify_email_background(request: Request, background_tasks: BackgroundTasks, email: str):
    token = jwt.encode({"email":email}, JWT_SECRET_KEY, algorithm=ALGORITHM)
    fm = FastMail(conf)
    message = MessageSchema(
        subject="Verify your email id",
        recipients=[email],
        body=get_message_template(token),
        subtype="html"
        )
    # await fm.send_message(message)
    background_tasks.add_task(fm.send_message, message)
    return json.dumps({'request':request, 'email':email})

@security_router.get("/verify-email-token/{token}")
async def verify_email(request: Request, token: str):
    token = token.split("&")[0]
    payload = jwt.decode(token, JWT_SECRET_KEY,algorithms=[ALGORITHM])
    if payload["email"]:
        if not crud.email_successfully_verified(payload["email"]): # This function return false if user already verified and true after activating account if not verified
            raise HTTPException(201, "Already verified email or user doesn't exist")
    else:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, "Invalid link for current user")
    return RedirectResponse(url=f"/api/v1/homepage",status_code=status.HTTP_303_SEE_OTHER)


######################################################################################
############################ END OF EMAIL VERIFICATION ###############################
######################################################################################

######################################################################################
################################ TEMPORARY HOMEPAGE ##################################
######################################################################################


# @security_router.get("/homepage")
# async def my_page(request: Request, redirect_url: Optional[str] = "", current_user: User = Depends(get_current_active_user)) -> "Response":
#     """
#     Just a temporary page for homepage
#     """
#     if not current_user:
#         return RedirectResponse(url="/api/v1/login/?redirect_url=homepage", status_code=status.HTTP_303_SEE_OTHER)
#     # Getting the user details
#     current_user = crud.get_user(current_user.email)
#     if current_user["email_verified"] == False:
#         return RedirectResponse(url="/api/v1/verify-email/", status_code=status.HTTP_303_SEE_OTHER)

#     if crud.get_user_third_party(current_user["_key"])=="custom":
#         # User not in any org
#         mylogger.info(current_user)
#         if crud.get_user_org(current_user["_key"])=="":
#             return RedirectResponse(url="/api/v1/add-organization/", status_code=status.HTTP_303_SEE_OTHER)
#         # User in org but not added calendar
#         # if not current_user["onboarded_yn"]:
#         #     auth_url = _build_auth_url(scopes=SCOPE,state="/add-microsoft-calendar")
#         #     auth_url = parse.quote(str(auth_url))
#         #     return RedirectResponse(url=f"/api/v1/add-calendar?auth_url={auth_url}",status_code=status.HTTP_303_SEE_OTHER)
#         # calendar = crud.get_add_user_calendar(current_user["_key"])
#         # if calendar["cal_type"]=="microsoft":
#         #     # Just updating microsoft calendar creds if user is of microsoft calendar
#         #     get_update_microsoft_service(current_user['_key'], calendar)

#     # User is logged in using 3rd party login and not in any org
#     # if current_user["onboarded_yn"] == False:
#     #     return RedirectResponse(url="/api/v1/add-organization/", status_code=status.HTTP_303_SEE_OTHER)

#     # User is onboarded and passes all constraints
#     users_org = crud.get_user_org(current_user["_key"]) # getting user org
#     is_admin = crud.check_org_admin(users_org,current_user["_key"]) # whether user is org admin or not
#     return templates.TemplateResponse("main.html", {"request": request,"current_user":current_user["_key"],"instanceid":"13917092-3f6f-49e5-b39b-e21c89f24565","organization":users_org,"isadmin":is_admin})
