import logging
from os import path

def init_logger_singleton(type):
    mylogger = logging.getLogger(name='admin')
    mylogger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('[%(name)s:%(asctime)s:%(module)s:%(lineno)s:%(levelname)s:%(message)s]')
    mypath=path.join(path.abspath(path.dirname(__file__)),'logs')
    filehandler = logging.FileHandler(path.join(mypath, f'{type}.log'))
    filehandler.setLevel(logging.DEBUG)
    filehandler.setFormatter(formatter)
    mylogger.addHandler(filehandler)
    return mylogger

#mylogger = init_logger_singleton("NAME_OF_LOG_FILE")

# The info is to be used when we want to just log a message of successful login, etc..
# mylogger.info(message)
    
# The debug is to be used for development time for making sure what happened during the processflow.
# It is alternative to print statements.
# mylogger.debug(message)

# To be used when some error or event can lead to failure of task but currently task is working.
# mylogger.warning(message)

# To be used when an error leads to failure in execution of task
# mylogger.error(message)

# To be used when task has absolutely failed and is not working at all.
# mylogger.critical(message)
