"""
this file contains
All the meetings related operations that includes
1. meetings creation
2. cancelling meetings
3. modifying meetings
4. contacts
5. past meetings related stuff and their summary

"""


# third party imports
# -------------------
import os, re, json, pika
from time import gmtime, strftime
import base64
from dateutil import tz
import tzlocal
import datetime as dt
from datetime import datetime
from datetime import date, time, timedelta
from fastapi import FastAPI, BackgroundTasks, APIRouter, Request, Form, status, Depends, Body
from typing import List, IO, Optional
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse, RedirectResponse, Response, JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi_mail import FastMail, MessageSchema,ConnectionConfig

from fastapi import File, UploadFile
from fastapi.responses import FileResponse, HTMLResponse, StreamingResponse
from pathlib import Path
import shutil
from tempfile import NamedTemporaryFile
from typing import Callable
from fastapi.middleware.cors import CORSMiddleware

from typing import IO
from fastapi import Header, Depends, HTTPException
from starlette import status

import multiprocessing
import tempfile

# local application imports
# -------------------------
#import datetime
from app.schemas.schemas import (
    MeetingItem,
    UserMeetingEdgeItem,
    MeetingSummary,
    TeamsItem,
    UserTeamEdge,
    ActiveHours,
    BusyHours,
    CommonSlots,
    ProjectItem,
    InviteMembers,
    DeleteMembers,
    InviteEmailSchema,
    Org,
    Create,
    CreateWebex,
    Token,
    CreateMeetingGet,
    CreateMeetingPost,
    Username,
    DeleteMember,
    DeleteAdmin,
    ModifyAdmin,
    TeamEdgeProject,
    #AddNewItem,
    GenerateMeetingLink,
    UpdateMeeting,
    MeetingReview,
    CreateAction,
    InsertDecision,
    AddComment,
    AddNotes,
    UpdateGoogleEvent,
    AddWorkspace,
    Createorganization,
    AddAgenda,
    SearchData,
    Markdown,
    WebexToken,
    AddTeamMember,
    ModifyDecision,
    ModifyAgenda,
    TaskComment,
    ModifyTask,
    AssignTask,
    NewTask,
    UpdateEventMicrosoft,
    CreateProject,
    CreateTeam,
    ContactUs,
    UpdateTeam
)
from app.services.crud import (
    insert_meetings,
    insert_user_meeting_edge,
    create_team,
    add_team_member,
    delete_team_member,
    delete_team_admin,
    get_check_list,
    add_list_to_record,
    add_item_to_record,
    delete_user_record,
    delete_user_record_item,
    insert_file,
    create_project,
    delete_project,
    modify_project,
    connect_team_project,
    get_third_party_creds,
    json_to_credentials_object,
    credentials_obj_to_json,
    create_google_cal_json,
    scopes,
)
from app.services import crud
from app.models.models import (
    user,
    meetings,
    orgs,
    graph,
    db_conn,
    user_meeting,
    check_list,
    Files,
    project,
    user_creds,
    teams,
)

from app.routes.v1.security import get_current_active_user,User

from app.models import models
from app.services.eventlogger import event_processor
from .logger import init_logger_singleton
from passlib.context import CryptContext
from markdown import markdown
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.http import MediaFileUpload
from google.auth.transport.requests import Request as req
from google.oauth2.credentials import Credentials




meetings_router = APIRouter()

# Logging setup -- Please read logger.py file to use this logger
mylogger = init_logger_singleton('meetings')

# extracing the template paths
# ----------------------------
# give the relative path to the templates for jinja2 rendering
template_dir = os.path.dirname(
    os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
)
file_dir = template_dir

# Now join the path to the actual template containing folder
template_dir = os.path.join(template_dir, "templates")
template_dir = os.path.join(template_dir, "meetings-templates")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


# Intilize templates variable by defining the containing directory
# Intilize templates variable by defining the containing directory
templates = Jinja2Templates(directory=template_dir)


#########################################################################
###############google calendat inegration################################
scopes = ['https://www.googleapis.com/auth/calendar','https://www.googleapis.com/auth/drive.file']#.readonly']

def get_service(name,service):
    """Shows basic usage of the Google Calendar API or Google Drive API.
    this functionality is to connect to the google calendar or drive
    For calendar - mylogger.infos the start and name of the next 10 events on the user's calendar.
    """
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    # if os.path.exists('token.pickle'):
    #     with open('token.pickle', 'rb') as token:
    #         creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if user.has(name):
        creds = user_creds.get(name)
        mylogger.info(creds)
        if creds is not None:
            creds = creds['oauth_token']
            creds = json_to_credentials_object(creds)
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds = json_to_credentials_object(creds)
            creds.refresh(req())
            user_creds.update({'_key':name,'oauth_token':credentials_obj_to_json(creds)})
        else:
            details = crud.get_third_party_creds()
            mylogger.info(details)
            flow = InstalledAppFlow.from_client_config(
                {'installed':details}, scopes)
            mylogger.info("checking for credentials from json............")
            creds = flow.run_local_server(host = '127.0.0.1', port=0)
            mylogger.info(creds)
            if not user_creds.has(name):
                user_creds.insert({'_key':name, 'oauth_token': credentials_obj_to_json(creds)})
            else:
                user_creds.update({'_key':name,'oauth_token':credentials_obj_to_json(creds)})
    try:
        mylogger.info(creds)
        if service in ["calendar","drive"]:
            service = build(service, 'v3', credentials=creds, cache_discovery=False)
    except Exception as e:
        mylogger.info("error",e)

    return service



def create_google_cal_json(summary, location, description, start_time, end_time, invitees,meetingProvider, timezone = 'Asia/Kolkata'):
    """
    this function will convert the raw request into the json file
    format that a google calendar required.
    parameters:
    @summary : agenda of the meeting here
    @location: location of the meeting
    @description: about the agenda/meeting description
    @start_time: starting date and time of the meeting
    @end_time: ending date and time of the meeting
    @meetingProvider: like google meet
    @timezone: by default it is set for Asia/Kolkata(it has to be changed
    automatically, it will be handled manually for the time being.)

    @return : return the clean json format that is required for google calendar

    """
    attendees = [{'email':invitee} for invitee in invitees]
    mylogger.info(attendees)
   
    #for invitee in invitees:
        #mylogger.info("Invitees",invitee["emailAddress"]["address"])
    event = {
        'summary': summary,
        'location': location,
        'description': description,
        'start': {
        'dateTime':start_time.strftime("%Y-%m-%dT%H:%M:%S"),
        'timeZone': timezone,
        },
        'end': {
        'dateTime': end_time.strftime("%Y-%m-%dT%H:%M:%S"),
        'timeZone': timezone,
        },
        'attendees': attendees,
        'reminders': {
            'useDefault': False,
            'overrides': [
              {'method': 'email', 'minutes': 24 * 60},
              {'method': 'popup', 'minutes': 10},
            ],
        },
        'conferenceData': {
            "createRequest": {
                "requestId": start_time.strftime("%Y-%m-%dT%H:%M:%S"),
                "conferenceSolutionKey": {
                    "type": meetingProvider,
                },
            },
        }
    }

    return event


#########################################################################




def Meeting_Card(meet):

    """

    This function's objective is to make list
    of data of a specific meeting.

    Parameter
    ---------
    parameter 'meet' is assumed to be key of 
    meeting of whose data we want access.

    Return
    ------
    Just return all the data stored at vertex 
    node of meeting with key 'meet'.

    """

    return graph.traverse(
        start_vertex="meetings/" + str(meet),
        strategy="bfs",
        direction="outbound",
        max_depth="1",
    )["vertices"][0]


def Meeting_Hoster(meet):

    """

    This function's objective is to find the
    host of specific meeting which stored on 
    edges connecting to that meeting node.

    Parameter
    ---------
    parameter 'meet' is assumed to be key of
    meeting of whose host we want to know.

    Return
    ------
    Just return all the 'HOST' of the vertex 
    node of meeting with key 'meet'.

    """

    forReturn = graph.traverse(
        start_vertex="meetings/" + str(meet),
        strategy="bfs",
        direction="inbound",
        max_depth="1",
    )["paths"][1:]

    for items in forReturn:
        if items["edges"][0]["host"] == "yes":
            HOST = items["vertices"][1]["name"]

    return HOST


def Meeting_Card_List(user_name):

    """

    This function's objective is to provide all
    the data of meeting node and corresponding 
    edge w.r.t a speific user.

    Parameter
    ---------
    parameter 'user_name' is that user of whom we
    want to know all the meeting history data .

    Return
    ------
    Returns a list of dict ( dict has two types of 
    data 1- on edge 2- on corr. meeting node).

    One dict contain all the data of user_name and one 
    of it's meeting vertex in database graph.

    """

    Data_List = []

    PATH = graph.traverse(
        start_vertex="account/" + str(user_name),
        strategy="bfs",
        direction="outbound",
        max_depth="1",
    )["paths"][1:]

    for eachpath in PATH:
        EDGE = eachpath["edges"][0]
        VERTEX = eachpath["vertices"][1]
        Data_List.append({"edge": EDGE, "vertex": VERTEX})

    return Data_List


def Meeting_Timeline(meeting_data):

    """

    This function's objective is to return
    when was the meeting happened :-
    1- Today
    2- Yesterday
    3- Past
    
    Parameter
    ---------

    parameter 'meeting_data' is the meeting
    of whose timeline we want to return.

    Return
    ------
    Return the corresponding time in string 
    format of the above 3 options of the 
    meeting i.e. when the meeting happened ,
    in 'Past' or 'Yesterday' or 'Today'.

    """

    today = date.today()
    yesterday = today - timedelta(days=1)

    Today_year = str(today.year)
    Today_month = str(today.month)
    Today_day = str(today.day)

    Yesterday_year = str(yesterday.year)
    Yesterday_month = str(yesterday.month)
    Yesterday_day = str(yesterday.day)

    fulldate = meeting_data["date"]
    match = re.search(r"([\d]*)-([\d]*)-([\d]*)", fulldate)
    meeting_day = match.group(1)
    meeting_month = match.group(2)
    meeting_year = match.group(3)

    if (
        meeting_year == Today_year
        and meeting_month == Today_month
        and meeting_day == Today_day
    ):
        return "Today"
    if (
        meeting_year == Yesterday_year
        and meeting_month == Yesterday_month
        and meeting_day == Yesterday_day
    ):
        return "Yesterday"
    else:
        return "Past"


def Sorted_Meeting_Card_Data(user_name):

    """

    Given a user , this function validates
    whether it is present in our database .
    If yes then it fetches all the meeting 
    data related to data user and divide it
    into three different timeline today , 
    yeterday or Past , with One more level of partition
    whether it was hosted by you or not.

    Parameter
    ---------
    parameter 'user_name' is the name of user of
    whose all meeting history data we want to access.

    Return
    ------
    Return a dict of three dict (i.e timelines), which contain list of
    data two different tags.
    In short it returns all the data of meeting history related to username
    passed as a parameter.

    """

    Data_List = []
    Data_Dict = {}
    Data_Dict["user_registered"] = 1
    Data_Dict["user"] = user_name

    if not user.has(user_name):
        Data_Dict["user_registered"] = "0"
        return Data_Dict
    else:
        Data_List = Meeting_Card_List(user_name)

    Today_meeting_history = {"sent": [], "received": []}
    Yesterday_meeting_history = {"sent": [], "received": []}
    Past_meeting_history = {"sent": [], "received": []}

    for meeting in Data_List:
        meeting_timeline = Meeting_Timeline(meeting["vertex"])

        host = meeting["edge"]["host"]

        meeting["vertex"]["host"] = "you"

        if host == "yes":
            if meeting_timeline == "Today":
                Today_meeting_history["sent"].append(meeting["vertex"])
            elif meeting_timeline == "Yesterday":
                Yesterday_meeting_history["sent"].append(meeting["vertex"])
            else:
                Past_meeting_history["sent"].append(meeting["vertex"])

        else:
            meeting["vertex"]["host"] = Meeting_Hoster(meeting["vertex"]["_key"])

            if meeting_timeline == "Today":
                Today_meeting_history["received"].append(meeting["vertex"])
            elif meeting_timeline == "Yesterday":
                Yesterday_meeting_history["received"].append(meeting["vertex"])
            else:
                Past_meeting_history["received"].append(meeting["vertex"])

    Data_Dict["Today"] = Today_meeting_history
    Data_Dict["Yesterday"] = Yesterday_meeting_history
    Data_Dict["Past"] = Past_meeting_history
    return Data_Dict


# @orders_router.get('/contact')
# ignore this one.. this one has to be moved to testing file
@meetings_router.get("/tables-check")
def tablesCheck():
    return {models.code_check()}


@meetings_router.post("/new_meeting/")
async def new_meeting(meeting: MeetingItem,  current_user: User = Depends(get_current_active_user)):

    """
    This is a simple route for making a new meeting.

    Parameter
    ---------
    Function parameter 'meeting' is a pydantic model
    of type 'meeting_item'.

    Return
    ------
    Returns the newly created meeting data.

    and more to be added.....

    """
    if current_user==None:
        redirect_url="new_meeting/"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    return insert_meetings(meeting)


@meetings_router.post("/new_user_meeting_edge/")
async def new_user_meeting_edge(user_meeting_edge: UserMeetingEdgeItem, current_user: User = Depends(get_current_active_user)):

    """
    This is a simple route for making a new user - meeting edge.

    Parameter
    ---------
    Function parameter 'user_meeting_edge' is a pydantic model
    of type 'user_meeting_edge_item'.

    Return
    ------
    Returns the newly created user-meeting edge-data.

    and more to be added.....

    """
    if current_user==None:
        redirect_url="new_user_meeting_edge/"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    return insert_user_meeting_edge(user_meeting_edge)


@meetings_router.get("/history/", response_class=HTMLResponse)
async def get_history(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    This function under the above route respond with 
    the Form to submit a specific user name 
    and get all meetings history related to 
    user .

    Parameters
    ----------
    No specific parameter just the trivial 'request' one.

    return
    ------
    This function just returns HTML page 'meeting_history.html'
    from the src/app/templates folder.

    """
    if current_user==None:
        redirect_url="history/"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
    # mylogger.info(os.path.dirname(__file__))
    return templates.TemplateResponse("meeting_history.html", {"request": request})


@meetings_router.get("/workspaces")
async def get_my_workspaces(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    Api to get the list of all workspaces
    based upon the current user
    returns the list
    """
    workspace_details = {}
    #print(current_user.__dict__)

    if current_user is None:# or current_user == '' or current_user == False or len(current_user.strip()) == 0:
        redirect_url="workspaces"
        #return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })
    else:
        users_org = crud.get_all_user_orgs(current_user.email)
        if crud.get_user_org(current_user.email)=="":
            if users_org is None:
                return JSONResponse({
                    'status_code': status.HTTP_100_CONTINUE,
                    'url': f"/api/v1/add-organization"
                    })
            #return RedirectResponse(url = f"/api/v1/add-organization", status_code=status.HTTP_303_SEE_OTHER)
        users_org = crud.get_all_user_orgs(current_user.email)
        if len(users_org) == 0:
            return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/create_org_account",
                'redirect_url':f"api/v1/workspaces"
                })

        #workspace_details['joined'] = users_org
        workspace_details['joined'] = []
        workspace_details['invited'] = crud.get_pending_invitations(current_user.email)
        for org in users_org:
            details = crud.get_org_det_for_workspaces(org)
            workspace_details['joined'].append(details)
        #workspace_details['invited'] = ['']
        return JSONResponse({"status_code":status.HTTP_200_OK, "data":workspace_details, 'detail':'wortkspaces'})
        #return templates.TemplateResponse("workspaces.html", {"request": request, "workspaces": users_org})


@meetings_router.get("/select_workspace/{workspace_name}")
async def select_current_workspace(workspace_name: str, current_user: User = Depends(get_current_active_user)):
    selected = crud.select_workspace_user(workspace_name, current_user.email)
    if selected:
        return JSONResponse({
            'status_code': status.HTTP_100_CONTINUE,
            'detail':'homepage',
            'url': "/api/v1/homepage"
            })
        #return RedirectResponse(url = "/api/v1/homepage", status_code=status.HTTP_303_SEE_OTHER)
    else:
        return JSONResponse({
            'status_code': status.HTTP_400_BAD_REQUEST,
            'detail':'"Could not switch organization. Please logout and login again to authorized org"',
            #'url': "/api/v1/homepage"
            })

        #return JSONResponse("Could not switch organization. Please logout and login again to authorized org",status_code=401)

@meetings_router.get('/home')
async def get_homepage_details(request: Request, current_user: User = Depends(get_current_active_user)):
    if current_user==None:
        redirect_url="home/"
        #return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login/?redirect_url={redirect_url}"
                })
    user_org = crud.get_user_org(current_user.email)
    upcoming_meetings = crud.get_upcoming_meetings(current_user.email)
    user_tasks = crud.get_all_org_tasks(current_user.email, user_org)
    user_decisions = crud.get_all_org_decisions(current_user.email, user_org)
    home = {}
    home['organization'] = user_org
    home['meetings'] = upcoming_meetings
    home['tasks'] = user_tasks
    home['decisions'] = user_decisions
    #return templates.TemplateResponse("home.html", {"request": request, "workspace": user_org,"meetings": upcoming_meetings, "tasks": user_tasks, "decisions": user_decisions})
    return  JSONResponse({
        'status_code': status.HTTP_200_OK,
        'data':home,
        'detail':'dictionary of homepage details'
        })
    



@meetings_router.post("/userhistory/", response_class=HTMLResponse)
async def User_History(request: Request,username: Username=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    This  route is for specific user's
    meeting history.

    Parameter
    ---------
    username are the only and necessary parameter for
    this function that will be used to get all the meetings history 
    data of the respective user.

    [We assume unique username]

    return
    ------
    This function under the above route will return the meetings related
    to provided username in the form.

    """
    if current_user==None:
        redirect_url="userhistory/"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    var = username.__dict__
    var = var["username"]
    return templates.TemplateResponse(
        "user_meeting_history.html",
        {"request": request, "Meeting_Data": Sorted_Meeting_Card_Data(var)},
    )


@meetings_router.get(
    "/{history}/fullhistory/{meetingname}", response_class=HTMLResponse
)
async def show_history(meetingname: str, request: Request, current_user: User = Depends(get_current_active_user)):

    """
    This route is built for full detail version of each meeting
    happened in the past i.e. full details for a specific 
    meeting history.
    """
    if current_user==None:
        redirect_url="{history}/fullhistory/{meetingname}"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse(
        "Specific_meeting_history.html",
        {"request": request, "Meeting_Data": Meeting_Card(meetingname)},
    )


# Teams API now


# @meetings_router.get("/create_team", response_class=HTMLResponse)
# async def create_new_team(request: Request, current_user: User = Depends(get_current_active_user)):

#     """
#     This route has been built to return a form for creating a new team.
#     """
#     if not current_user:
#         return RedirectResponse(url="/api/v1/login?redirect_url=create_team", status_code=status.HTTP_303_SEE_OTHER)
#     users_org = crud.get_user_org(current_user.email) # getting user org
#     is_admin = crud.check_org_admin(users_org,current_user.email) # whether user is org admin or not
#     if is_admin:
#         return templates.TemplateResponse("create_team.html", {"request": request})
#     else:
#         return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not organization admin")


# This route requires form validation , which is to be done later.
@meetings_router.post("/create_team", response_class=HTMLResponse)
async def post_create_new_team(request: Request, team: CreateTeam=Depends(), current_user: User = Depends(get_current_active_user)):

    """
    This route has been built to save the data of form from create_team
    route in the database.
    
    """
    if not current_user:
        redirect_url = "create_team"
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })
    users_org = crud.get_user_org(current_user.email) # getting user org
    is_admin = crud.check_org_admin(users_org,current_user.email) # whether user is org admin or not
    #print(is_admin)
    if not is_admin:
        #return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not organization admin")
        return JSONResponse({
            'status_code': status.HTTP_403_FORBIDDEN,
            'detail':'not authorized to create team'
            })
    data = team.__dict__#await request.form()
    data['members'] = data['members'].split(',')
    #data['members'].append(current_user.email)
    if crud.check_users_status(data['members']) == False:
        return JSONResponse({
            'status_code':status.HTTP_404_NOT_FOUND,
            'detail':'user not found'
            })

    #print(data)
    formdata = data
    formdata["admin"] = current_user.email
    newly_formed_team = create_team(formdata)
    print(newly_formed_team)
    #event_processor("CreateTeam", newly_formed_team)
    try:
        folder_path = os.path.join(file_dir,"resources")
        folder_path = os.path.join(folder_path,users_org)
        folder_path = os.path.join(folder_path,"teams")

        team_path = os.path.join(folder_path,data["TeamName"])
        os.mkdir(team_path)
        os.mkdir(os.path.join(team_path,"meetings"))
    except Exception as e:
        print(e)

    # return RedirectResponse(
    #     url="/api/v1/create_team", status_code=status.HTTP_303_SEE_OTHER
    # )
    if newly_formed_team == True:
        return JSONResponse({
            'status_code': status.HTTP_201_CREATED,
            'detail':'data created successfully...'
            })
    else:
        return JSONResponse({
            'status_code':status.HTTP_409_CONFLICT,
            'detail':'dupllicates, already a team is created witht that name'
            })


@meetings_router.get("/delete_team/{team_name}", response_class=HTMLResponse)
async def delete_team(request: Request, team_name: str, current_user: User = Depends(get_current_active_user)):
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=myteams", status_code=status.HTTP_303_SEE_OTHER)
    #users_org = crud.get_user_org(current_user.email) # getting user org
    is_admin = crud.check_team_admin(team_name,current_user.email) # whether user is org admin or not
    if not is_admin:
        return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not organization admin")
    # deleted_team = crud.delete_team(team_name)
    # event_processor("DeleteTeam", deleted_team)
    mylogger.info("deleted")
    return RedirectResponse(
        url="/api/v1/myteams", status_code=status.HTTP_303_SEE_OTHER
    )


@meetings_router.get("/add-team-member", response_class=HTMLResponse)
async def add_new_member(request: Request, current_user: User = Depends(get_current_active_user)):

    """
    This route has been built to return a form for adding a member to team.
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=add-team-member", status_code=status.HTTP_303_SEE_OTHER)
    users_org = crud.get_user_org(current_user.email) # getting user org
    is_admin = crud.check_team_admin(users_org,current_user.email) # whether user is org admin or not
    if is_admin:
        return templates.TemplateResponse("add-team-member.html", {"request": request})
    else:
        return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not organization admin")
    


# This route requires form validation , which is to be done later.
@meetings_router.post("/add-team-member", response_class=HTMLResponse)
async def post_add_new_member(atm:AddTeamMember=Depends(),  current_user: User = Depends(get_current_active_user)):

    """
    This route has been built to save the data of form from add-team-member
    route in the database.
    
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=add-team-member", status_code=status.HTTP_303_SEE_OTHER)
    formdata = atm.__dict__
    team_name = formdata['TeamName']
    #users_org = crud.get_user_org(current_user.email) # getting user org
    is_admin = crud.check_team_admin(team_name,current_user.email) # whether user is org admin or not
    if not is_admin:
        return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not organization admin")
    formdata = atm.__dict__
    newmember = add_team_member(formdata)
    event_processor("AddNewMember", newmember)
    return RedirectResponse(
        url=f"/api/v1/{formdata['TeamName']}/display_team", status_code=status.HTTP_303_SEE_OTHER
    )


@meetings_router.get("/{team_name}/display_team", response_class=HTMLResponse)
async def display_members(request: Request, team_name: str, current_user: User = Depends(get_current_active_user)):

    """
    This route has been built to display all team members.
    
    """
    if not current_user:
        #return RedirectResponse(url="/api/v1/login?redirect_url={team_name}/display_team", status_code=status.HTTP_303_SEE_OTHER)
        response = JSONResponse({
        'status_code':status.HTTP_303_SEE_OTHER,
        #'url':'login',
        'detail':'not authenticated.'
        })
    my_org = crud.get_user_org(current_user.email)
    members = crud.get_team_data(my_org,team_name)
    is_admin = crud.check_team_admin(team_name,current_user.email)
    #return templates.TemplateResponse("show_team_members.html", {"request": request,"members":members,"team_name":team_name, "is_admin":is_admin})
    return JSONResponse({
        'status_code':status.HTTP_200_OK,
        'data': members,
        'is_admin':is_admin
        })


@meetings_router.post('/{team_name}/update')
async def modify_team_details(team_name: str, update_team:UpdateTeam=Depends() ,current_user:User = Depends(get_current_active_user)):
    if current_user is None:
        redirect_url="/"+team_name+"/update"
        #return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })
    my_org = crud.get_user_org(current_user.email)
    org_teams = crud.get_org_teams(my_org)
    get_team_key = crud.get_team_key(team_name, org_teams)
    if get_team_key is None:
        crud.notify_user(current_user.email,"Update team", f"A team named {team_name} is not updated due to incorrect key/project name",f"/api/v1/{team_name}/data")
        return JSONResponse ({
            'status_code':status.HTTP_404_NOT_FOUND,
            'detail': "no team with the provided key",
            })
    update_status= crud.update_team(update_team, get_team_key)
    if update_status == True:
        print("wortking")
        
        crud.notify_user(current_user.email,"Update team", f"A Project named {team_name} is updated",f"/api/v1/{team_name}/update_project")
        return JSONResponse({
            'status_code':status.HTTP_201_CREATED,
            'detail':'updated_successfully'
            })

    elif update_status == False:
        crud.notify_user(current_user.email,"Update team", f"A team named {team_name} is not updated due to bad request",f"/api/v1/{team_name}/data")
        return JSONResponse({
            'status_code':status.HTTP_400_BAD_REQUEST,
            'detail':'issue in the request sent'
            })


@meetings_router.get("/myteams", response_class=HTMLResponse)
async def display_members(request: Request, current_user: User = Depends(get_current_active_user)) -> "Jinja2Templates":

    """
    This route has been built to display all your teams.
    
    """
    if not current_user:
       #return RedirectResponse(url=f"/api/v1/login/?redirect_url=myteams", status_code=status.HTTP_303_SEE_OTHER)
       response = JSONResponse({
        'status_code':status.HTTP_303_SEE_OTHER,
        #'url':'login',
        'detail':'not authenticated.'
        })
    my_teams = crud.get_org_user_teams(current_user.email)
    #teams_names = crud.get_user_teams(current_user.email)
    mylogger.info(my_teams)
    print(my_teams)
    #return teams_docs
    #teams_data = []
    #for my_team in teams_docs:
    #    #teams_data[team['_key']] = team
    #    print(my_team)
    #    teams_data.append(teams['_key'])

    return JSONResponse({
        'status_code':status.HTTP_200_OK,
        'data': my_teams

        })
    #return templates.TemplateResponse("show_teams.html", {"request": request, "user":current_user.email, "teams":teams_docs})



@meetings_router.get("/token")
async def token(request:Request,token:WebexToken=Depends()):
    with open(r'/root/1-on-1-meetings/src/app/routes/v1/db.json','r') as f:
        data = json.load(f)
    f.close()
    code = token.code
    state= token.state
    client_id=data['client_id']
    client_secret=data["client_secret"]
    message= client_id+":"+client_secret
    asci = message.encode('ascii')
    base64_bytes = base64.b64encode(asci)
    encoded = base64_bytes.decode('ascii')
    redirect_uri=data['redirect_uri']
    data=json.loads(state)
    topic=data["topic"]
    password=data["password"]
    datetime=data["datetime"]
    host=data["host"]
    duration=data["duration"]
    body={'grant_type' : 'authorization_code' , 'code' : code , 'redirect_uri' : redirect_uri }
    header={ 'Authorization' : 'Basic ' +  encoded }
    response=rq.post('https://zoom.us/oauth/token',data = body, headers= header)
    response=response.json()
    accessToken=response['access_token']
    with open(r'/root/1-on-1-meetings/src/app/routes/v1/db.json','r') as f:
        data = json.load(f)
    f.close()
    data['accessToken']=accessToken
    with open(r'/root/1-on-1-meetings/src/app/routes/v1/db.json','w') as f:
        json. dump(data, f)
    f.close()
    return RedirectResponse(url="http://api.workpeer.in/api/v1/create_meeting?topic="+topic+"&duration="+duration+"&password="+password+"&datetime="+datetime+"&host="+host)



@meetings_router.post("/create")
async def create(request:Request,create:Create = Depends()):
    with open(r'/root/1-on-1-meetings/src/app/routes/v1/db.json','r') as f:
        data = json.load(f)
    f.close()

    accessToken=data['accessToken']
    client_id=data['client_id']
    redirect_uri=data['redirect_uri']
    create.zoom_datetime=create.zoom_datetime+':00Z'
    body={"topic" : create.topic,"type" : 2,"start_time" : create.zoom_datetime,"duration" : create.duration,  "password" : create.password, "schedule_for": create.host, "settings":{"join_before_host":"false"} }
    header={ 'Authorization' : 'Bearer '+ accessToken }
    response=rq.post('https://api.zoom.us/v2/users/me/meetings',json = body, headers= header)
    print(response.status_code)
    try:
        response=response.json()
        print(response['join_url'])
    except:
        x={"topic" : create.topic,"datetime" : create.zoom_datetime,"duration" : create.duration,  "password" : create.password, "host": create.host }
        click="https://zoom.us/oauth/authorize?response_type=code&client_id="+client_id+"&redirect_uri="+redirect_uri+"&state="+json.dumps(x)
        return RedirectResponse(url=click)
    start=datetime.strptime(create.zoom_datetime[:-4], '%Y-%m-%dT%H:%M')
    end=start + timedelta(minutes = int(create.duration))
    data={"hostname":create.host,"date":create.zoom_datetime[:10],"meeting_subject":create.topic,"agenda":create.topic,"start_time":create.zoom_datetime[11:16],"end_time":end.strftime("%H:%M"),"link":response['join_url']}
    members=[create.host]
    db_insertion_status,meeting_id = crud.create_meeting(data, members,"TEAM_MEETING", "team")
    return templates.TemplateResponse("create_meetings.html",{"request": request, "msg": response['join_url']})










@meetings_router.get("/token_webex")
async def token_webex(request:Request,token:WebexToken=Depends()):
    with open(r'C:\Users\Sarthak\Desktop\Docs\project\1-on-1-meetings\src\app\routes\v1\db1.json') as f:
        data = json.load(f)
    f.close()
    code = token.code
    state= token.state
    redirect_uri=data["redirect_uri"]
    client_id=data["client_id"]
    client_secret=data["client_secret"]
    header={"Content-Type": "application/x-www-form-urlencoded"}
    body={'grant_type' : 'authorization_code' , 'code' : code , 'redirect_uri' : redirect_uri, "client_id": client_id, "client_secret": client_secret }
    response=rq.post('https://webexapis.com/v1/access_token',data = body, headers=header)
    response=response.json()
    accessToken=response['access_token']
    with open(r'C:\Users\Sarthak\Desktop\Docs\project\1-on-1-meetings\src\app\routes\v1\db1.json','r') as f:
        data = json.load(f)
    f.close()
    data['accessToken']=accessToken
    with open(r'C:\Users\Sarthak\Desktop\Docs\project\1-on-1-meetings\src\app\routes\v1\db1.json','w') as f:
        json. dump(data, f)
    f.close()
    x=state.split("@_")
    topic=x[0]
    end=x[1]+":"+x[5]
    start=x[2]+":"+x[6]
    password=x[3]
    invitee=x[4]
    return RedirectResponse(url="http://api.workpeer.in/api/v1/create_meeting?title="+topic+"&end="+end+"&password_webex="+password+"&start="+start+"&invitees="+invitee)

    
@meetings_router.post("/create_webex")
async def create_webex(request:Request,create:CreateWebex=Depends()):
    with open(r'C:\Users\Sarthak\Desktop\Docs\project\1-on-1-meetings\src\app\routes\v1\db1.json','r') as f:
        data = json.load(f)
    f.close()
    accessToken=data['accessToken']
    click=data["click"]
    topic=create.topic
    password=create.password
    start_datetime=create.start_datetime
    end_datetime=create.end_datetime
    invitees=create.invitees
    start_datetime=start_datetime+':00+05:30'
    end_datetime=end_datetime+':00+05:30'
    invite=invitees.split(',')
    arr=[]
    for x in invite:
        ar={"email":x,"displayName":x,"coHost":"false"}
        arr.append(ar)    
    body={"title" : topic,"end" : end_datetime,"start" : start_datetime,  "password" : password, "enabledAutoRecordMeeting": "false", "allowAnyUserToBeCoHost" : "false","invitees":arr }
    header={ 'Authorization' : 'Bearer '+ accessToken ,"Accept": "application/json"}
    response=rq.post('https://webexapis.com/v1/meetings',json = body, headers= header)
    print(response.json())
    try:
        response=response.json()
        print(response['webLink'])
    except:
        y=topic+"@_"+end_datetime[0:-12]+"@_"+start_datetime[0:-12]+"@_"+password+"@_"+invitees+"@_"+end_datetime[-11:-9]+"@_"+start_datetime[-11:-9]
        click=click+y
        return RedirectResponse(url=click,status_code=status.HTTP_303_SEE_OTHER)
    data={"hostname":"sarthakagarwal308@gmail.com","date":start_datetime[:10],"meeting_subject":topic,"agenda":topic,"start_time":start_datetime[11:16],"end_time":end_datetime[11:16],"link":response['webLink']}
    members=invite
    db_insertion_status,meeting_id = crud.create_meeting(data, members)
    return templates.TemplateResponse("create_meetings.html",{"request": request, "msg": response['webLink']})



@meetings_router.get("/{team_name}/create_meeting", response_class=HTMLResponse)
async def create_meetings(request: Request, team_name: str, create:CreateMeetingGet=Depends(), current_user: User = Depends(get_current_active_user)) -> "Jinja2Templates":
    """
    Main page to enter data to create meetings

    Parameter
    ----------
    request : Request

    Return
    -------
    Jinja2Templates respone
    """
    topic=create.topic    
    duration=create.duration
    password =create.password
    zoom_datetime=create.zoom_datetime
    host=create.host
    title =create.title
    password_webex =create.password_webex
    end=create.end
    start=create.start

    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url={team_name}/create_meeting", status_code=status.HTTP_303_SEE_OTHER)
    team_det = teams.get(team_name)
    if team_det['Disabled_yn'] == False:
        return f"{team_name} team is no longer available. Please contact your admin "
    if not crud.get_user_org(current_user.email):
        return "You are not linked to any organization"
    #users_org = crud.get_user_org(current_user.email) # getting user org
    is_admin = crud.check_team_admin(team_name,current_user.email) # whether user is org admin or not
    if is_admin:
        return templates.TemplateResponse("create_meetings.html", {"request": request,"team":team_name,"topic" : topic,"duration":duration,"password":password,"datetime":datetime[0:-4],"host":host,"title":title,"end":end,"start":start,"password_webex":password_webex})
    else:
        return JSONResponse(content=jsonable_encoder({'status_code':status.HTTP_403_FORBIDDEN, 'detail':"User not organization admin"}))



@meetings_router.post("/{team_name}/create_meet")
async def create_team_meeting(request: Request, team_name: str, files: List[UploadFile] = File(...), current_user: User = Depends(get_current_active_user)) -> "str response":
    """
    Use to create meeting for teams
    @author: Priyav
    Created on Mon Dec 07 10:03:19 2020
    
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    form data contains....
    @host: who is hosting the meeting
    @duration : total time of the meeting
    @date: date of the meeting
    @st_time: starting time of the meeting
    @end_time: ending time of the meeting
    @attendees: list of attendees of the meeting

    Return
    -------
    HTTP Status response: if meeting is create successfully, 
    it returns 200 and if fails, it throws 401
    """

    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url={team_name}/create_meeting", status_code=status.HTTP_303_SEE_OTHER)
    #users_org = crud.get_user_org(current_user.email) # getting user org
    team_det = teams.get(team_name)
    if team_det['Disabled_yn'] == False:
        return f"{team_name} team is no longer available. Please contact your admin "
    is_admin = crud.check_team_admin(team_name,current_user.email) # whether user is org admin or not
    if not is_admin:
        return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not organization admin")
    form = await request.form()
    data = jsonable_encoder(form)
    mylogger.info(data.keys())
    mylogger.info(data)
    mylogger.info(files)
    file_data = create_upload_files(files)
    mylogger.info("File data : ",file_data)
    mylogger.info("the team meeting data above...........")
    if data["linktext"]=="":
        del data["linktext"]
    else:
        data["link"] = data["linktext"]
        del data["linktext"]
    my_org = crud.get_user_org(current_user.email)
    team_members = crud.get_team_members(my_org, team_name)
    mylogger.info(team_members)
    #del team_members[data['hostname']]
    #mylogger.info(team_members)
    
    start_time = datetime.strptime(data['date']+' '+data['start_time']+':00', "%Y-%m-%d %H:%M:%S")
    end_time = datetime.strptime(data['date']+' '+data['end_time']+':00', "%Y-%m-%d %H:%M:%S")

    commonFreeSlots = get_common_slots(team_members, data['date'])
    #mylogger.info("The common free slots of the organization members..................\n",commonFreeSlots)
    start_time = start_time.replace(tzinfo=tz.tzlocal())
    #del team_members[hostname] 
    db_insertion_status,meeting_id = crud.create_meeting(data,team_members, "TEAM_MEETING", team_name)
    #mylogger.info(db_insertion_status)
    summary = data['meeting_subject']

    ################################## RABBIT MQ ######################################

    # connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    # channel = connection.channel()

    # channel.exchange_declare(exchange='topic_logs', exchange_type='topic')

    # routing_key = "NewMeeting.{}.{}.{}".format(data['hostname'],summary.replace(" ",""),str(start_time).replace(" ","-"))
    # message = f"Meeting created by {data['hostname']} for {summary} on {str(start_time)}"
    # channel.basic_publish(
    #     exchange='topic_logs', routing_key=routing_key, body=message)
    # mylogger.info(" [x] Sent %r:%r" % (routing_key, message))
    # connection.close()

    ################################## NOTIFICATIONS ####################################

    for invitee in team_members:
        crud.notify_user(invitee,"New team meeting scheduled", f"New team meeting created for {summary} at {start_time}",f"/api/v1/meeting_details/{meeting_id}", meeting_id)

    return "Meeting created Successfully"



@meetings_router.get("/{date}/project-free-slots")
async def get_project_free_slots(date: str, current_user: User = Depends(get_current_active_user)):
    """
    used to get free slot of a project that user is in...
    @parameters:
    date: date of the meetng to get free slots on.
    current_user: get the current logged in user details.
    """




@meetings_router.get("/delete-team-member", response_class=HTMLResponse)
async def delete_new_member(request: Request, current_user: User = Depends(get_current_active_user)):

    """
    This route has been built to return a form for removing a member from a team.
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=delete-team-member", status_code=status.HTTP_303_SEE_OTHER)
    users_org = crud.get_user_org(current_user.email) # getting user org
    is_admin = crud.check_org_admin(users_org,current_user.email) # whether user is org admin or not
    if is_admin:
        return templates.TemplateResponse("delete_team_member.html", {"request": request})
    else:
        return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not organization admin")


# This route requires form validation , which is to be done later.
@meetings_router.post("/{team_name}/delete-team-member", response_class=HTMLResponse)
async def post_delete_new_member(team_name: str, dm:DeleteMember=Depends(),  current_user: User = Depends(get_current_active_user)):

    """
    This route has been built to save the data of form from delete-team-member
    route in the database.
    
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=delete-team-member", status_code=status.HTTP_303_SEE_OTHER)
    #form_data = dm.__dict__
    #teamname = form_data['TeamName']
    team_det = teams.get(team_name)
    if team_det['Disabled_yn'] == True:
        return f"{team_name} team is no longer available. Please contact your admin "
    #users_org = crud.get_user_org(current_user.email) # getting user org
    is_admin = crud.check_team_admin(team_name,current_user.email) # whether user is org admin or not
    if not is_admin:
        return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not organization admin")
    #formdata = await request.form()
    deletedmember = delete_team_member(team_name, dm.members)
    event_processor("DeleteTeamMember", deletedmember)
    return RedirectResponse(
        url="/api/v1/delete-team-member", status_code=status.HTTP_303_SEE_OTHER
    )


@meetings_router.get("/delete-team-admin", response_class=HTMLResponse)
def delete_admin(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    This route has been built to return a form for removing the ADMIN from a team.
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=delete-team-admin", status_code=status.HTTP_303_SEE_OTHER)
    users_org = crud.get_user_org(current_user.email) # getting user org
    is_admin = crud.check_org_admin(users_org,current_user.email) # whether user is org admin or not
    if not is_admin:
        return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not admin")
    return templates.TemplateResponse("delete_team_admin.html", {"request": request})


# This route requires form validation , which is to be done later.
@meetings_router.post("/delete-team-admin", response_class=HTMLResponse)
async def post_delete_admin(da:DeleteAdmin=Depends(),  current_user: User = Depends(get_current_active_user)):

    """
    This route has been built to save the data of form from delete-team-member
    route in the database.
    
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=delete-team-admin", status_code=status.HTTP_303_SEE_OTHER)
    formdata = da.__dict__
    team_name = formdata['TeamName']
    team_det = teams.get(team_name)
    if team_det['Disabled_yn'] == True:
        return f"{team_name} team is no longer available. Please contact your admin "
    is_admin = crud.check_team_admin(team_name,current_user.email) # whether user is org admin or not
    if not is_admin:
        return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not admin")
    deletedmember = delete_team_admin(formdata)
    event_processor("DeleteTeamAdmin", deletedmember)
    return RedirectResponse(
        url="/api/v1/delete-team-admin", status_code=status.HTTP_303_SEE_OTHER
    )

######################################################################################
################################## PROJECT API'S #####################################
######################################################################################



# @meetings_router.get("/create-project", response_class = HTMLResponse) 
# def create_project_get(request:Request,current_user: User = Depends(get_current_active_user)):
#     """
#     This api just return the form for new project.
#     Parameter
#     ----------
#     request : Request contains of all headers and other details that 
#               are related to user details and the data incoming from
#               the form.
#     current_user: contains the user info who is signed in
#     """
#     if not current_user:
#         return RedirectResponse(url="/api/v1/login/?redirect_url=create-project", status_code=status.HTTP_303_SEE_OTHER)
#     users_org = crud.get_user_org(current_user.email) # getting user org
#     is_admin = crud.check_org_admin(users_org,current_user.email) # whether user is org admin or not
#     if is_admin:
#         return templates.TemplateResponse("create-project.html", {"request": request})
#     else:
#         return HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail="User not organization admin")
    
    

@meetings_router.post("/create-project", response_class = HTMLResponse) 
async def create_project_post(project_data: CreateProject =Depends(),current_user: User = Depends(get_current_active_user)):
    """
    This route stores all the info about Project form returned from
    the above route.
    Parameter
    ----------
    current_user: contains the user info who is signed in
    form data contains....
    @FieldType: field type of project
    @ProjectName : name of project
    @ExpectedEndDate: end date of the project
    @ProjectDescription: description of project
    @StartedOn: start date of project
    @ActiveYN: boolean stating project is active or not
    """
    if not current_user:
        redirect_url = "create-project"
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })


    users_org = crud.get_user_org(current_user.email) # getting user org
    is_admin = crud.check_org_admin(users_org,current_user.email) # whether user is org admin or not
    org_admins = crud.get_org_admins(users_org)
    org_admins.append(current_user.email)
    org_admins = list(set(org_admins))
    print("org admins are...")
    print(org_admins)
    if not is_admin:
        return JSONResponse({
            'status_code': status.HTTP_403_FORBIDDEN,
            'detail':'not authorized to create project'
            })
    project_data_dict = project_data.__dict__
    
    print(project_data)
    project_data_dict["created_by"] = current_user.email
    project_data_dict["Admins"] = org_admins
    if crud.check_teams_status(users_org,project_data_dict['teams']) == False:
        return JSONResponse({
            'status_code': status.HTTP_404_NOT_FOUND,
            'detail':'no such team or team is disabled'
            })

    project_status = create_project(project_data_dict)
    if project_status is None:
        return JSONResponse({
            'status_code': status.HTTP_404_NOT_FOUND,
            'detail':'no workspace present'
            })
    if project_status == False:
        return JSONResponse({
            'status_code': status.HTTP_409_CONFLICT,
            'detail':'project with the given title is already created'
            })

    print(project_data_dict)
    try:
        folder_path = os.path.join(file_dir,"resources")
        folder_path = os.path.join(folder_path,users_org)
        folder_path = os.path.join(folder_path,"projects")

        project_path = os.path.join(folder_path,project_data_dict["project_title"])
        os.mkdir(project_path)
        os.mkdir(os.path.join(project_path,"meetings"))
    except Exception as e:
        print(e)

    crud.notify_user(current_user.email,"New Project Created", f"A new Project named {project_data_dict['title']} is created",f"/api/v1/create_project")
    # return RedirectResponse(
    #     url=f"/api/v1/{project_data_dict['title']}/data", status_code=status.HTTP_303_SEE_OTHER
    # )
    return JSONResponse({
        'status_code':status.HTTP_201_CREATED,
        'url':f"/api/v1/{project_data_dict['title']}/data",
        'detail': 'new project is created successfully.'
        })



@meetings_router.get('/workspace_members')
async def get_workspace_members(current_user: User = Depends(get_current_active_user)):
    if not current_user:
        redirect_url = "create-project"
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })


    users_org = crud.get_user_org(current_user.email) # getting user org
    print("the user org is .....", users_org)
    print(users_org is None)
    print(not users_org)
    print(users_org == '')
    if not users_org:
        return JSONResponse({
            'status_code': status.HTTP_403_FORBIDDEN,
            'detail':'not authorized, please select organization'
            })

    is_admin = crud.check_org_admin(users_org,current_user.email) # whether user is org admin or not

    if is_admin == False:
        return JSONResponse({
            'status_code': status.HTTP_403_FORBIDDEN,
            'detail':'not authorized to create project'
            })

    members = crud.get_workspace_users(users_org)

    return members




@meetings_router.get('/sample_uploader')
async def sample_uploading_html(request:Request):
    content = """<html>
    <body>
    <form action="/api/v1/sample_uploader/" enctype="multipart/form-data" method="post">
    <input name="files" type="file" multiple>
    <input type="submit">
    </form>
    </body>
    </html> """
    #return HTMLResponse(content=content)
    return templates.TemplateResponse("sample_file_uploader.html", {"request": request})

@meetings_router.post('/sample_uploader')
def sample_upload_files(files: List[UploadFile] = File(...)):
    #return {"filenames": [file.filename for file in files]
    dest_path = f"{file_dir}/files/"
    #for file in files:
    #return create_upload_files(files)
    for f in files:
        file_location = dest_path+f.filename
        with open(file_location, "wb+") as file_obj:
            file_obj.write(f.file.read())
    return files




@meetings_router.get('/sample_display/{filename}')
def sample_upload_files(filename: str):
    #return {"filenames": [file.filename for file in files]
    dest_path = f"{file_dir}/files/"
    #file = file[0]
    #field_type = filename.split('.')[-1]
    #if field_type == 'pdf':
    #    media_type = 'application/pdf'
    #elif field_type == 'xls' or field_type == 'xlsx':
    #    try:
    #        media_type= 'application/vnd.ms-xls'
    #    except:
    #        media_type = 'application/vnd.ms-excel'
    #elif field_type == 'csv':
    #    media_type = 'text/csv'
    #elif field_type == 'json':
    #    media_type = 'text/json'
    #elif field_type in ['jpeg', 'JPEG', 'JPG','jpg', 'png', 'PNG']:
    #    media_type = 'image/'+field_type
    #else:
    #    media_type = 'text/plain'
    #
    media_type ='mime'
    return FileResponse(dest_path+filename, media_type=media_type)


@meetings_router.get("/{project_name}/modify_project_admin", response_class = HTMLResponse) 
def modify_project_admin(request:Request,project_name:str,current_user: User = Depends(get_current_active_user)):
    """
    This route returns the form which inputs the new admin name of project
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    """
    if not current_user:
         return RedirectResponse(url = "/api/v1/login/?redirect_url=create-project", status_code=status.HTTP_303_SEE_OTHER)

    
    return templates.TemplateResponse("modify_project_admin.html", {"request": request,"project_name":project_name})

@meetings_router.get("/{project_name}/{member}/delete_project_member", response_class = HTMLResponse) 
def modify_project_admin(request:Request,project_name:str,member:str,current_user: User = Depends(get_current_active_user)):
    """
    This route stores is for deleting the project memver
     Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    member: username of member that is to be deleted from project
    """
    if not current_user:
         return RedirectResponse(url = "/api/v1/login/?redirect_url=create-project", status_code=status.HTTP_303_SEE_OTHER)

    if not crud.isProjectAdmin(current_user.email,project_name):
      return "Not authorized to delete member"

    crud.delete_project_member(project_name,member)

    return RedirectResponse(
        url="/api/v1/"+project_name+"/display-project-members", status_code=status.HTTP_303_SEE_OTHER
    )
    

@meetings_router.post("/{project_name}/modify_project_admin", response_class = HTMLResponse) 
async def create_project_post(project_name:str,ma:ModifyAdmin=Depends(),current_user: User = Depends(get_current_active_user)):
    """
    This route contains crud functions to modify project admin
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    form data contains....
    @admin: username of new project admin
    """
    if not current_user:
         return RedirectResponse(url = "/api/v1/login/?redirect_url=create-project", status_code=status.HTTP_303_SEE_OTHER)

    data = ma.__dict__
    mylogger.info(data)
    crud.modify_project_admin(project_name,data,current_user.email)
    return RedirectResponse(
        url="/api/v1/"+project_name+"/data", status_code=status.HTTP_303_SEE_OTHER
    )

@meetings_router.get("/{project_name}/add-projectmates", response_class = HTMLResponse) 
def add_projectmates(project_name:str,request:Request,current_user: User = Depends(get_current_active_user)):
    """
    This api renders page to add new project mebers to project
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url={project_name}/add-projectmates", status_code=status.HTTP_303_SEE_OTHER)

    if not crud.isProjectAdmin(current_user.email,project_name):
        return "Not authorized to add members"

    return templates.TemplateResponse("add_projectmates.html", {"request": request,"project_name":project_name})
 


@meetings_router.post("/{project_name}/add-projectmates", response_class = HTMLResponse) 
async def add_projectmates_post(project_name:str,request: Request, current_user: User = Depends(get_current_active_user)):
    """
    This api collects from data of project members and add them to project
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    form data contains....
    @ProjectName: name of project
    @ProjectMembers: list of members to be added to project
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url={project_name}/add-projectmates", status_code=status.HTTP_303_SEE_OTHER)


    formdata = await request.form()
    formdata_dict = formdata.__dict__
    formdata_dict['_dict']["ProjectName"] = project_name
    
    crud.add_projectmates(formdata_dict['_dict'])
   
    return RedirectResponse(
        url="/api/v1/{project_name}/add-projectmates", status_code=status.HTTP_303_SEE_OTHER)



@meetings_router.get("/display-projects", response_class = HTMLResponse) 
def display_projects(request:Request, current_user: User = Depends(get_current_active_user)):
    """
    This api display all the projects of an organization
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    """
    if not current_user:
        #return RedirectResponse(url="/api/v1/login?redirect_url=display-projects", status_code=status.HTTP_303_SEE_OTHER)
        redirect_url = "display-projects"
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })

    org_name = crud.get_user_org(current_user.email)
    projects = crud.display_org_projects(org_name)
    print(projects)
    # admin_map = {}
    # for p in projects:
    #     admin_map[p] = crud.isProjectAdmin(current_user.email,p)
    # print(admin_map)
    # return templates.TemplateResponse("display_projects.html", {"request": request,"projects":projects, "adminmap":admin_map})

    return JSONResponse({
        'status_code': status.HTTP_200_OK,
        'data':projects
        })



@meetings_router.get("/{project_name}/display-project-members", response_class = HTMLResponse) 
def display_projectmates(request:Request,project_name:str, current_user: User = Depends(get_current_active_user)):
    """
    This api display all members of project
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    """
    if not current_user:
        #return RedirectResponse(url="/api/v1/login?redirect_url={project_name}/display-project-members", status_code=status.HTTP_303_SEE_OTHER)
        redirect_url = "display-project-members"
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })

    members = crud.display_project_members(project_name)
    is_admin = crud.isProjectAdmin(current_user.email, project_name)
    return JSONResponse({
        'status_code': status.HTTP_200_OK


        })
    
    return templates.TemplateResponse("display_project_members.html", {"request": request,"projectmates":members,"projectname":project_name, "isadmin":is_admin})

@meetings_router.get("/{project_name}/data", response_class = HTMLResponse) 
def display_project_data(request:Request,project_name:str, current_user: User = Depends(get_current_active_user)):
    """
    This api diaplays all info of project
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    """
    if not current_user:
        #return RedirectResponse(url="/api/v1/login?redirect_url={project_name}/data", status_code=status.HTTP_303_SEE_OTHER)
        redirect_url = "{project_name}/data"
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })
    my_org = crud.get_user_org(current_user.email)
    project_details = crud.get_project_data(my_org,project_name)
    print(project_details)
    return JSONResponse({
        'status_code':status.HTTP_200_OK,
        'data':project_details
        })
    
    #return templates.TemplateResponse("display_project_data.html", {"request": request,"project_details":project_details})

@meetings_router.get("/my_projects", response_class = HTMLResponse) 
def my_projects(request:Request, current_user: User = Depends(get_current_active_user)):
    """
    This api returns all the projects of which current user is a member
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    """
    if not current_user:
        #return RedirectResponse(url="/api/v1/login?redirect_url=my_projects", status_code=status.HTTP_303_SEE_OTHER)
        redirect_url = "my-projects"
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })
    my_org = crud.get_user_org(current_user.email)
    org_projects = crud.display_org_projects(my_org)
    my_projects = crud.get_user_projects(current_user.email, org_projects)
    print(my_projects)
    return JSONResponse({
        'status_code':status.HTTP_200_OK,
        'data':my_projects
        })
    #return templates.TemplateResponse("my_projects.html", {"request": request,"my_projects":my_projects})

@meetings_router.get("/{project_name}/delete-project", response_class = HTMLResponse) 
def delete_project_get(project_name:str,request:Request,current_user: User = Depends(get_current_active_user)):
    """
    This api deletes the project by changing status of AcyiveYN to False.
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    """
    if not current_user:
        #return RedirectResponse(url="/api/v1/login?redirect_url={project_name}/delete-project", status_code=status.HTTP_303_SEE_OTHER)
        redirect_url = "{project_name}/delete-project"
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })

    if not crud.isProjectAdmin(current_user.email,project_name):
        return JSONResponse({
            'status_code':status.HTTP_403_FORBIDDEN,
            'detail':'access denied'
            })

    delete_project(project_name)
    crud.notify_user(current_user.email,"Delete Project", f"A Project named {project_name} is deleted",f"/api/v1/{project_name}/delete-project")
    
    return RedirectResponse(url="/api/v1/display-projects", status_code=status.HTTP_303_SEE_OTHER)

@meetings_router.get("/select-project-to-modify", response_class = HTMLResponse) 
def select_modify_project_get(request:Request, current_user: User = Depends(get_current_active_user)):
    """
    This api just return the form for modifying a project.
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=select-project-to-modify", status_code=status.HTTP_303_SEE_OTHER)
    return templates.TemplateResponse("select-project.html", {"request": request})

def retrieve_project(Project_Key):
    """
    This function returns the data of the project's latest instance
    reffered by key = ProjectName

    Parameter
    ---------
    Project_Key - This parameter basically work as key for 
    searching doc the project vertex collection.

    Return
    ------
    Returns the dict of found data.
    """
    if not project.has(Project_Key):
        raise ValueError(
            "key {} doesnot present in {} vertex collection.. ".format(
                Project_Key, project
            )
        )

    

    p_doc = project.get(Project_Key)
    return p_doc['project_data']


'''
@meetings_router.get("/{project_name}/update_project", response_class = HTMLResponse)
async def modify_project_get(request:Request,project_name:str,current_user: User = Depends(get_current_active_user)):
    """
    This api just return the form for modifying a project.
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url={project_name}/update_project", status_code=status.HTTP_303_SEE_OTHER)

    if not crud.isProjectAdmin(current_user.email,project_name):
        return "Not authorized to modify project"

    P_data = retrieve_project(project_name)
    return templates.TemplateResponse("modify-project.html", {
        "request": request,"P_data":P_data,"project_name":project_name
     })
'''


@meetings_router.post("/{project_name}/update_project", response_class = HTMLResponse) 
async def modify_project_post(request:Request,project_name:str,project_data: ProjectItem =Depends(),current_user: User = Depends(get_current_active_user)):
    """
    This route just modifies the data of the ProjectName
    from the form of the above api.
     Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    current_user: contains the user info who is signed in
    project_name: name of project
    form data contains....
    @FieldType: field type of project
    @ProjectName : name of project
    @ExpectedEndDate: end date of the project
    @ProjectDescription: description of project
    @StartedOn: start date of project
    @ActiveYN: boolean stating project is active or not
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url={project_name}/update_project", status_code=status.HTTP_303_SEE_OTHER)

    project_data_dict = project_data.__dict__
    workspace= crud.get_user_org(current_user.email)
    project_key = crud.get_project_key(workspace, project_name)
    if project_key is None:
        crud.notify_user(current_user.email,"Update Project", f"A Project named {project_name} is not updated due to incorrect key/project name",f"/api/v1/{project_name}/data")
        return JSONResponse ({
            'status_code':status.HTTP_404_NOT_FOUND,
            'detail': "no project with the provided key",
            })
    update_status = modify_project(project_data_dict,project_key)

    if update_status is None:
        crud.notify_user(current_user.email,"Update Project", f"A Project named {project_name} is not updated due to incorrect key/project name",f"/api/v1/{project_name}/data")
        return JSONResponse ({
            'status_code':status.HTTP_404_NOT_FOUND,
            'detail': "no project with the provided key",
            })
    elif update_status is False:
        crud.notify_user(current_user.email,"Update Project", f"A Project named {project_name} is not updated due to bad request",f"/api/v1/{project_name}/data")
        return JSONResponse({
            'status_code':status.HTTP_400_BAD_REQUEST,
            'detail':'issue in the request sent'
            })
    else:
        crud.notify_user(current_user.email,"Update Project", f"A Project named {project_name} is updated",f"/api/v1/{project_name}/update_project")
        return JSONResponse({
            'status_code':status.HTTP_201_CREATED,
            'detail':'updated_successfully'
            })




######################################################################################
################################## END OF PROJECT API's #####################################
######################################################################################

#  connecting Project and teams functions

@meetings_router.get("/team-edge-project", response_class = HTMLResponse) 
def team_edge_project_get(request:Request, current_user: User = Depends(get_current_active_user)):
    """
    This api just return the form for creating an edge between a project and team.
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=team-edge-project", status_code=status.HTTP_303_SEE_OTHER)
    
    return templates.TemplateResponse("team-project-edge.html", {"request": request})


@meetings_router.post("/team-edge-project", response_class = HTMLResponse) 
async def team_edge_project_post(request:Request,tep:TeamEdgeProject=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    This route just to create an edge between team and project.
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=team-edge-project", status_code=status.HTTP_303_SEE_OTHER)
    formdata = tep.__dict__
    p_key = formdata['ProjectName'] #+ '_' + formdata['CreatedYear'] +  '_' + formdata['CreatedMonth'] +  '_' + formdata['CreatedDay']
    team_key = formdata['TeamName']

    connect_team_project(p_key,team_key)
    return RedirectResponse(
        url="/api/v1/team-edge-project/", status_code=status.HTTP_303_SEE_OTHER
    )



# User Check list routes and functions


@meetings_router.get("/my-to-do-list", response_class=HTMLResponse)
def show_all_list(request: Request, current_user: User = Depends(get_current_active_user)):

    """
    This route just shows all ToDoList of the user
    with key in DB as username
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=my-to-do-list", status_code=status.HTTP_303_SEE_OTHER)
    email = current_user.email
    retrieved_title_list = get_check_list(email)
    if retrieved_title_list == False:
        ret = ["No such user"]
        return ret[0]

    return templates.TemplateResponse(
        "user_all_to_do_list.html",
        {
            "request": request,
            "ToDoList": retrieved_title_list,
            "username": json.dumps(current_user.email),
            "u1": current_user.email,
        },
    )


@meetings_router.get("/my-to-do-list/addlist", response_class=HTMLResponse)
def add_new_user_list(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    This route is to make a form for a new user check list
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=my-to-do-list/addlist", status_code=status.HTTP_303_SEE_OTHER)
    return templates.TemplateResponse(
        "user_add_new_list.html", {"request": request}
    )


# This route requires form validation , which is to be done late
@meetings_router.post("/my-to-do-list/addlist", response_class=HTMLResponse)
async def add_new_user_list_db(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    This route processes the form posted by the above route.
    And stores the data of user check list to our DB
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=my-to-do-list/addlist", status_code=status.HTTP_303_SEE_OTHER)

    formdata = await request.form()
    item_list = []
    for item in formdata:
        if item != "Title":
            item_list.append(formdata[item])
    add_list_to_record(formdata["Title"], item_list, current_user.email)
    return RedirectResponse(
        url="/api/v1/my-to-do-list", status_code=status.HTTP_303_SEE_OTHER
    )


# This route requires request body validation , which is to be done later.
@meetings_router.post(
    "/my-to-do-list/{title}/addlistitem", response_class=HTMLResponse
)
async def add_new_item(request:Request, title: str, current_user: User = Depends(get_current_active_user)):
    """
    This route is for add a new item in the record specified by the title.
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=my-to-do-list/{title}/addlistitem", status_code=status.HTTP_303_SEE_OTHER)

    d = await request.body()
    d = str(d)
    
    match = re.search(r"([\w])\'\"([\s\S]+)\"\'", d)

    if match:
        data = match.group(2)
        add_item_to_record(title, current_user.email, data)
        return data


# This route requires request body validation , which is to be done later.
@meetings_router.post(
    "/my-to-do-list/{title}/dellist", response_class=HTMLResponse
)
async def del_list( title: str, current_user: User = Depends(get_current_active_user)):
    """
    This route deletes the record as specified by title.
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=my-to-do-list/{title}/dellist", status_code=status.HTTP_303_SEE_OTHER)
    delete_user_record(current_user.email, title)
    return None


# This route requires request body validation , which is to be done later.
@meetings_router.post(
    "/my-to-do-list/{title}/{item}/dellist", response_class=HTMLResponse
)
async def del_list_item( title: str, item: str, current_user: User = Depends(get_current_active_user)):
    """
    This route deletes the item specified as item parameter 
    from the record  specified by title.
    """
    # mylogger.info(username,title,item)
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=my-to-do-list/{title}/{item}/dellist", status_code=status.HTTP_303_SEE_OTHER)
    delete_user_record_item(current_user.email, title, item)
    return None


# task - view_upcoming_meetings


@meetings_router.get("/search_meetings", response_class=HTMLResponse)
async def search_meetings(request: Request, current_user: User = Depends(get_current_active_user)) -> "Jinja2Templates":
    """
        ignore this api
    Use to fetch meetings data for a particular user

    Parameter
    ----------
    request : Request

    Return
    -------
    Jinja2Templates response
    """

    return templates.TemplateResponse("search_meetings.html", {"request": request})


@meetings_router.post("/search_meetings_result", response_class=HTMLResponse)
async def search_meetings_result(request: Request,username:Username=Depends(), current_user: User = Depends(get_current_active_user)) -> "Jinja2Templates":
    """
    ignore this api
    Use to send meetings data of search results
    scope1: limited to only team name
    scope2: limited to the teams of only that admin

    Default setting : send upcoming meetings

    Parameter
    ----------
    request : Request

    Return
    -------
    Jinaj2Templates response
    """
    
    form_data = username.__dict__
    username = form_data["username"]

    today = dt.date.today()
    tomorrow = today + dt.timedelta(days=1)

    data = crud.get_upcoming_meetings(username)

    return templates.TemplateResponse(
        "upcoming_meetings.html",
        {
            "request": request,
            "data": data,
            "username": username,
            "today": str(today),
            "tomorrow": str(tomorrow),
        },
    )


@meetings_router.get("/upcoming_meetings", response_class=HTMLResponse)
async def user_meetings(request: Request, current_user: User = Depends(get_current_active_user)) -> "Jinja2Templates":
    """
    Use to get all scheduled meetings-info

    Parameter
    ----------
    request : Request

    Return
    -------
    Jinja2Templates response
    """
    if current_user==None:
        redirect_url="upcoming_meetings"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    today = date.today()
    tomorrow = today + timedelta(days=1)

    data = crud.get_upcoming_meetings(current_user.email)
    mylogger.info(data)
    return templates.TemplateResponse(
        "upcoming_meetings.html",
        {
            "request": request,
            "data": data,
            "username": current_user.email,
            "today": str(today),
            "tomorrow": str(tomorrow),
        },
    )


@meetings_router.get(
    "/meeting_details/{meeting_key}", response_class=HTMLResponse
)
async def meeting_detail(
    meeting_key: str, request: Request, current_user: User = Depends(get_current_active_user)
) -> "Jinja2Templates":
    """
    Use to get detailed info of a particular meeting

    Parameter
    ----------
    username : str
    request : Request
    meeting_key : str

    Return
    -------
    Jinja2Templates response
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=meeting_details/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)
    username = current_user.email
    meeting, action_task = crud.get_meeting_details(username, meeting_key)
    subagendas = crud.get_meeting_agendas(meeting_key)
    mylogger.info(subagendas)
    return templates.TemplateResponse(
        "details.html",
        {
            "request": request,
            "meeting": meeting,
            "action_task": action_task,
            "username": username,
            "meeting_key": meeting_key,
            "subagendas": subagendas,
        },
    )


#   Used entirely to push data in db
@meetings_router.get("/create_meeting", response_class=HTMLResponse)
async def create_meetings(request: Request, current_user: User = Depends(get_current_active_user)) -> "Jinja2Templates":
    """
    Main page to enter data to create meetings

    Parameter
    ----------
    request : Request

    Return
    -------
    Jinja2Templates respone
    """
    if not current_user:
       return RedirectResponse(url="/api/v1/login/?redirect_url=create_meeting", status_code=status.HTTP_303_SEE_OTHER)
    if not crud.get_user_org(current_user.email):
        return "You are not linked to any organization"
    usertype = crud.get_user_third_party(current_user.email)
    preferedcalendar = ""
    if usertype=="custom":
        preferedcalendar = crud.get_add_user_calendar(current_user.email)["cal_type"]
    return templates.TemplateResponse("create_meetings.html", {"request": request, "usertype":usertype, "pcalendar":preferedcalendar})


from app.routes.v1.security import COOKIE_AUTHORIZATION_NAME

def create_upload_files(files: List[UploadFile] = File(...)):#, file_size: int = Depends(valid_content_length)):
    # return {"filenames": {file.filename:[file.filename,Path(file.filename).suffix, tempfile.mkstemp(prefix='parser_', suffix=extension)[1], file.content_type] for file in files}}
    """
    parameters:
    file_size: checking the size for all files..
    files: multiple files
    @return:
    if fails due to overall upload file size increases, it throws the exception with the type of file it has issue.
    if succeeds, copies the file to the destination folder.
    """
    #mylogger.info("Recived Files ------------------\n",files)
    file_details = {}
    file_size = 1000000000000000
    
    #temp: IO = NamedTemporaryFile(delete=False)
    dest_path = f"{file_dir}/files/"
    real_file_size = 0
    size_overflow = False
    overflown_format = ''
    for file in files:
        extension = Path(file.filename).suffix
        mylogger.info(file.filename)
        for chunk in file.file:
            real_file_size += len(chunk)
            if real_file_size >= file_size:
                size_overflow = True
                overflown_format = extension
                break
                

    if size_overflow == True:
        raise HTTPException(
                    status_code=status.HTTP_413_REQUEST_ENTITY_TOO_LARGE, detail="{} is Too large".format(overflown_format)
                )
    else:
        for file in files:
            #temp: IO = NamedTemporaryFile(delete=False)
            extension = Path(file.filename).suffix
            _, path = tempfile.mkstemp(prefix='parser_', suffix=extension)
            file_details[file.filename] = [extension, path, file.content_type]
            with open(os.path.join(dest_path, file.filename), 'wb+') as folder:
                shutil.copyfileobj(file.file, folder)

    return file_details

@meetings_router.post("/generate_meeting_link/{org_team_project}")
async def generate_meeting_link(request:Request,org_team_project: str,gml:GenerateMeetingLink=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    @author : Priyav Kaneria
    08-02-2021
    """
    data = gml.__dict__
    print(data)
    
    start_time = datetime.strptime(data['date']+' '+data['start_time']+':00', "%Y-%m-%d %H:%M:%S")
    start_time = start_time.replace(tzinfo=tz.tzlocal())
    end_time = datetime.strptime(data['date']+' '+data['end_time']+':00', "%Y-%m-%d %H:%M:%S")
    end_time = end_time.replace(tzinfo=tz.tzlocal())
    summary = data['meeting_subject']
    description = data['agenda']
    location = 'Hyderabad'#data['link']
    meetingProvider = data['meetingprovider']
    organization = crud.get_user_org(current_user.email)

    if org_team_project=="team":

        members = crud.get_team_members(organization, team_name)
    elif org_team_project=="project":
        members = crud.get_project_members(data['project_name'])
    else:
        members = list(crud.get_users_of_org(organization).keys())

    ################################# PREFERED CALENDAR ######################################
    usertype = crud.get_user_third_party(current_user.email)
    preferedcalendar = ""
    if usertype == "custom":
        preferedcalendar = crud.get_add_user_calendar(current_user.email)["cal_type"]

    if preferedcalendar == "microsoft":
        invitees = []
        for invitee in members:
            myuser = user.get(invitee)
            inviteetype = crud.get_user_third_party(invitee)
            email = invitee
            if inviteetype=="custom":
                username = myuser["username"]
            else:
                username = myuser['full_name']
            invitees.append({'emailAddress':{"address":email,"name": username},'type':"required"})
    
    if meetingProvider=="teamsForBusiness":
        ############################ ADD EVENT IN OUTLOOK CALENDAR ###############################
        if usertype == "microsoft" or preferedcalendar == "microsoft":
            if usertype == "microsoft":
                access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
            else:
                access_token = crud.get_add_user_calendar(current_user.email)['access_token']
            mylogger.info(tzlocal.get_localzone().zone)
            body = {
                    "subject": summary,
                    "body": {
                        "contentType": "HTML",
                        "content": description
                    },
                    "start": {
                        "dateTime": f"{str(start_time.date())}T{str(start_time.time())}",
                        "timeZone": str(tzlocal.get_localzone().zone)
                    },
                    "end": {
                        "dateTime": f"{str(end_time.date())}T{str(end_time.time())}",
                        "timeZone": str(tzlocal.get_localzone().zone)
                    },
                    "location":{
                        "displayName":location
                    },
                    "attendees": invitees,
                    "isOnlineMeeting": True,
                    "onlineMeetingProvider": meetingProvider         
                }

            r2 = make_api_call('POST', "https://graph.microsoft.com/v1.0/me/events", access_token, body=body)
            mylogger.info(r2)
            if("error" in r2) :
                raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"Unable to validate social login, error is {r2}")
            else:
                mylogger.info("Event added to microsoft calender successfully")
                if "onlineMeeting" in r2:
                    joiningURL = r2["onlineMeeting"]["joinUrl"]
                    mylogger.info(joiningURL)
                    return JSONResponse({"link":joiningURL})
    
    elif meetingProvider == "hangoutsMeet":
        ############################ ADD EVENT IN GOOGLE CALENDAR ###############################
        if usertype == "google" or preferedcalendar == "google":
            #create google_cal_json will convert the form_data into the
            #format that googel calendar understands.....
            #event = create_google_cal_json(summary, location, description, start_time, end_time, invitees, timezone = 'Asia/Kolkata')
            event = create_google_cal_json(summary, location, description, start_time, end_time, members,meetingProvider, timezone = 'Asia/Kolkata')
            mylogger.info(str(event))
            # client_websocket(event)
            mylogger.info("event in google format........")
            mylogger.info(event)
            service = get_service(data["hostname"],"calendar")
            result = service.calendarList().list().execute()
            calendar_id = result['items'][0]['id']
            mylogger.info(result)
            mylogger.info(calendar_id)
            #event = json.dumps(event, default=datetime_handler)
            #event = json.loads(event)
            mylogger.info(event)
            mylogger.info(type(event))
            final_result = service.events().insert(calendarId= 'primary', conferenceDataVersion=1, body=event).execute()
            mylogger.info(final_result)
            gmeetlink = final_result["conferenceData"]["entryPoints"][0]["uri"]
            mylogger.info("\n\n\nGOOGLE MEET JOINING LINK",gmeetlink)
            return JSONResponse({"link":gmeetlink})

    elif meetingProvider == "Zoom":
        #############################################################################
        ###################### CODE FOR MAKING ZOOM MEETING #########################
        #############################################################################
        return JSONResponse({"link":"link to zoom meet"})


#   Used entirely to push data in db
@meetings_router.post("/create_meeting")
async def create_meeting(request: Request, files: List[UploadFile] = File(...), cmp:CreateMeetingPost=Depends(), current_user: User = Depends(get_current_active_user)) -> "str response":
    """
    Use to create meeting
    @author: KrishNa
    Created on Tue Sep 22 15:20:22 2020

    @co-author: Priyav
    Modified many many times
    
    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    form data contains....
    @host: who is hosting the meeting
    @duration : total time of the meeting
    @date: date of the meeting
    @st_time: starting time of the meeting
    @end_time: ending time of the meeting
    @attendees: list of attendees of the meeting

    Return
    -------
    HTTP Status response: if meeting is create successfully, 
    it returns 200 and if fails, it throws 401
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=create_meeting", status_code=status.HTTP_303_SEE_OTHER)
    #form = await request.form()
    #mylogger.info(form)
    #data = jsonable_encoder(form)
    data= cmp.__dict__
    mylogger.info(files)
    file_data = create_upload_files(files)
    mylogger.info("File data : ",file_data)
    username = current_user.email
    organization = crud.get_user_org(username)
    mylogger.info(organization)
    user_with_org = crud.get_users_of_org(organization)
    email_ids = list(user_with_org.keys())
    org_members = email_ids
    mylogger.info("Org members",org_members)

    start_time = datetime.strptime(data['date']+' '+data['start_time']+':00', "%Y-%m-%d %H:%M:%S")
    end_time = datetime.strptime(data['date']+' '+data['end_time']+':00', "%Y-%m-%d %H:%M:%S")

    commonFreeSlots = get_common_slots(org_members, data['date'])
    mylogger.info("The common free slots of the organization members..................\n",commonFreeSlots)
    start_time = start_time.replace(tzinfo=tz.tzlocal())
    #email_ids.remove('realpkk')

    mylogger.info("the meeting data above...........")
    if data["linktext"]=="":
        del data["linktext"]
    else:
        data["link"] = data["linktext"]
        del data["linktext"]
    db_insertion_status,meeting_id = crud.create_meeting(data, email_ids, "ORGANIZATION_MEETING", organization)
    mylogger.info(db_insertion_status)
    summary = data['meeting_subject']
    # if db_insertion_status == True:
    #     # invitees = [user.get(invitee)['email'] for invitee in data['invitees'].split(',')]
    #     mylogger.info(type(data['date']))
    #     mylogger.info(type(data['start_time']))
    #     start_time = datetime.strptime(data['date']+' '+data['start_time']+':00', "%Y-%m-%d %H:%M:%S")
    #     start_time = start_time.replace(tzinfo=tz.tzlocal())
    #     end_time = datetime.strptime(data['date']+' '+data['end_time']+':00', "%Y-%m-%d %H:%M:%S")
    #     end_time = end_time.replace(tzinfo=tz.tzlocal())
    #     summary = data['meeting_subject']
    #     description = data['agenda']
    #     location = 'Hyderabad'#data['link']
    #     invitees = []
    #     meetingProvider = data['meetingprovider']

    #     ################################# PREFERED CALENDAR ######################################
    #     usertype = crud.get_user_third_party(current_user.email)
    #     preferedcalendar = ""
    #     if usertype == "custom":
    #         preferedcalendar = crud.get_add_user_calendar(current_user.email)["cal_type"]
    #     for invitee in org_members:
    #         myuser = user.get(invitee)
    #         inviteetype = crud.get_user_third_party(invitee)
    #         email = invitee
    #         if inviteetype=="custom":
    #             username = myuser["username"]
    #         else:
    #             username = myuser['full_name']
    #         if not preferedcalendar == "google":
    #            invitees.append({'emailAddress':{"address":email,"name": username},'type':"required"})

    #     mylogger.info(start_time)
    #     mylogger.info(type(start_time))
    #     mylogger.info(end_time)

    #     ############################ ADD EVENT IN GOOGLE CALENDAR ###############################

    #     if usertype == "google" or preferedcalendar == "google":
    #         #create google_cal_json will convert the form_data into the
    #         #format that googel calendar understands.....
    #         #event = create_google_cal_json(summary, location, description, start_time, end_time, invitees, timezone = 'Asia/Kolkata')
    #         event = create_google_cal_json(summary, location, description, start_time, end_time, email_ids,meetingProvider, timezone = 'Asia/Kolkata')
    #         mylogger.info(str(event))
    #         # client_websocket(event)
    #         mylogger.info("event in google format........")
    #         mylogger.info(event)
    #         service = get_service(data["hostname"],"calendar")
    #         result = service.calendarList().list().execute()
    #         calendar_id = result['items'][0]['id']
    #         mylogger.info(result)
    #         mylogger.info(calendar_id)
    #         #event = json.dumps(event, default=datetime_handler)
    #         #event = json.loads(event)
    #         mylogger.info(event)
    #         mylogger.info(type(event))
    #         final_result = service.events().insert(calendarId= 'primary', conferenceDataVersion=1, body=event).execute()
    #         mylogger.info(final_result)
    #         gmeetlink = final_result["conferenceData"]["entryPoints"][0]["uri"]
    #         mylogger.info("\n\n\nGOOGLE MEET JOINING LINK",gmeetlink)
        
    #     ############################ ADD EVENT IN OUTLOOK CALENDAR ###############################

    #     if usertype == "microsoft" or preferedcalendar == "microsoft":
    #         if usertype == "microsoft":
    #             access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
    #         else:
    #             access_token = crud.get_add_user_calendar(current_user.email)['access_token']
    #         mylogger.info(tzlocal.get_localzone().zone)
    #         body = {
    #                 "subject": summary,
    #                 "body": {
    #                     "contentType": "HTML",
    #                     "content": description
    #                 },
    #                 "start": {
    #                     "dateTime": f"{str(start_time.date())}T{str(start_time.time())}",
    #                     "timeZone": str(tzlocal.get_localzone().zone)
    #                 },
    #                 "end": {
    #                     "dateTime": f"{str(end_time.date())}T{str(end_time.time())}",
    #                     "timeZone": str(tzlocal.get_localzone().zone)
    #                 },
    #                 "location":{
    #                     "displayName":location
    #                 },
    #                 "attendees": invitees,
    #                 "isOnlineMeeting": True,
    #                 "onlineMeetingProvider": meetingProvider         
    #             }

    #         r2 = make_api_call('POST', "https://graph.microsoft.com/v1.0/me/events", access_token, body=body)
    #         mylogger.info(r2)
    #         if "onlineMeeting" in r2:
    #             mylogger.info(r2["onlineMeeting"]["joinUrl"])

    #         if("error" in r2) :
    #             raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"Unable to validate social login, error is {r2}")
    #         else:
    #             mylogger.info("Event added to microsoft calender successfully")

        ################################## RABBIT MQ ######################################

        #connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        
        #channel = connection.channel()

        #channel.exchange_declare(exchange='topic_logs', exchange_type='topic')

        #routing_key = "NewMeeting.{}.{}.{}".format(data['hostname'],summary.replace(" ",""),str(start_time).replace(" ","-"))
        #message = f"Meeting created by {data['hostname']} for {summary} on {str(start_time)}"
        #channel.basic_publish(
        #    exchange='topic_logs', routing_key=routing_key, body=message)
        #mylogger.info(" [x] Sent %r:%r" % (routing_key, message))
        #connection.close()

        ################################## NOTIFICATIONS ####################################

    for invitee in org_members:
        crud.notify_user(invitee,"New organization meeting scheduled", f"New organization meeting created for {summary} at {start_time}",f"/api/v1/meeting_details/{meeting_id}", meeting_id)

    return "Meeting created Successfully"

@meetings_router.get("/update_meeting/{meeting_key}", response_class=HTMLResponse)
async def update_meeting(request: Request, meeting_key: str, current_user: User = Depends(get_current_active_user)):
    """
    Update meeting for custom login users

    Parameter
    ---------
    request : Request
    meeting_key : str
        meeting key of meeting to be updated

    Return
    -------
    Jinja2Templates Response
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=update_meeting/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)    
    data = meetings.get(meeting_key)
    return templates.TemplateResponse("update_meeting.html",{"request": request, "data":data})

@meetings_router.post("/update_meeting/{meeting_key}")
async def update_meetings(request: Request, meeting_key: str,um:UpdateMeeting=Depends(), current_user: User = Depends(get_current_active_user)):
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=update_meeting/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)    
    data = um.__dict__
    mylogger.info(data)
    meeting_data = meetings.get(meeting_key)
    mylogger.info(meeting_data)

    crud.update_meeting(meeting_key,data)
    for invitee in meeting_data["invitees"]:
        crud.notify_user(invitee,f"Meeting details of {data['meeting_subject']}",f"Meeting details of {data['meeting_subject']}",f"/api/v1/meeting_details/{meeting_key}",meeting_key)
    return RedirectResponse(url = f"/api/v1/meeting_details/{meeting_key}",status_code=status.HTTP_303_SEE_OTHER)


@meetings_router.get("/{project_name}/create_project_meeting", response_class=HTMLResponse)
async def create_project_meetings(request: Request, project_name:str,current_user: User = Depends(get_current_active_user)) -> "Jinja2Templates":
    """
    Main page to enter data to create meetings

    Parameter
    ----------
    request : Request

    Return
    -------
    Jinja2Templates respone
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=/{project_name}/create_project_meeting", status_code=status.HTTP_303_SEE_OTHER)
    project_det = project.get(project_name)
    print(project_det)
    if project_det['project_data']['active_yn'] == 'No':
        return "project is no longer active"

    if not crud.get_user_org(current_user.email):
        return "You are not linked to any organization"

    if not crud.isProjectAdmin(current_user.email,project_name):
        return "Not authorized to create meeting"

    return templates.TemplateResponse("create_meetings.html", {"request": request,"project_name":project_name})


@meetings_router.post("/{project_name}/create_project_meeting")
async def create_project_meeting(request: Request, project_name:str, files: List[UploadFile] = File(...),current_user: User = Depends(get_current_active_user)) -> "str response":
    """
    Use to create meeting for projects
    @author: Sejal
    Created on  Dec 16

    Parameter
    ----------
    request : Request contains of all headers and other details that 
              are related to user details and the data incoming from
              the form.
    form data contains....
    @host: who is hosting the meeting
    @duration : total time of the meeting
    @date: date of the meeting
    @st_time: starting time of the meeting
    @end_time: ending time of the meeting
    @attendees: list of attendees of the meeting

    Return
    -------
    HTTP Status response: if meeting is create successfully, 
    it returns 200 and if fails, it throws 401
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url={project_name}/create_project_meeting", status_code=status.HTTP_303_SEE_OTHER)
    project_det = project.get(project_name)
    print(project_det)
    if project_det['project_data']['active_yn'] == 'No':
        return "project is no longer active"

    form = await request.form()
    data = jsonable_encoder(form)
    print(data)
    print(data['start_time'])
    print(data['end_time'])
    #mylogger.info(data)
    #mylogger.info(files)
    # file_data = create_upload_files(files)
    #mylogger.info("File data : ",file_data)
    #mylogger.info("the project meeting data above...........")
    if data["linktext"]=="":
        del data["linktext"]
    else:
        data["link"] = data["linktext"]
        del data["linktext"]
    project_members = crud.get_project_members(project_name)
    #mylogger.info(project_members)
    #mylogger.info(data)
    #mylogger.info(data['hostname'])
    #project_members.remove(data['hostname'])
    #mylogger.info(project_members)
    
    start_time = datetime.strptime(data['date']+' '+data['start_time']+':00', "%Y-%m-%d %H:%M:%S")
    end_time = datetime.strptime(data['date']+' '+data['end_time']+':00', "%Y-%m-%d %H:%M:%S")

    #if [data['start_time'], data['end_time']] in crud.get_busy_slots()

    commonFreeSlots = get_common_slots(project_members, data['date'])
    #mylogger.info("The common free slots of the organization members..................\n",commonFreeSlots)
    start_time = start_time.replace(tzinfo=tz.tzlocal())
    #project_members.remove(data['hostname']) 
    db_insertion_status,meeting_id = crud.create_meeting(data,project_members, "PROJECT_MEETING", project_name)
    #mylogger.info(db_insertion_status, meeting_id)
    if db_insertion_status == True:
        for member in project_members:
            busy_slot = {'username':member, 'meeting_date':data['date'], 'start_time': data['start_time'], 'end_time':data['end_time']}
            crud.insert_busy_hours(busy_slot)
    #mylogger.info(db_insertion_status)
    summary = data['meeting_subject']

    ################################## RABBIT MQ ######################################

    #connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    #channel = connection.channel()

    #channel.exchange_declare(exchange='topic_logs', exchange_type='topic')

    #routing_key = "NewMeeting.{}.{}.{}".format(data['hostname'],summary.replace(" ",""),str(start_time).replace(" ","-"))
    #message = f"Meeting created by {data['hostname']} for {summary} on {str(start_time)}"
    #channel.basic_publish(
        #exchange='topic_logs', routing_key=routing_key, body=message)
    #mylogger.info(" [x] Sent %r:%r" % (routing_key, message))
    #connection.close()

    ################################## NOTIFICATIONS ####################################

    for invitee in project_members:
        crud.notify_user(invitee,"New project meeting scheduled", f"New project meeting created for {summary} at {start_time}",f"/api/v1/{project_name}/create_project_meeting", meeting_id)


    return "Meeting created Successfully"


# meeting_review
@meetings_router.get(
    "/meeting_review/{meeting_key}", response_class=HTMLResponse
)
async def meeting_review(
    request: Request, meeting_key: str,current_user: User = Depends(get_current_active_user)
) -> "Jinja2Templates":
    """
    Use to render meeting review form template

    Parameter
    ----------
    request : Request
    username : str
                user who wants to submit review form
    meeting_key : str
                    meeting key whose review has to be submited
    
    Return
    -------
    Jinja2Templates Response
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=meeting_review/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse(
        "meeting_review_form.html",
        {"request": request, "username": current_user.email, "meeting_key": meeting_key},
    )


@meetings_router.post("/meeting_review_submission/{meeting_key}")
async def meeting_review_submission(
    meeting_key: str,mr:MeetingReview=Depends(),current_user: User = Depends(get_current_active_user)
) -> str:
    """
    Use to submit meeting review form

    Parameter
    ----------
    username : str
                user who wants to submit review form
    meeting_key : str
                    meeting key whose review has to be submited
    Return
    -------
    str response
    """ 
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=meeting_review_submission/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)

    form = mr.__dict__
    mylogger.info(form.get('rating'))
    #body = await request.body()
    #mylogger.info(body)

    crud.meeting_review(form, current_user.email, meeting_key)

    return "Your submission is successful"


# Pending Meetings


@meetings_router.get("/pending_meetings", response_class=HTMLResponse)
async def user_meetings(request: Request, current_user: User = Depends(get_current_active_user)) -> "Jinja2Templates":
    """
    Use to get all pending meetings-info

    Parameter
    ----------
    request : Request

    Return
    -------
    Jinja2Templates response
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=pending_meetings", status_code=status.HTTP_303_SEE_OTHER)

    today = date.today()
    tomorrow = today + timedelta(days=1)

    data = crud.get_pending_meetings(current_user.email)

    return templates.TemplateResponse(
        "pending_meetings.html",
        {
            "request": request,
            "data": data,
            "today": str(today),
            "tomorrow": str(tomorrow),
        },
    )


@meetings_router.get(
    "/accept_pending_meetings/{meeting_key}", response_class=HTMLResponse
)
async def accept_pending_meetings(
    request: Request, meeting_key: str,current_user: User = Depends(get_current_active_user)
) -> "Jinja2Templates":
    """
    Use to accept pending meetings

    Parameter
    ----------
    request : Request
    
    username : str
            user who is accepting the pending meeting request

    meeting_key : str
            meeting which user is going to accept

    Return
    -------
    Jinja2Templates response
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=accept_pending_meetings/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)

    crud.accept_pending_meetings(current_user.email, meeting_key)
    mylogger.info("done")

    return RedirectResponse(
        url=f"/api/v1/pending_meetings",
        status_code=status.HTTP_303_SEE_OTHER,
    )


@meetings_router.get(
    "/reject_pending_meetings/{meeting_key}", response_class=HTMLResponse
)
async def reject_pending_meetings(
    request: Request,  meeting_key: str,current_user: User = Depends(get_current_active_user)
) -> "Jinja2Templates":
    """
    Use to get reject pending meetings request

    Parameter
    ----------
    request : Request

    username : str
            user who is rejecting the pending meeting request

    meeting_key : str
            meeting which user is going to reject

    Return
    -------
    Jinja2Templates response
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=accept_pending_meetings/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)

    crud.reject_pending_meetings(current_user.email, meeting_key)

    return RedirectResponse(
        url=f"/api/v1/pending_meetings",
        status_code=status.HTTP_303_SEE_OTHER,
    )


"""
File Management - Upload Download
"""


# Make file folder out of the project directory
file_folder = os.path.dirname(
    os.path.dirname(
        os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
    )
)


upload_folder = os.path.join(file_folder, "All Files")


async def valid_content_length(content_length: int = Header(..., lt=5_000_000)):
    """
    Check the size of the file to be less than 5MB thoough file header.
    """
    return content_length


@meetings_router.get("/manage_files/", response_class=HTMLResponse)
async def display_user_profile(request: Request,current_user: User = Depends(get_current_active_user)):
    """
    End-point to return the file management page.
    Also rendres a list containing all files in the assigned folder - 'All Files'  
    """

    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=manage_files/", status_code=status.HTTP_303_SEE_OTHER)

    Files = []

    for root, dirs, files in os.walk(upload_folder):
        for filename in files:
            Files.append(filename)

    return templates.TemplateResponse(
        "file_management.html", {"request": request, "Files": Files,},
    )


@meetings_router.get("/download_file/")
def download_file(filename: str,current_user: User = Depends(get_current_active_user)):
    """
    End-point to download files.

    Parameters
    ----------
    filename : str
        This is the filename which is sent in form from templates.
        Filename should be a valid of the files present in the 
        'All Files' folder.

    Returns
    -------
        A FileReponse of the selected filename.
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=download_file/", status_code=status.HTTP_303_SEE_OTHER)

    return FileResponse(f"{upload_folder}/{filename}")

    """
    Zip response - Just a trial

    # content = b"<file content>" * 1000
    # with open("/tmp/stuff", "wb") as tozip:
    #     tozip.write(content)
    # with ZipFile('example.zip', 'w') as myzip:
    #     myzip.write("/tmp/stuff")
    # response = FileResponse(path='example.zip', filename="example.zip")
    # return response

    """


"""
End of File Management
"""


@meetings_router.get("/end_meeting/{meeting_key}")
async def end_meeting( meeting_key: str,current_user: User = Depends(get_current_active_user)):
    """
    function to end the meeting
    
    Parameter
    ----------
    username : str
            username of the user who is ending the meeting
    meeting_key : str
            meeting_key of the meeting which needs to
            be ended
    
    Return
    -------
    Jinja2Templates response
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=end_meeting/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)

    crud.end_meeting(meeting_key)

    return RedirectResponse(
        url=f"/api/v1/meeting_details/{meeting_key}",
        status_code=status.HTTP_303_SEE_OTHER,
    )


@meetings_router.post("/insert_active_slot")
async def add_active_hours(request: ActiveHours,current_user: User = Depends(get_current_active_user)):
    mylogger.info(request)
    # request.start_time = datetime.strptime(request.start_time,'%H:%M').time()
    # request.end_time = datetime.strptime(request.end_time,'%H:%M').time()
    mylogger.info(request)
    mylogger.info(dict(request))

    crud.insert_active_hours(dict(request))

    # mylogger.info(username)


@meetings_router.post("/insert_busy_slot")
async def add_busy_hours(request: BusyHours,current_user: User = Depends(get_current_active_user)):
    mylogger.info(request)
    # request.start_time = datetime.strptime(request.start_time,'%H:%M').time()
    # request.end_time = datetime.strptime(request.end_time,'%H:%M').time()
    mylogger.info(request)
    mylogger.info(dict(request))

    crud.insert_busy_hours(dict(request))

    # mylogger.info(username)


@meetings_router.get("/add_meeting_attendees", response_class=HTMLResponse)
async def add_new_member(request: Request,current_user: User = Depends(get_current_active_user)):

    """
    This route has been built to return a form for adding a member to team.
    """

    return templates.TemplateResponse(
        "select_team_members_for_meeting.html", {"request": request}
    )


# def get_active_slots(users):


def get_max_st_and_min_end_time(active_slots_list):
    """
    parameters:
    @active_slots_list: list
    contains curated maximum start time and min end time
    extracted by analysing the active time slots of the members
    attending the meeting.
    Every user contains a daily activity schedule from starting time
    to ending time like office timings.
    we will get max_starting_free_time for all the start_times 
    of the users active hours.
    In a similar way, we will get max_ending_free_time from all the end_times
    of the users active hours.

    return: list contains max_start_time and min_end_time
    """
    #created dummy max_st_time and min_end_time for comparison
    max_st_time, min_end_time = datetime.strptime('00:00', '%H:%M').time(), datetime.strptime('23:59', '%H:%M').time()
    for slots in active_slots_list:
        #mylogger.info(slots)
        #mylogger.info(slot)
        if slots[0] > max_st_time:
            max_st_time = slots[0]
        
        if slots[1] < min_end_time:
            min_end_time = slots[1]
    return [max_st_time, min_end_time]


#def get_active_slots(users):
def get_common_free_slots(busy_slots_list, active_slots_list):
    """
    parameters:
    @busy_slots_list: list
    contains busy_slots of all the users for a selected day or
    in a date range(yet to be written) in a single list
    @active_slots_list: list
    please check "get_max_st_and_min_end_time" function for description.
    """
    mylogger.info("mylogger.infoing the slots list.....")
    mylogger.info("busy_slots_list")
    mylogger.info(busy_slots_list)
    mylogger.info("active slots list")
    mylogger.info(active_slots_list)
    #sorting the busy_slots in a reverse order based on st_time of the busy_slots
    busy_slots_list =[[datetime.strptime(meeting[0],'%H:%M').time(), datetime.strptime(meeting[1],'%H:%M').time()] for meeting in sorted(busy_slots_list, key=lambda x:x[0], reverse=True)]
    mylogger.info(busy_slots_list)
    mylogger.info(active_slots_list)
    # mylogger.info("get_common_free_slots............")
    # for slot in busy_slots_list:
    #     mylogger.info(slot)
    
    common_busy_slots = []
    common_free_slots = []
    
    while busy_slots_list:
        #deletes the last busy slot.
        meeting_time = busy_slots_list.pop()
        mylogger.info(meeting_time)
        mylogger.info(max(meeting_time[0], meeting_time[1]))
        mylogger.info((datetime.combine(date.today(), meeting_time[1]) - datetime.combine(date.today(), meeting_time[0])).seconds//60)
        #mylogger.info(meeting_time[1]-meeting_time[0])
        #checks if common_busy_slots is empty or not and
        # if not empty, it checks for meeting_time's starting time and 
        #ending time of last element in the common_busy_slots list,
        # if the condition satisfies... we will replace as mentioned
        #in the condition.
        #else if any of the condition fails, it will just append the popped
        #item to the common_busy_slots list
        if (meeting_time[0] >= active_slots_list[0] and meeting_time[0] <= active_slots_list[1]) and (meeting_time[1] >= active_slots_list[0] and meeting_time[1] <= active_slots_list[1]):
            if common_busy_slots and common_busy_slots[-1][1] >= meeting_time[0]:
                common_busy_slots[-1][1] = max(meeting_time[1], common_busy_slots[-1][1])
            
            else:
                common_busy_slots.append(meeting_time)
    
    # mylogger.info("==================================================")
    # mylogger.info(common_busy_slots)
    
    #mylogger.info("==================================================")
    #to get the starting times..
    #we will be adding start_time of active_slots_list and starting time of
    #the first common_busy_slots element
    #then, we will extracting the time gaps between the busy_slots in the for loop.
    #finally, we will be appending the end_time of the last busy_slot and active_slot's end time
    #to get the final free slot if any.
    if active_slots_list[0] != common_busy_slots[0][0]:
        common_free_slots.append([active_slots_list[0], common_busy_slots[0][0]])
    for k in range(len(common_busy_slots)-1):
        common_free_slots.append([common_busy_slots[k][1], common_busy_slots[k+1][0]])
    if active_slots_list[1] != common_busy_slots[-1][1]:
        common_free_slots.append([common_busy_slots[-1][1], active_slots_list[1]])
    #mylogger.info("mylogger.infoing common free slots............")
    #mylogger.info(common_free_slots)
    print(common_free_slots)
    #common_free_slots = json.dumps(common_free_slots)
    return common_free_slots

def divide_common_slots(common_free_slots, interval):
    time_interval = datetime.datetime.timedelta(minutes=interval)
    divided_common_slots = []
    for free_slot in common_free_slots:
        start_time = free_slot[0]
        end_time = free_slot[1]
        while(start_time+time_interval<end_time):
            divided_common_slots.append(start_time,start_time+time_interval)
            start_time = start_time+time_interval
    return divided_common_slots

def get_common_slots(members: list, from_:str = "", to_:str = "", days_:str = "", duration_:str = ""):

    """
    This route has been built to save the data of form from add-team-member
    route in the database.
    
    """
    dates_window = []
    #mylogger.info(members)
    #mylogger.info(len(days_), len(from_), len(to_))
    if len(days_) == 0 and len(from_) == 0 and len(to_) == 0:
        days = 1
        #mylogger.info(days)
        dates_window = [dt.date.today()]
    elif len(days_)>0 and len(from_) == 0 and len(to_) == 0:
        from_ = dt.date.today()
        #mylogger.info(from_)
        #mylogger.info(from_.date())

        #from_ = datetime.strptime(from_, '%Y')

        #end_date = curr_date+timedelta(days=days)
        dates_window = [from_+timedelta(days=i) for i in range(int(days_))]
        #mylogger.info(dates_window)
    elif len(days_) == 0 and len(from_)>0 and len(to_) == 0:
        #mylogger.info("when start date is given")
        dates_window = [datetime.strptime(from_, '%Y-%m-%d').date()]
        #mylogger.info(from_)
        #mylogger.info(dates_window)
    elif len(days_) == 0 and len(from_)>0 and len(to_) > 0:
        #mylogger.info("when start date is given")
        from_ = datetime.strptime(from_, '%Y-%m-%d').date()
        to_ = datetime.strptime(to_, '%Y-%m-%d').date()
        days_ = to_-from_
        dates_window = [from_+timedelta(days=i) for i in range(days_.days)]
        #dates_window = [datetime.strptime(from_, '%Y-%m-%d').date()]
        #mylogger.info(from_)
        #mylogger.info(dates_window)

    #mylogger.info(request.schedule_date)
    #mylogger.info(members)
    active_slots_list = get_max_st_and_min_end_time(crud.get_active_slots(members))
    print("the active slots are............")
    print(active_slots_list)
    busy_slots_list = crud.get_busy_slots(members, dates_window)
    print("overall busy slots list is..........")
    print(busy_slots_list)
   # mylogger.info(busy_slots_list)
   # mylogger.info(active_slots_list)

    # if len(busy_slots_list) == 0:
    #     times = active_slots_list#get_max_st_and_min_end_time(active_slots_list)
    #     #mylogger.info(active_slots_list)
    #     #return active_slots_list
    # else:
    if not busy_slots_list:
        active_slots_dict = {}
        for window in dates_window:
            active_slots_dict[window] = active_slots_list
        #mylogger.info(active_slots_dict)
        return active_slots_dict
    else:
        common_free_slots = {}
        for window in dates_window:
            window = window.strftime('%Y-%m-%d')
            #mylogger.info(str(window))
            #mylogger.info(list(busy_slots_list.keys()))
            #mylogger.info(type(window), type(list(busy_slots_list.keys())[0]))
            if window not in busy_slots_list.keys():
                common_free_slots[window] = active_slots_list
            else:
                timings = busy_slots_list[window]
                common_free_slots[window] = get_common_free_slots(timings, active_slots_list)
        # for date, timings in busy_slots_list.items():
        #     mylogger.info(timings)
        #     common_free_slots[date] = get_common_free_slots(timings, active_slots_list)

        #mylogger.info(common_free_slots)
        print("common free slots are...........")
        print(common_free_slots)
        common_free_slots = divide_common_slots(common_free_slots, duration)
        return {'free_slots':common_free_slots}


# Render Create action page
@meetings_router.get("/render_create_action/{meeting_key}")
async def render_create_action(
    request: Request,  meeting_key: str, current_user: User = Depends(get_current_active_user)
) -> "Jinja2Templates":
    """
    Render create action list page

    Parameter
    ----------
    request : Request

    username : str
            username of host of the meeting

    meeting_key : str
            meeting_key for the meeting whose action list needs to be created

    Return
    -------
    Jinja2Templates
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=render_create_action/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)
    if crud.check_meeting_existence_and_gethost(meeting_key) != current_user.email:
        return "No meeting with the given key exists for the user"
    return templates.TemplateResponse(
        "create_action.html",
        {"request": request, "username": current_user.email, "meeting_key": meeting_key,},
    )


# Create action
@meetings_router.post("/create_action/{meeting_key}")
async def create_action( meeting_key: str,ca:CreateAction=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    Create action list

    Parameter
    ----------
    request : Request

    username : str
            username of host of the meeting

    meeting_key : str
            meeting_key for the meeting whose action list needs to be created

    Return
    -------
    Jinja2Templates
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=render_create_action/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)
    if crud.check_meeting_existence_and_gethost(meeting_key) != current_user.email:
        return "No meeting with the given key exists"
    data = ca.__dict__

    crud.create_action_list(current_user.email, meeting_key, data)

    return {"action created successfully !!"}


@meetings_router.get("/delete_action/{meeting_key}/{action_key}")
async def delete_action(meeting_key: str, action_key: str, current_user: User = Depends(get_current_active_user)):
    """
    delete action list from the meeting

    Parameter
    ----------
    meeting_key : str
            meeting_key of the meeting from which actio list is to be deleted
    action_key : str
            action_key of the action which is to be deleted
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=delete_action/{meeting_key}/{action_key}", status_code=status.HTTP_303_SEE_OTHER)
    if crud.check_meeting_existence_and_gethost(meeting_key) != current_user.email:
        return "No meeting with the given key exists"
    crud.delete_action_list(meeting_key, action_key)

    return {"action list deleted successfully"}


@meetings_router.post("/comment/{task_key}")
async def comment_task(task_key: str,tc:TaskComment=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    Assign task to a team member

    Parameter
    ----------
    request : Request

    username : str
            username of the user to whom task needs to be assigned
    task_key : str
            task_key of the task which is going to be assigned

    Return
    -------
    str
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=comment/{task_key}", status_code=status.HTTP_303_SEE_OTHER)
    data = tc.__dict__
    data = data["comment"]
    crud.add_comment_task(current_user.email, data, task_key)
    return {"Commented successfully"}


# Assign task
@meetings_router.post("/assign_task/{action_key}/{task_key}")
async def assign_task( action_key: str, task_key: str,at:AssignTask=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    Assign task to a team member

    Parameter
    ----------
    request : Request

    username : str
            username of the user to whom task needs to be assigned
    task_key : str
            task_key of the task which is going to be assigned

    Return
    -------
    str
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=assign_task/{action_key}/{task_key}", status_code=status.HTTP_303_SEE_OTHER)

    data = at.__dict__
    if data:
        data = data["assignees"].split(",")
        crud.assign_task(data, task_key)
    else:
        username = username.split(",")
        crud.assign_task(current_user.email, task_key)

    return {"Members entered are successfully assigned"}


# Unassign task
@meetings_router.post("/unassign_task/{action_key}/{task_key}")
async def unassign_task( task_key: str,at:AssignTask=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    Assign task to a team member

    Parameter
    ----------
    request : Request

    username : str
            username of the user who is going to unassign the task
    task_key : str
            task_key of the task which is going to be unassigned to the team members

    Return
    -------
    str
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=unassign_task/{action_key}/{task_key}", status_code=status.HTTP_303_SEE_OTHER)

    data = at.__dict__
    if data:
        data = data["assignees"].split(",")
        crud.unassign_task(data, task_key)
    else:
        crud.unassign_task(current_user.email, task_key)

    return {"Members entered are successfully unassigned"}


# add task
@meetings_router.post("/add_task/{action_key}")
async def add_task(action_key: str,nt:NewTask=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    function to add task to the present action list

    Parameter
    ----------
    request : Request

    username : str
            username of the user who is adding the add
    action_key : str
            action_key of the action to which new task is added
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=assign_task/{action_key}/{task_key}", status_code=status.HTTP_303_SEE_OTHER)

    data = nt.__dict__
    print(data)

    crud.add_task(current_user.email, action_key, data)

    return {"new task is successfully added"}


# modifiy task
@meetings_router.post("/modifiy_task/{action_key}/{task_key}")
async def modify_task( action_key: str, task_key: str,mt:ModifyTask=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    function to modify the task in the present action list

    Parameter
    ----------
    request : Request

    action_key : str
            action_key of the action to which task is modified
    task_key : str
            task_key of the task which is modified
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=modifiy_task/{action_key}/{task_key}", status_code=status.HTTP_303_SEE_OTHER)

    data = mt.__dict__

    crud.modify_task(action_key, task_key, data)

    return {"task modified successfully"}


# delete task
@meetings_router.get("/delete_task/{action_key}/{task_key}")
async def delete_task( action_key: str, task_key: str, current_user: User = Depends(get_current_active_user)):
    """
    function to delete task from the present action list

    Parameter
    ----------
    action_key : str
            action_key of the action from which task is deleted
    task_key : str
            task_key of the task which is deleted

    Return
    -------
    str
    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=delete_task/{action_key}/{task_key}", status_code=status.HTTP_303_SEE_OTHER)

    crud.delete_task(action_key, task_key)

    return {"task deleted successfully"}

#display all tasks
@meetings_router.get("/display_tasks")
async def display_tasks(request: Request, current_user: User = Depends(get_current_active_user)): #For testing directly have set current_user
    """
    Display all tasks of the user of present organization
    """
    user_org = crud.get_user_org(current_user.email)
    all_user_org_task_docs = crud.get_all_org_tasks(current_user.email,user_org)
    return templates.TemplateResponse("display_tasks.html", {"request":request, "tasks":all_user_org_task_docs})

# decision task
@meetings_router.post("/insert_decision/{meeting_key}")
async def insert_decision( meeting_key: str,id:InsertDecision=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    Insert decisions to a meeting

    Parameter
    ----------
    request : Request

    meeting_key : str
            meeting_key of the meeting to which 
            decisions need to be inserted
    username : str
            username of the user who is inserting
             the new decisions

    Return
    -------
    str
    """

    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=insert_decision/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)

    data = id.__dict__
    data = data["decision"].split("\r\n")

    status = crud.insert_decision(meeting_key, data, current_user.email)

    if status:
        return {"Decisions inserted successfully"}
    else:
        return {"Decisions not inserted successfully. Please try again"}


@meetings_router.get("/get_decision/{meeting_key}/{is_host}")
async def get_decision(
    request: Request, meeting_key: str, is_host: bool,current_user: User = Depends(get_current_active_user)
):
    """
    function to get decisions and comments of a meeting

    Parameter
    ----------
    request : Request

    meeting_key : str
            meeting_key of the meeting from which decisions
            and comments need to be retrieved
    username : str
            username of the user who is part of the meeting
    is_host : bool
            if user is host or not

    Return
    -------
    Jinja2Templates
    """
    if not current_user:
        return RedirectResponse(url="/api/v1/login?redirect_url=accept_pending_meetings/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)

    data = crud.get_decision(meeting_key)

    return templates.TemplateResponse(
        "decision.html",
        {
            "request": request,
            "decisions": data,
            "meeting_key": meeting_key,
            "username": current_user.email,
            "is_host": is_host,
        },
    )


@meetings_router.get(
    "/delete_decision/{meeting_key}/{decision_key}/{decision_name}/{is_host}"
)
async def delete_decision(
    meeting_key: str,
    decision_key: str,
    decision_name: str,
    is_host: bool,
    current_user: User = Depends(get_current_active_user)
):
    """
    delete decision from a meeting
    
    Parameter
    ----------
    meeting_key : str
            meeting_key of the meeting from which decision needs to be deletd
    decision_key : str
            decision_key of the decision from which we are deleting
    username : str
            username of the user who is deleting this decision
    is_host : bool
            if the user is host of the meeting or not
    """

    if crud.delete_decision(meeting_key, decision_key, decision_name):
        return RedirectResponse(
            url=f"/api/v1/get_decision/{meeting_key}/{is_host}",
            status_code=status.HTTP_303_SEE_OTHER,
        )
    else:
        return {"Decision is not deleted successfully. Please try again"}


@meetings_router.post("/modifiy_decision/{meeting_key}/{decision_key}/{decision_name}")
async def modifiy_decision(
 meeting_key: str, decision_key: str, decision_name: str,md:ModifyDecision=Depends(),current_user: User = Depends(get_current_active_user)
):
    """
    function to modifify decision in a meeting

    Parameter
    ----------
    request : Request

    meeting_key : str
            meeting_key of the meeting in which we are modifying
            the decision
    decision_key : str
            decision_key of the decision doc which needs to be modified
    decision_name : str
            decision_name which needs to be modified
    
    Return
    -------
    str
    """

    data = md.__dict__
    if crud.modify_decision(meeting_key, decision_key, decision_name, data):
        return {"Decision modified successfully"}
    else:
        return {"Decision not modified successfully. Please try again"}


@meetings_router.get(
    "/add_upvote/{meeting_key}/{decision_key}/{decision_name}/{is_host}"
)
async def add_upvote(
    meeting_key: str,
    decision_key: str,
    decision_name: str,
    is_host: bool,
    current_user: User = Depends(get_current_active_user)
):
    """
    function to add upvote to the upvoted list

    Parameter
    ----------
    decision_key : str
            decision_key of the decision in which user wants to upvote
    decision_name : str
            decision_name of the decision to which user wants to upvote
    username : str
            username of the user who is upvoting
    is_host : bool
            if the user is the host or not
    
    Return
    -------
    RedirectResponse
    """

    status1 = crud.add_upvote(decision_key, decision_name, current_user.email)
    status2 = crud.remove_downvote(decision_key, decision_name, current_user.email)
    if status1:
        return RedirectResponse(
            url=f"/api/v1/get_decision/{meeting_key}/{is_host}",
            status_code=status.HTTP_303_SEE_OTHER,
        )
    else:
        return {"Please retry again"}


@meetings_router.get(
    "/remove_upvote/{meeting_key}/{decision_key}/{decision_name}/{is_host}"
)
async def remove_upvote(
    meeting_key: str,
    decision_key: str,
    decision_name: str,
    is_host: bool,
    current_user: User = Depends(get_current_active_user)
):
    """
    function to remove upvote from the upvoted list

    Parameter
    ----------
    decision_key : str
            decision_key of the decision in which user wants to remove upvote
    decision_name : str
            decision_name of the decision to which user wants to remove upvote
    username : str
            username of the user who is removing upvote
    is_host : bool
            if the user is the host or not
    
    Return
    -------
    RedirectResponse
    """

    if crud.remove_upvote(decision_key, decision_name, current_user.email):
        return RedirectResponse(
            url=f"/api/v1/get_decision/{meeting_key}/{is_host}",
            status_code=status.HTTP_303_SEE_OTHER,
        )
    else:
        return {"Please retry again"}


@meetings_router.get(
    "/add_downvote/{meeting_key}/{decision_key}/{decision_name}/{is_host}"
)
async def add_downvote(
    meeting_key: str,
    decision_key: str,
    decision_name: str,
    is_host: bool,
    current_user: User = Depends(get_current_active_user)
):
    """
    function to add downvote to the downvoted list

    Parameter
    ----------
    decision_key : str
            decision_key of the decision in which user wants to downvote
    decision_name : str
            decision_name of the decision to which user wants to downvote
    username : str
            username of the user who is downvoting
    is_host : bool
            if the user is the host or not
    
    Return
    -------
    RedirectResponse
    """

    status1 = crud.add_downvote(decision_key, decision_name, current_user.email)
    status2 = crud.remove_upvote(decision_key, decision_name, current_user.email)

    if status1:
        return RedirectResponse(
            url=f"/api/v1/get_decision/{meeting_key}/{is_host}",
            status_code=status.HTTP_303_SEE_OTHER,
        )
    else:
        return {"Please retry again"}


@meetings_router.get(
    "/remove_downvote/{meeting_key}/{decision_key}/{decision_name}/{is_host}"
)
async def remove_downvote(
    meeting_key: str,
    decision_key: str,
    decision_name: str,
    is_host: bool,
    current_user: User = Depends(get_current_active_user)
):
    """
    function to remove downvote to the downvoted list

    Parameter
    ----------
    decision_key : str
            decision_key of the decision in which user wants to remove downvote
    decision_name : str
            decision_name of the decision to which user wants to remove downvote
    username : str
            username of the user who is removing downvote
    is_host : bool
            if the user is the host or not
    
    Return
    -------
    RedirectResponse
    """

    if crud.remove_downvote(decision_key, decision_name, current_user.email):
        return RedirectResponse(
            url=f"/api/v1/get_decision/{meeting_key}/{is_host}",
            status_code=status.HTTP_303_SEE_OTHER,
        )
    else:
        return {"Please retry again"}


@meetings_router.post("/add_comment/{decision_key}")
async def add_comment( decision_key: str,ac:AddComment=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    function to add comment to the decision of a meeting

    Parameter 
    ----------
    request : Request

    decision_key : str
            decision_key of the decision to which comment is added
    username : str
            username of the user who is commenting to a decision
    
    Return
    -------
    str
    """

    data = ac.__dict__
    data = data["comment"]

    if crud.add_comment(decision_key, current_user.email, data):
        return {"Your comment is added successfully"}
    else:
        return {"Please try again"}

@meetings_router.get("/notes/{meeting_key}")
async def add_notes(request: Request, meeting_key: int, current_user: User = Depends(get_current_active_user)):
    """
    function to add notes of a meeting

    Parameter 
    ----------
    request : Request

    meeting_key : int
            meeting_key of the meeting in which notes are added
    
    Return
    -------
    str
    """
    noteeditdisplay = "none"
    noteeditdisable = "true"
    if crud.meeting_going_on(str(meeting_key)) and crud.check_host(current_user.email,str(meeting_key)):
        noteeditdisplay = "display"
        noteeditdisable = ""
    mypathstr = crud.get_path_notes(str(meeting_key))
    mypath=os.path.join(os.path.join(os.path.abspath(os.path.dirname(__file__)),'notes'),mypathstr)
    try:
        os.makedirs(mypath)
    except:
        mylogger.info("Folder exists")
    try:
        notesfile = open(os.path.join(mypath,f"{meeting_key}.txt"),"r")
    except:
        notesfile = open(os.path.join(mypath,f"{meeting_key}.txt"),"x")
        notesfile = open(os.path.join(mypath,f"{meeting_key}.txt"),"r")
    notes = notesfile.read()
    notesfile.close()
    if crud.add_notes(str(meeting_key), mypath):
        mylogger.info("Your path is added successfully")
    else:
        mylogger.info("Please try again")
    return templates.TemplateResponse("meeting_notes.html", {"request": request,"notes":notes,"meeting_key": meeting_key,"username":current_user.email,"disp":noteeditdisplay,"notedisp":noteeditdisable})


@meetings_router.post("/notes/{meeting_key}")
async def add_notes(request: Request, meeting_key: int,ad:AddNotes=Depends(),current_user: User = Depends(get_current_active_user)):
    """
    function to add notes of a meeting

    Parameter 
    ----------
    request : Request

    meeting_key : int
            meeting_key of the meeting in which notes are added
    username : str
            username of user
    
    Return
    -------
    redirect
    """
    mypathstr = crud.get_path_notes(str(meeting_key))
    mypath=os.path.join(os.path.join(os.path.abspath(os.path.dirname(__file__)),'notes'),mypathstr)
    notesfile = open(os.path.join(mypath,f"{meeting_key}.txt"),"w")
    
    # Save data to local file
    data = ad.__dict__
    data = data["notes"]
    data = re.sub(r'\n\s*', '', data)
    mylogger.info(data)
    notesfile.write(data)
    notesfile.close()
    return RedirectResponse(url=f"/api/v1/notes/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)

@meetings_router.get("/viewnote/{meeting_key}")
async def view_notes(request: Request, meeting_key: int, current_user: User = Depends(get_current_active_user)):
    """
    function to add notes of a meeting

    Parameter 
    ----------
    request : Request

    meeting_key : int
            meeting_key of the meeting in which notes are added
    
    Return
    -------
    str
    """
    buttontext = ""
    disp = "none"
    if crud.meeting_going_on(str(meeting_key)):
        if crud.check_host(username,str(meeting_key)):
            if crud.check_notes(str(meeting_key)):
                buttontext = "Modify Notes"
            else:
                buttontext = "Create Notes"
            disp = "display"
        elif crud.check_invitee(username,str(meeting_key)):
            if crud.check_notes(str(meeting_key)):
                buttontext = "View Notes"
                disp = "display"
    return templates.TemplateResponse("view_notes.html", {"request": request,"meeting_key": meeting_key,"username":current_user.email,"disp":disp,"buttontext":buttontext})



################## APIS FOR OUTLOOK CALENDER ##########################
#################################################################################
import requests as rq


def make_api_call(method, url, token, body=None,tzone=None):

    """
    function to make api calls for outlook calender

    Parameter 
    ----------
    method : specify which method it is from 'GET','POST','PATCH','PUT' or 'DELETE'

    url : url of api to which request is made

    token : access token provided by microsoft of the user which is currently logged in

    body : request body 

    tzon : 
    
    Return
    -------
    response in json form
    """

    # Send these headers with all API calls
    headers = {
        'Authorization' : '{0}'.format(token),
        'Content-Type': 'application/json'
    }

    if tzone!=None : 
        headers["Prefer"] = 'outlook.timezone="'+str(tzone)+'"'
    if method == "GET":
        response = rq.get(url, headers = headers)

    elif method == "POST":
        response = rq.post(url,  data = json.dumps(body),headers = headers)

    elif method == "DELETE":
        response = rq.delete(url, headers = headers)
        return response
    
    elif method == "PUT":
        headers['Content-Type']='text/plain'
        response = rq.put(url,  data = json.dumps(body), headers = headers)
        
    elif method == "PATCH":
        response = rq.patch(url,  data = json.dumps(body), headers = headers)

    return response.json()


@meetings_router.get("/list-events/")
async def listEvents(request: Request, current_user: User = Depends(get_current_active_user)) -> "Response":

    """
    function to list events of the user currently logged in

    Parameter 
    ----------
    request : Request
    
    Return
    -------
    HTML response (list_events template)
    """

    access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
  
    r = make_api_call('GET', "https://graph.microsoft.com/v1.0/me/calendar/events", access_token, tzone=tzlocal.get_localzone().zone)

    if("error" in r) :
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"Unable to validate social login, error is {r}")

        ###### IF YOU NEED ANY OTHER DETAILS OF MEETING THEN SEE THIS SAMPLE EVENT 
        #  {
        #     "@odata.etag": "W/\"yx+I9cZGv...ANg43mw==\"",
        #     "id": "AQMkADAwATM3ZmYAZS0zMmFjLWE0MGMtMDACLT...4wAAADYOenEAAAA=",
        #     "createdDateTime": "2020-11-02T10:10:17.2187477Z",
        #     "lastModifiedDateTime": "2020-11-02T10:10:39.491879Z",
        #     "changeKey": "yx+I9cZGvEa+...4wAANg43mw==",
        #     "categories": [],
        #     "transactionId": "55f51c63-XXXX-XXXX-XXXX-dcb029c357bf",
        #     "originalStartTimeZone": "India Standard Time",
        #     "originalEndTimeZone": "India Standard Time",
        #     "iCalUId": "04000000820...FE1DF24500010F4E9BCC84653D0442E7",
        #     "reminderMinutesBeforeStart": 15,
        #     "isReminderOn": false,
        #     "hasAttachments": false,
        #     "subject": "Meeting 2",
        #     "bodyPreview": "",
        #     "importance": "normal",
        #     "sensitivity": "normal",
        #     "isAllDay": false,
        #     "isCancelled": false,
        #     "isOrganizer": true,
        #     "responseRequested": true,
        #     "seriesMasterId": null,
        #     "showAs": "busy",
        #     "type": "singleInstance",
        #     "webLink": "https://outlook.live.com/owa/?itemid=AQMkADAwATM...pUUCmAAAA%3D&exvsurl=1&path=/calendar/item",
        #     "onlineMeetingUrl": null,
        #     "isOnlineMeeting": false,
        #     "onlineMeetingProvider": "unknown",
        #     "allowNewTimeProposals": true,
        #     "isDraft": false,
        #     "recurrence": null,
        #     "onlineMeeting": null,
        #     "responseStatus": {
        #         "response": "organizer",
        #         "time": "0001-01-01T00:00:00Z"
        #     },
        #     "body": {
        #         "contentType": "html",
        #         "content": ""
        #     },
        #     "start": {
        #         "dateTime": "2020-11-02T14:00:00.0000000",
        #         "timeZone": "UTC"
        #     },
        #     "end": {
        #         "dateTime": "2020-11-02T15:30:00.0000000",
        #         "timeZone": "UTC"
        #     },
        #     "location": {
        #         "displayName": "",
        #         "locationType": "default",
        #         "uniqueIdType": "unknown",
        #         "address": {},
        #         "coordinates": {}
        #     },
        #     "locations": [],
        #     "attendees": [],
        #     "organizer": {
        #         "emailAddress": {
        #             "name": "Priyav Kaneria",
        #             "address": "notpriyavkaneria@outlook.com" PLEASE DO NOT SPAM
        #         }
        #     }
        # }
    return templates.TemplateResponse("list_events.html", 
    {
        "request": request,
        "events_list": r["value"]
    })


@meetings_router.get("/update-event/{id}")
async def deleteEvent(request: Request, id: str, current_user: User = Depends(get_current_active_user)) -> "Response":
    """
    function to render the form for event to be updated (populating the values of the event prevously created)

    Parameter 
    ----------
    request : Request

    id : string
               id of event to be updated
    
    Return
    -------
    HTML view (form)
    """

    access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
    r = make_api_call('GET', f"https://graph.microsoft.com/v1.0/me/events/{id}", access_token, tzone=tzlocal.get_localzone().zone)
    data = r['bodyPreview']
    return templates.TemplateResponse("update_meeting_microsoft.html", 
    {
        "request": request,
        "event": r,
        "agenda": data
    })


@meetings_router.post("/update-event/{id}")
async def deleteEvent( id: str,uem:UpdateEventMicrosoft=Depends(), current_user: User = Depends(get_current_active_user)) -> "Response":
    """
    function to update the event in oulook calender by receiving form data and hence making api call for updation

    Parameter 
    ----------
    request : Request

    id : string
               id of event to be updated
    
    Return
    -------
    redirect response that will redirect to list events page showing updated values
    """

    data = uem.__dict__
    access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
    start_time = datetime.strptime(data['date']+' '+data['start_time']+':00', "%Y-%m-%d %H:%M:%S")
    end_time = datetime.strptime(data['date']+' '+data['end_time']+':00', "%Y-%m-%d %H:%M:%S")
    body = {
                "subject": data["meeting_subject"],
                "body": {
                    "contentType": "HTML",
                    "content": data["agenda"]
                },
                "start": {
                    "dateTime": f"{str(start_time.date())}T{str(start_time.time())}",
                    "timeZone": str(tzlocal.get_localzone().zone)
                },
                "end": {
                    "dateTime": f"{str(end_time.date())}T{str(end_time.time())}",
                    "timeZone": str(tzlocal.get_localzone().zone)
                }
            }
    r = make_api_call('PATCH', f"https://graph.microsoft.com/v1.0/me/events/{id}", access_token, body=body)
    return RedirectResponse(
        url="/api/v1/list-events", status_code=status.HTTP_303_SEE_OTHER
    )


@meetings_router.get("/delete-event/{id}")
async def deleteEvent(request: Request, id: str, current_user: User = Depends(get_current_active_user)) -> "Response":

    """
    function to delete the event already created in outlook calender in past

    Parameter 
    ----------
    request : Request

    id : string
               id of event to be deleted
    
    Return
    -------
    redirect response that will redirect to list events page showing updated values
    """

    access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
    r = make_api_call('DELETE', f"https://graph.microsoft.com/v1.0/me/events/{id}", access_token)
    return RedirectResponse(
        url="/api/v1/list-events", status_code=status.HTTP_303_SEE_OTHER
    )

##################### END OF APIS FOR OUTLOOK CALENDER ##########################
#################################################################################


################## APIS FOR GOOGLE CALENDER ##########################
#################################################################################

@meetings_router.get("/list-events-google/")
async def listEvents(request: Request, current_user: User = Depends(get_current_active_user)) -> "Response":

    """
    function to list events of the user currently logged in

    Parameter 
    ----------
    request : Request
    
    Return
    -------
    HTML response (list_events template)
    """
    mylogger.info(current_user)
    service = get_service(current_user.email,"calendar")
    page_token = None
    while True:
      events = service.events().list(calendarId='primary', pageToken=page_token).execute()
      for event in events['items']:
        mylogger.info(event)
      page_token = events.get('nextPageToken')
      if not page_token:
        break
   

    
    return templates.TemplateResponse("list-events-google.html", 
    {
        "request": request,
        "events_list": events['items']
    })


@meetings_router.get("/delete-event-google/{id}")
async def deleteEvent(request: Request, id: str, current_user: User = Depends(get_current_active_user)) -> "Response":

    """
    function to delete the event already created in outlook calender in past

    Parameter 
    ----------
    request : Request

    id : string
               id of event to be deleted
    
    Return
    -------
    redirect response that will redirect to list events page showing updated values
    """
    service = get_service(current_user.email,"calendar")
    service.events().delete(calendarId='primary', eventId=id).execute()
    return RedirectResponse(
        url="/api/v1/list-events-google", status_code=status.HTTP_303_SEE_OTHER
    )

@meetings_router.get("/update-event-google/{id}")
async def deleteEvent(request: Request, id: str, current_user: User = Depends(get_current_active_user)) -> "Response":
    """
    function to render the form for event to be updated (populating the values of the event prevously created)

    Parameter 
    ----------
    request : Request

    id : string
               id of event to be updated
    
    Return
    -------
    HTML view (form)
    """
    service = get_service(current_user.email,"calendar")
    event = service.events().get(calendarId='primary', eventId=id).execute()
    return templates.TemplateResponse("update_meeting_google.html", 
    {
        "request": request,
        "event": event
    })

@meetings_router.post("/update-event-google/{id}")
async def deleteEvent(id: str,uge:UpdateGoogleEvent=Depends(), current_user: User = Depends(get_current_active_user)) -> "Response":
    """
    function to update the event in oulook calender by receiving form data and hence making api call for updation

    Parameter 
    ----------
    request : Request

    id : string
               id of event to be updated
    
    Return
    -------
    redirect response that will redirect to list events page showing updated values
    """

    data = uge.__dict__
    start_time = datetime.strptime(data['date']+' '+data['start_time']+':00', "%Y-%m-%d %H:%M:%S")
    end_time = datetime.strptime(data['date']+' '+data['end_time']+':00', "%Y-%m-%d %H:%M:%S")

    mylogger.info(start_time,end_time)

    event = {
                "summary": data["meeting_subject"],
                "start": {
                    "dateTime": start_time.strftime("%Y-%m-%dT%H:%M:%S"),
                    "timeZone": str(tzlocal.get_localzone().zone)
                },
                "end": {
                    "dateTime": end_time.strftime("%Y-%m-%dT%H:%M:%S"),
                    "timeZone": str(tzlocal.get_localzone().zone)
                }
            }
   
    service = get_service(current_user.email,"calendar")
    updated_event = service.events().update(calendarId='primary', eventId=id, body=event).execute()
    return RedirectResponse(
        url="/api/v1/list-events-google", status_code=status.HTTP_303_SEE_OTHER
    )

##################### END OF APIS FOR GOOGLE CALENDER ##########################
#################################################################################

#################################################################################
########################## APIS FOR organizationS ###############################

#Modified: For Onboarded User implentation (user can not access home page till its onboarded)
@meetings_router.get("/select_workspace")
async def search_worspace(request: Request):
    return templates.TemplateResponse("select_workspace.html", {"request": request})

@meetings_router.post('/get_workspace')
async def get_workspace(org: Org = Depends()):
    data = org.__dict__
    #mylogger.info(data)
    # data = json.loads(data)
    # mylogger.info(data)
    workplace_status, workspace = crud.get_org_by_workspace(data['org_name'])
    #mylogger.info(workplace_status)
    #mylogger.info(workspace)
    if workplace_status == True:
        return RedirectResponse(url = f"/api/v1/home", status_code=status.HTTP_303_SEE_OTHER)
    else:
        return status.HTTP_404_NOT_FOUND


    # if workspace == None:
    #     return False

    #return True, workspace['organization']



#Modified: For Onboarded User implentation (user can not access home page till its onboarded)
@meetings_router.get("/add-organization/{email}", response_class=HTMLResponse)
async def get_create_org_page(request: Request,email: str):
    # current_user = crud.get_user(email)
    # if current_user["onboarded_yn"]:
    #     return RedirectResponse(url = "/api/v1/homepage", status_code=status.HTTP_303_SEE_OTHER)
    return templates.TemplateResponse("create_organization.html", {"request": request, "email":email})


#Modified: For Onboarded User implentation (user can not access home page till its onboarded)
@meetings_router.get("/add-organization", response_class=HTMLResponse)
async def get_create_org_page(request: Request, current_user: User = Depends(get_current_active_user)):
    # current_user = crud.get_user(email)
    # if current_user["onboarded_yn"]:
    #     return RedirectResponse(url = "/api/v1/homepage", status_code=status.HTTP_303_SEE_OTHER)
    return templates.TemplateResponse("create_organization.html", {"request": request, "email":current_user.email})



@meetings_router.post('/create_org_account')
async def create_org_acocunt(request: Request, background_tasks: BackgroundTasks, org_acc: Createorganization=Depends(), current_user: User = Depends(get_current_active_user)):
    data = org_acc.__dict__
    email = data['email']
    del data['email']
    user_org = crud.get_user_org(email)
    cuser = crud.get_user(email)
    if not cuser:
        return JSONResponse({"status":status.HTTP_403_FORBIDDEN,"comment":"User not found"})
    print("Data is ",data)
    #try:
    org_result = crud.create_org(data = data, email = email)
    print("the org result is...")
    print(org_result)
    if org_result is False:
        return JSONResponse({
                'status_code':status.HTTP_417_EXPECTATION_FAILED,
                'detail':'expected data to create org is not requested.'
                })
            #return {"Your request for creating org has failed please make account again"}
    '''
    except Exception as e:
        return JSONResponse({
                'status_code':status.HTTP_417_EXPECTATION_FAILED,
                'detail':str(e)
                })
    '''
    #try:
    link_result = crud.linking(data = {"organization": data["organization"], "admin": email}, admin=True)
    print("the link result is...")
    print(link_result)
    if link_result is False:
        #print("something bad in link_result has happened")
        return JSONResponse({
                'status_code':status.HTTP_417_EXPECTATION_FAILED,
                'detail':'user_data is invalid'
                })
    '''
    except Exception as e:
        print("something.... related to linking")
        return JSONResponse({
                'status_code':status.HTTP_417_EXPECTATION_FAILED,
                'detail':str(e)
                })
    '''
    try:
        folder_path = os.path.join(file_dir,"resources")
        folder_path = os.path.join(folder_path,data['organization'])
        os.mkdir(folder_path)
        os.mkdir(os.path.join(folder_path,"projects"))
        os.mkdir(os.path.join(folder_path,"teams"))
        os.mkdir(os.path.join(folder_path,"meetings"))
    except Exception as e:
        print(e)
    if user_org == "":
        return JSONResponse({
            'status_code':status.HTTP_100_CONTINUE,
            'url':f"/api/v1/invite_members/{data['organization']}/{email}",
            'detail':'when the user not logged in and creating the workspace.'
        })
        #return RedirectResponse(url=f"/api/v1/invite_members/{data['organization']}/{email}", status_code=status.HTTP_303_SEE_OTHER)
    else:
        return JSONResponse({
            'status_code':status.HTTP_100_CONTINUE,
            'url':f"/api/v1/invite_members/{data['organization']}",
            'detail':'when the user not logged in and creating the workspace.'
        })
        #return RedirectResponse(url=f"/api/v1/invite_members/{data['organization']}", status_code=status.HTTP_303_SEE_OTHER)



# @meetings_router.post('/create_organization', response_class=HTMLResponse)
# async def create_org(co:Createorganization=Depends(), current_user: User = Depends(get_current_active_user)):
#     # if current_user==None:
#     #     redirect_url="create_organization"
#     #     return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
#     details = co.__dict__
#     details['admin'] = current_user.email
#     #mylogger.info(details['admin'])
#     details['since'] = date.today()
#     #mylogger.info(details)
#     print(details)
    
#     crud.create_org(details)
#     crud.linking(details,True)
#     return RedirectResponse(url = f"/api/v1/invite_members", status_code=status.HTTP_303_SEE_OTHER)


@meetings_router.post('/invite_teammates')
async def invite_teammembers(request: InviteMembers, current_user: User = Depends(get_current_active_user)):

    company = crud.get_user_org(current_user.email)['_key'].split('_')[0]
    mylogger.info(organization)
    # user_with_org = crud.get_users_of_org(organization)
    # email_ids = list(user_with_org.keys())

    details= jsonable_encoder(request)
    crud.adding_members_to_org(organization, current_user.email)


@meetings_router.post('/delete_members')
async def delete_members(request: DeleteMembers):

    details = jsonable_encoder(request)
    crud.deleting_members_to_org(details['org_name'], details['members'])


conf = ConnectionConfig(
    MAIL_USERNAME="contact@aztlan.in",
    MAIL_PASSWORD="Aztlan@123",
    MAIL_PORT=587,
    #MAIL_FROM = "contact@aztlan.in",
    MAIL_SERVER="smtp.office365.com",
    MAIL_TLS=True,
    MAIL_SSL=False
)



@meetings_router.get("/invite_members/{organization}/{email}")
async def read_item(request:Request, organization: str, email: str):
    return templates.TemplateResponse("item.html", {"request":request, "org":organization, "email":email})


@meetings_router.get("/invite_members/{organization}")
async def read_item(request:Request, organization: str, current_user: User = Depends(get_current_active_user)):
    return templates.TemplateResponse("item.html", {"request":request, "org":organization, "email":current_user.email})


async def send_event_data(details, users_with_org=None):
    mylogger.info(details)
    mylogger.info(users_with_org)
    email_list = list(users_with_org.keys())
    if users_with_org is not None:
        attendees = [v for k, v in users_with_org.items()]
    else:
        attendees = email_list

    #[detail.get('email') for detail in details.get('attendees')]
    #email_list.append(details['creator']['email'])
    
    mylogger.info(email_list)
    html ="""description:\n {} \n\n attendees:\n {} """.format(details.get('description'), '\n'.join(attendees))
    mylogger.info(html)
    bgt = BackgroundTasks()
    
    message = MessageSchema(
        subject=details.get('summary'),
        recipients= email_list,
        body=html,
        subtype="html"
        )
    fm = FastMail(conf)
    #await fm.send_message(message)
    bgt.add_task(fm.send_message, message)

def get_message_template(company:str,key:str):
    html = """
    <html> 
    <body>
    <p>Hi This test mail
    <br>Thanks for using Fastapi-mail</p> 
    <br>use below link to join into the organization
        <a href ="{}">click here </a>
    </body> 
    </html>
    """.format("http://api.workpeer.in/api/v1/invite/"+company+"/"+key)
    return html

@meetings_router.post("/emailbackground")
async def send_in_background(background_tasks: BackgroundTasks, Email: InviteEmailSchema = Depends()):
    #mylogger.info(dict(request))
    #mylogger.info(request.email)
    # company = crud.get_user_org(current_user.email)['_key'].split('_')[0]
    # mylogger.info(company)
    admin = Email.user_email
    print(admin)
    company = Email.organization
    print(company)
    print(Email.emails)
    mylogger.info(Email.emails)
    email_list = Email.emails.split(',')
    print(email_list)
    mylogger.info(email_list)
    fm = FastMail(conf)
    if email_list != [' ']:
        keys = crud.add_user_invitation(admin,email_list, company)
        #mylogger.info("org",company)
        mylogger.info(keys)
        for mail in keys:
            #mylogger.info(mail,"---->",keys[mail])
            message = MessageSchema(
                subject="Invite to join organization",
                recipients=[str(mail)],
                body=get_message_template(company,keys[mail]),
                subtype="html"
                )
            # await fm.send_message(message)
            background_tasks.add_task(fm.send_message, message)
    if crud.get_user_third_party(admin)!="custom":
        crud.set_onboard(admin)
        mylogger.info("User onboarded")
    return JSONResponse({
        'status_code':status.HTTP_200_OK,
        'url':"/api/v1/home",
        'detail':'email sent and redirect to home by default. If the user not authenticated, then it redirects to login page'
        })
    return RedirectResponse(url="/api/v1/home", status_code=status.HTTP_303_SEE_OTHER)


@meetings_router.post('/delete_members')
async def delete_members(request: DeleteMembers,current_user: User = Depends(get_current_active_user)):

    details = jsonable_encoder(request)
    crud.deleting_members_to_org(details['org_name'], details['members'])


conf = ConnectionConfig(
    MAIL_USERNAME="contact@aztlan.in",
    MAIL_PASSWORD="Aztlan@123",
    MAIL_PORT=587,
    #MAIL_FROM = "contact@aztlan.in",
    MAIL_SERVER="smtp.office365.com",
    MAIL_TLS=True,
    MAIL_SSL=False
)



@meetings_router.get("/invite_members")
async def read_item(request:Request, current_user: User = Depends(get_current_active_user)):
    return templates.TemplateResponse("item.html", {"request":request})



async def send_event_data(details, users_with_org=None):
    mylogger.info(details)
    mylogger.info(users_with_org)
    email_list = list(users_with_org.keys())
    if users_with_org is not None:
        attendees = [v for k, v in users_with_org.items()]
    else:
        attendees = email_list

    #[detail.get('email') for detail in details.get('attendees')]
    #email_list.append(details['creator']['email'])
    
    mylogger.info(email_list)
    html ="""description:\n {} \n\n attendees:\n {} """.format(details.get('description'), '\n'.join(attendees))
    mylogger.info(html)
    bgt = BackgroundTasks()
    
    message = MessageSchema(
        subject=details.get('summary'),
        recipients= email_list,
        body=html,
        subtype="html"
        )
    fm = FastMail(conf)
    #await fm.send_message(message)
    bgt.add_task(fm.send_message, message)

def get_message_template(company:str,key:str):
    html = """
    <html> 
    <body>
    <p>Hi This test mail
    <br>Thanks for using Fastapi-mail</p> 
    <br>use below link to join into the organization
        <a href ="{}">click here </a>
    </body> 
    </html>
    """.format("http://api.workpeer.in/api/v1/invite/"+company+"/"+key)
    return html
'''
@meetings_router.post("/emailbackground")
async def send_in_background(background_tasks: BackgroundTasks, email: InviteEmailSchema = Depends(), current_user: User = Depends(get_current_active_user)):
    #mylogger.info(dict(request))
    #mylogger.info(request.email)
    # company = crud.get_user_org(current_user.email)['_key'].split('_')[0]
    # mylogger.info(company)
    admin = current_user.email
    company = crud.get_user_org(current_user.email)
    mylogger.info(email)
    email_list = email.email.split(',')
    mylogger.info(email_list)
    keys = crud.add_user_invitation(admin,email_list, company)
    mylogger.info("org",company)
    mylogger.info(keys)
    fm = FastMail(conf)
    for mail in keys:
        mylogger.info(mail,"---->",keys[mail])
        message = MessageSchema(
            subject="Invite to join organization",
            recipients=[str(mail)],
            body=get_message_template(company,keys[mail]),
            subtype="html"
            )
        # await fm.send_message(message)
        background_tasks.add_task(fm.send_message, message)


    # return JSONResponse(status_code=200, content={"message": "email has been sent"})
    if crud.get_user_third_party(current_user.email)!="custom":
        crud.set_onboard(current_user.email)
        mylogger.info("User onboarded")
    return RedirectResponse(url="/api/v1/homepage", status_code=status.HTTP_303_SEE_OTHER)
'''

@meetings_router.get("/invite/{organization_name}/{key}")
async def activation_link_clicked(request: Request, organization_name: str, key: str, current_user: User = Depends(get_current_active_user)):

    if current_user is None:
        redirect_url="invite/"+organization_name+"/"+"key"
        #return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
        return JSONResponse({
                'status_code': status.HTTP_401_UNAUTHORIZED,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })
    email = key.split('_')[0]
    cuser = crud.get_user(email)
    if not cuser:
        #return RedirectResponse(url=f"/api/v1/new_user_signup?redirect_url=invite/{organization_name}/{key}", status_code=status.HTTP_303_SEE_OTHER)
        return JSONResponse({
            "status":status.HTTP_401_UNAUTHORIZED,
            'url':f"/api/v1/new_user_signup",
            'redirect_url':f"invite/{organization_name}/{key}",
            "comment":"User not found"})
    
    data = {}
    data["admin"] = email
    data["organization"] = organization_name
    data["_key"] = key
    crud.linking(data,False)
    mylogger.info("THE USER HAS BEEN LINKED TO organization SUCCESSFULLY")
    #crud.set_onboard(email)
    return JSONResponse({
        'status_code': status.HTTP_200_OK,
        'url':'workspaces',
        'detail':'go to workspaces route'
        })
    #return RedirectResponse(url = "/api/v1/workspaces", status_code=status.HTTP_303_SEE_OTHER)


@meetings_router.get("/add-calendar")
async def add_calendar_custom_user(request: Request, auth_url: str = "", current_user: User = Depends(get_current_active_user), ltype: Optional[str] = ""):
    if auth_url=="":
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"No auth URL found")
    if not current_user:
        return RedirectResponse(url = f"/api/v1/login", status_code=status.HTTP_303_SEE_OTHER)
    if ltype!="":
        return templates.TemplateResponse("add-calendar.html", {"request":request, "auth_url":auth_url, "ltype":ltype})
    if crud.get_user_third_party(current_user.email)!="custom":
        return RedirectResponse(url = "/api/v1/homepage", status_code=status.HTTP_303_SEE_OTHER)
    return templates.TemplateResponse("add-calendar.html", {"request":request, "auth_url":auth_url})

@meetings_router.get("/add-google-calendar")
async def add_google_calendar_custom_user(request: Request, current_user: User = Depends(get_current_active_user)):
    if not current_user:
        return RedirectResponse(url = f"/api/v1/login", status_code=status.HTTP_303_SEE_OTHER)
    service = get_service(current_user.email, "calendar")
    mylogger.info(current_user.email)
    mylogger.info("mylogger.infoed the current users email")
    crud.get_add_user_calendar(current_user.email, "google")
    crud.set_onboard(current_user.email)
    return RedirectResponse(url = "/api/v1/homepage", status_code=status.HTTP_303_SEE_OTHER)


@meetings_router.get("/add-microsoft-calendar")
async def add_google_calendar_custom_user(request: Request, token: str, reftoken: str, current_user: User = Depends(get_current_active_user)):
    mylogger.info(current_user)
    if not current_user:
        redirect_url = "add-microsoft-calendar"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
    if not crud.get_user_org(current_user.email):
        return RedirectResponse(url="/api/v1/add-organization/", status_code=status.HTTP_303_SEE_OTHER)
    crud.set_onboard(current_user.email)
    #current_user = crud.get_user(current_user.email)
    calendar = crud.get_add_user_calendar(current_user.email, "microsoft", token, reftoken)
    return RedirectResponse(url = "/api/v1/homepage", status_code=status.HTTP_303_SEE_OTHER)

# @meetings_router.get("/sendnotification")
# def send_notification(request: Request, email:str, message:str, id:Optional[str] = "", myrequest:str = 'post'):
#     crud.notify_user(email,message,id,myrequest)
#     return {"User Notification requested ;)"}



######################################################################################
################################## MEETING AGENDA ####################################
######################################################################################

# Author : Priyav Kaneria


@meetings_router.post("/agenda/add/{meeting_key}")
async def meeting_detail( meeting_key: str,aa:AddAgenda=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    Use to add agenda to a particular meeting

    Parameter
    ----------
    request : Request
    meeting_key : str

    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=meeting_details/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)
    username = current_user.email
    data = aa.__dict__
    # if username == crud.get_meeting_doc(meeting_key)["hostname"]:
    crud.add_meeting_agenda(meeting_key,data)
    for assignee in data["assignees"].split(','):
        crud.notify_user(assignee,"New meeting agenda assigned", f"Agenda {data['new_agenda']} has been assigned to you for meeting {crud.get_meeting_doc(meeting_key)['meeting_subject']}",f"/api/v1/meeting_details/{meeting_key}", meeting_key)
    return {"Your sub-agenda has been added successfully"}
    # return {"You are not the host of the meeting so you cannot add"}



@meetings_router.post("/agenda/modify/{meeting_key}/{agenda_key}")
async def meeting_detail( meeting_key: str, agenda_key: str,ma:ModifyAgenda=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    Use to modify agenda of a particular meeting

    Parameter
    ----------
    request : Request
    meeting_key : str
    agenda_key : str

    """
    if not current_user:
       return RedirectResponse(url=f"/api/v1/login/?redirect_url=meeting_details/{meeting_key}", status_code=status.HTTP_303_SEE_OTHER)
    username = current_user.email
    data = ma.__dict__
    # if username == crud.get_meeting_doc(meeting_key)["hostname"]:
    crud.modify_meeting_agenda(agenda_key,data)
    for assignee in data["assignees"].split(','):
        crud.notify_user(assignee,f"Agenda modified", f"Agenda {data['modified_agenda']} that has been assigned to you for meeting {crud.get_meeting_doc(meeting_key)['meeting_subject']} was modified",f"/api/v1/meeting_details/{meeting_key}", meeting_key)
    return {"Your sub-agenda has been modified successfully"}
    # return {"You are not the host of the meeting so you cannot add"}

######################################################################################
################################### SEARCH APIS ######################################
######################################################################################

# Author : Sejal Gupta

@meetings_router.get('/search_meetings_by_meeting_subject', response_class=HTMLResponse)
def get_search(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    function that renders search page for meetings to be searched by field meeting_subject

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template
    """
    if current_user==None:
        redirect_url="search_meetings_by_meeting_subject"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse("search_meetings_by_subject.html", {"request":request})


@meetings_router.post('/search_meetings_by_meeting_subject', response_class=HTMLResponse)
async def post_search(request: Request,sd:SearchData=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    function that takes the form data containing keyword to be searched and calling corrosponding search function from crud

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template and list of search_results
    """
    if current_user==None:
        redirect_url="search_meetings_by_meeting_subject"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
    details = sd.__dict__
    mylogger.info(details)
    search_results = crud.search_meetings_by_meeting_subject(details)
    mylogger.info(search_results)
    return templates.TemplateResponse("search_meetings_by_subject.html", {"request":request, "search_results":search_results})


@meetings_router.get('/search_meetings_by_agenda', response_class=HTMLResponse)
def get_search(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    function that renders search page for meetings to be searched by field agenda

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template
    """
    if current_user==None:
        redirect_url="search_meetings_by_agenda"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse("search_meetings_by_agenda.html", {"request":request})


@meetings_router.post('/search_meetings_by_agenda', response_class=HTMLResponse)
async def post_search(request: Request, sd:SearchData=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    function that takes the form data containing keyword to be searched and calling corrosponding search function from crud

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template and list of search_results
    """
    if current_user==None:
        redirect_url="search_meetings_by_agenda"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
    details = sd.__dict__
    mylogger.info(details)
    search_results = crud.search_meetings_by_agenda(details)
    return templates.TemplateResponse("search_meetings_by_agenda.html", {"request":request, "search_results":search_results})


@meetings_router.get('/search_meetings_by_invitees', response_class=HTMLResponse)
def get_search(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    function that renders search page for meetings to be searched by field invitees

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template
    """
    if current_user==None:
        redirect_url="search_meetings_by_invitees"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse("search_meetings_by_invitees.html", {"request":request})


@meetings_router.post('/search_meetings_by_invitees', response_class=HTMLResponse)
async def post_search(request: Request,sd:SearchData=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    function that takes the form data containing keyword to be searched and calling corrosponding search function from crud

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template and list of search_results
    """
    if current_user==None:
        redirect_url="search_meetings_by_invitees"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
    details = sd.__dict__
    mylogger.info(details)
    search_results = crud.search_meetings_by_invitees(details)
    return templates.TemplateResponse("search_meetings_by_invitees.html", {"request":request, "search_results":search_results})



@meetings_router.get('/search_actions_by_title', response_class=HTMLResponse)
def get_search(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    function that renders search page for actions to be searched by field title

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template
    """
    if current_user==None:
        redirect_url="search_actions_by_title"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse("search_actions_by_title.html", {"request":request})


@meetings_router.post('/search_actions_by_title', response_class=HTMLResponse)
async def post_search(request: Request,sd:SearchData=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    function that takes the form data containing keyword to be searched and calling corrosponding search function from crud

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template and list of search_results
    """
    if current_user==None:
        redirect_url="search_actions_by_title"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
    details = sd.__dict__
    mylogger.info(details)
    search_results = crud.search_actions_by_title(details)
    mylogger.info(search_results)
    return templates.TemplateResponse("search_actions_by_title.html", {"request":request, "search_results":search_results})


@meetings_router.get('/search_actions_by_description', response_class=HTMLResponse)
def get_search(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    function that renders search page for actions to be searched by field description

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template
    """
    if current_user==None:
        redirect_url="search_actions_by_description"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse("search_actions_by_description.html", {"request":request})


@meetings_router.post('/search_actions_by_description', response_class=HTMLResponse)
async def post_search(request: Request,sd:SearchData=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    function that takes the form data containing keyword to be searched and calling corrosponding search function from crud

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template and list of search_results
    """
    if current_user==None:
        redirect_url="search_actions_by_description"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
    details = sd.__dict__
    mylogger.info(details)
    search_results = crud.search_actions_by_description(details)
    return templates.TemplateResponse("search_actions_by_description.html", {"request":request, "search_results":search_results})



@meetings_router.get('/global_search', response_class=HTMLResponse)
def get_search(request: Request, current_user: User = Depends(get_current_active_user)):
    """
    function that renders search page for gloabl searches (all tables)

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in
    
    Return
    -------
    render template
    """
    if current_user==None:
        redirect_url="global_search"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)

    return templates.TemplateResponse("global_search.html", {"request":request})


@meetings_router.post('/global_search', response_class=HTMLResponse)
async def post_search(request: Request,sd:SearchData=Depends(), current_user: User = Depends(get_current_active_user)):
    """
    function that takes the form data containing keyword to be searched and calling corrosponding search function from crud

    Parameter 
    ----------
    request : Request

    current_user : User
            user that is logged in

    Extras: It's implemented with multiprocessing in which more than one processor
    are supported at same time.
    Description:
    1)To import the multiprocessing module, we do:

    import multiprocessing

    2)To create a process, we create an object of Process class. It takes following arguments:

    target: the function to be executed by process
    args: the arguments to be passed to the target function
    
    Return
    -------
    render template and dictionary of search_results from all tables
    """
    if current_user==None:
        redirect_url="global_search"
        return RedirectResponse(url = f"/api/v1/login/?redirect_url={redirect_url}", status_code=status.HTTP_303_SEE_OTHER)
    details = sd.__dict__
    mylogger.info(details)

    manager = multiprocessing.Manager()
    return_dict = manager.dict()

    p1 = multiprocessing.Process(target=crud.global_search_meetings, args=(details,return_dict)) 
    p2 = multiprocessing.Process(target=crud.global_search_actions, args=(details,return_dict)) 
  
    # starting process 1 
    p1.start() 
    # starting process 2 
    p2.start() 
  
    # wait until process 1 is finished 
    p1.join() 
    # wait until process 2 is finished 
    p2.join()

    meeting_results = return_dict["meeting_results"]
    action_results = return_dict["action_results"]

    search_results = {"meeting_results":meeting_results,"action_results":action_results}

    return templates.TemplateResponse("global_search.html", {"request":request, "search_results":search_results})


######################################################################################
##################################### MARKDOWN #######################################
######################################################################################

# Author : Priyav Kaneria


@meetings_router.get('/markdown', response_class=HTMLResponse)
async def markdown_example(request: Request):
    return templates.TemplateResponse('markdown.html',{'request':request})


@meetings_router.post('/markdown', response_class=HTMLResponse)
async def markdown_example_post(request: Request,md:Markdown=Depends()):
    mdfile = md.__dict__
    mdfile = mdfile["md"]
    Markdown = markdown(mdfile)
    return templates.TemplateResponse('markdown.html',{'request':request, 'Markdown':Markdown})


######################################################################################
#################################### ONE DRIVE #######################################
######################################################################################

# Author : Priyav Kaneria

@meetings_router.get("/onedrive_structure_trial")
def create_onedrive_structure(request: Request, current_user: User = Depends(get_current_active_user)):
    access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
    # aztlan_folder_body = {
    #     "name":"Aztlan",
    #     "folder": {},
    #     "@microsoft.graph.conflictbehaviour":"fail",
    # }
    # r = make_api_call('POST','https://graph.microsoft.com/v1.0/me/drive/root/children',access_token,aztlan_folder_body)
    # mylogger.info(r)
    # if "error" in r:
    #     raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=f"Unauthenticated")
    user_pto = crud.get_user_pto(current_user.email)
    meetings_folder_body = {
        "name":"meetings",
        "folder": {},
        "@microsoft.graph.conflictbehaviour":"fail",
    }
    for organization in user_pto["organizations"]:
        org_folder_body = {
            "name":organization,
            "folder": {},
            "@microsoft.graph.conflictbehaviour":"fail",
        }
        ro = make_api_call('POST',f'https://graph.microsoft.com/v1.0/me/drive/root/children',access_token,org_folder_body)
        mylogger.info(ro)
        proj_folder_body = {
            "name":"projects",
            "folder": {},
            "@microsoft.graph.conflictbehaviour":"fail",
        }
        rpf = make_api_call('POST',f'https://graph.microsoft.com/v1.0/me/drive/root:/{organization}:/children',access_token,proj_folder_body)
        mylogger.info(rpf)
        rpm = make_api_call('POST',f'https://graph.microsoft.com/v1.0/me/drive/root:/{organization}/projects:/children',access_token,meetings_folder_body)
        mylogger.info(rpm)
        for project in user_pto["projects"][organization]:
            project_wise_folder_body = {
                "name":project,
                "folder": {},
                "@microsoft.graph.conflictbehaviour":"fail",
            }
            rp = make_api_call('POST',f'https://graph.microsoft.com/v1.0/me/drive/root:/{organization}/projects:/children',access_token,project_wise_folder_body)
            mylogger.info(rp)
        for team in user_pto["teams"][organization]:
            team_folder_body = {
                "name":team,
                "folder": {},
                "@microsoft.graph.conflictbehaviour":"fail",
            }
            rt = make_api_call('POST',f'https://graph.microsoft.com/v1.0/me/drive/root:/{organization}:/children',access_token,team_folder_body)
            mylogger.info(rt)
            rtm = make_api_call('POST',f'https://graph.microsoft.com/v1.0/me/drive/root:/{organization}/{team}:/children',access_token,meetings_folder_body)
            mylogger.info(rtm)
    return "Successful"


@meetings_router.get("/onedrive_file_upload")
def upload_file_to_onedrive(request: Request, current_user: User = Depends(get_current_active_user)):
    access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
    file_body = "The file content has been uploaded"
    r = make_api_call('PUT',f"https://graph.microsoft.com/v1.0/me/drive/root:/test.txt:/content",access_token,file_body)
    mylogger.info(r)
    return "Uploaded"


@meetings_router.get("/onedrive_file_update")
def update_file_in_onedrive(request: Request, current_user: User = Depends(get_current_active_user)):
    access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
    file_body = "The file content has been updated"
    r = make_api_call('PUT',f"https://graph.microsoft.com/v1.0/me/drive/root:/test.txt:/content",access_token,file_body)
    mylogger.info(r)
    return "Updated"


@meetings_router.get("/onedrive_file_delete")
def delete_file_in_onedrive(request: Request, current_user: User = Depends(get_current_active_user)):
    access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
    r = make_api_call('DELETE',f"https://graph.microsoft.com/v1.0/me/drive/root:/test.txt",access_token)
    mylogger.info(r)
    return "Deleted File"


@meetings_router.get("/onedrive_folder_delete")
def delete_folder_in_onedrive(request: Request, current_user: User = Depends(get_current_active_user)):
    access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
    r = make_api_call('DELETE',f"https://graph.microsoft.com/v1.0/me/drive/root:/test",access_token)
    mylogger.info(r)
    return "Deleted Folder"


######################################################################################
################################### GOOGLE DRIVE ######################################
######################################################################################

# Author : Priyav Kaneria


@meetings_router.get("/googledrive_structure_trial")
def create_onedrive_structure(request: Request, current_user: User = Depends(get_current_active_user)):
    service = get_service(current_user.email,"drive")
    # Aztlan_folder_metadata = {
    #     'name': 'Aztlan',
    #     'mimeType': 'application/vnd.google-apps.folder'
    # }
    # Afile = service.files().create(body=Aztlan_folder_metadata,fields='id').execute()
    Meetings_folder_metadata = {
        'name': 'meetings',
        'mimeType': 'application/vnd.google-apps.folder'
    }
    user_pto = crud.get_user_pto(current_user.email)
    for organization in user_pto["organizations"]:
        organization_folder_metadata = {
            'name': organization,
            'mimeType': 'application/vnd.google-apps.folder'
        }
        ofile = service.files().create(body=organization_folder_metadata,fields='id').execute()
        mylogger.info(ofile)
        projects_folder_metadata = {
            'name': "Projects",
            'mimeType': 'application/vnd.google-apps.folder',
            'parents':[ofile.get('id')]
        }
        pfile = service.files().create(body=projects_folder_metadata,fields='id').execute()
        mylogger.info(pfile)
        Meetings_folder_metadata['parents'] = [pfile.get('id')]
        mfile = service.files().create(body=Meetings_folder_metadata,fields='id').execute()
        mylogger.info(mfile)
        for project in user_pto["projects"][organization]:
            project_wise_folder_metadata = {
                'name': project,
                'mimeType': 'application/vnd.google-apps.folder',
                'parents':[pfile.get('id')]
            }
            pwfile = service.files().create(body=project_wise_folder_metadata,fields='id').execute()
            mylogger.info(pwfile)
        for team in user_pto["teams"][organization]:
            team_folder_metadata = {
                'name': team,
                'mimeType': 'application/vnd.google-apps.folder',
                'parents':[ofile.get('id')]
            }
            tfile = service.files().create(body=team_folder_metadata,fields='id').execute()
            mylogger.info(tfile)
            Meetings_folder_metadata['parents'] = [tfile.get('id')]
            tmfile = service.files().create(body=Meetings_folder_metadata,fields='id').execute()
            mylogger.info(tmfile)
    return "Successful"


@meetings_router.get("/googledrive_file_upload")
def upload_file_to_googledrive(request: Request, current_user: User = Depends(get_current_active_user)):
    service = get_service(current_user.email,"drive")
    file_metadata = {
        'name': "File uploaded",
        'mimeType': 'application/vnd.google-apps.document'
    }
    nfile = service.files().create(body=file_metadata,fields='id').execute()
    mylogger.info(nfile)
    return "Uploaded"


# @meetings_router.get("/googledrive_file_update")
# def update_file_in_googledrive(request: Request, current_user: User = Depends(get_current_active_user)):
#     access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
#     file_body = "The file content has been updated"
#     r = make_api_call('PUT',f"https://graph.microsoft.com/v1.0/me/drive/root:/test.txt:/content",access_token,file_body)
#     mylogger.info(r)
#     return "Updated"


@meetings_router.get("/googledrive_file_delete")
def delete_file_in_googledrive(request: Request, current_user: User = Depends(get_current_active_user)):
    service = get_service(current_user.email,"drive")
    nfile = service.files().delete(fileId='1nUN9aBvcOquTgIz6uDS2dRirpWWCymMr_oTVPxgsMv8').execute()
    return "Deleted"


# @meetings_router.get("/googledrive_folder_delete")
# def delete_folder_in_googledrive(request: Request, current_user: User = Depends(get_current_active_user)):
#     access_token = request.cookies.get(COOKIE_AUTHORIZATION_NAME)
#     r = make_api_call('DELETE',f"https://graph.microsoft.com/v1.0/me/drive/root:/test",access_token)
#     mylogger.info(r)
#     return "Deleted Folder"




@meetings_router.post('/contact_us', response_class=HTMLResponse)
async def contact_us_post(request: Request, cu:ContactUs=Depends()):
    data = cu.__dict__
    beta_status = crud.post_contact_us(data)

    if beta_status == True:
        return JSONResponse({
            'status_code': status.HTTP_201_CREATED,
            'detail':'beta request is taken and under process'
            })
    elif beta_status == False:
        return JSONResponse({
            'status_code':status.HTTP_409_CONFLICT,
            'detail':'you are already added to beta program'
            })
    else:
        return JSONResponse({
            'status_code':status.HTTP_405_METHOD_NOT_ALLOWED,
            'detail':'some issue in the data'
            })


@meetings_router.get('/beta-list')
async def get_beta_list(request:Request, current_user: User = Depends(get_current_active_user)):
    '''
    if current_user is None:
        redirect_url = "beta-list"
        return JSONResponse({
                'status_code': status.HTTP_303_SEE_OTHER,
                'url': f"/api/v1/login",
                'redirect_url':f"{redirect_url}"
                })
    '''

    beta_list = crud.get_beta_list()
    return JSONResponse({
        'status_code':status.HTTP_200_OK,
        'data':beta_list
        })

