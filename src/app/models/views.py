# from src import core
# from .src import core
from core.database import conn_db, get_graph
from core.config import Config

config = Config()


db_conn = conn_db(config.MEETINGS_DB_NAME, config.DB_USER, config.DB_PASS)
graph = get_graph(db_conn, config.GRAPH_NAME)


def create_view(view_name, view_type, vertex, fields):
    """
	to create a new view

	Parameters
	----------
	view_name: name of the view
	view_type:type of view
	fields:contains information about collection to which that view is linked and analyzers of the fields
	Returns
	-------
	return the new view that is created
	"""
   
    db_conn.create_view(
        name=view_name,
        view_type=view_type,
        properties={
            "links":{
                vertex:{
                    "includeAllFields":True,
                    "fields":fields["fields"]
               }
            }
        }
        
    )
   
    view = db_conn.view(view_name)
    
    
    


    return db_conn.view(view_name)

def get_view(view_name, view_type, vertex, fields):
    """
	to get an view

	Parameters
	----------
	view_name: name of the view
	view_type: type of view
    fields:contains information about collection to which that view is linked and analyzers of the fields
	
	Returns
	-------
	returns the required view
	"""
    for view in db_conn.views():
        if view["name"] == view_name:
            return db_conn.view(view_name)


    return create_view(view_name, view_type, vertex, fields)



###########################################################################
"""
views
"""
"""
	makes and gets a view

	Parameters
	----------
	view_name: name of the view
	view_type: type of view
    fields:contains information about collection to which that view is linked and analyzers of the fields

	(analyzers:Analyzers parse input values and transform them into sets of sub-values, for example by breaking up text into words.)
	
	Example: 
	String =>This is arango search
	The above string gets breakdown into
	["This","is","arango","search"]

	Link to get more info about arango search: https://www.arangodb.com/docs/stable/arangosearch.html
	"""

meetings_view = get_view("meetings_view", "arangosearch","meetings",{"fields":{"meeting_subject":{"analyzers":["text_en"]},"agenda":{"analyzers":["text_en"]},"invitees":{"analyzers":["text_en"]}}})
print(meetings_view)

actions_view = get_view("actions_view", "arangosearch","action",{"fields":{"title":{"analyzers":["text_en"]},"description":{"analyzers":["text_en"]}}})
print(actions_view)


