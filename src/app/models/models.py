# from src import core
# from .src import core
from core.database import conn_db, get_graph
from core.config import Config

config = Config()


db_conn = conn_db(config.MEETINGS_DB_NAME, config.DB_USER, config.DB_PASS)
graph = get_graph(db_conn, config.GRAPH_NAME)

aql = db_conn.aql


class DuplicateKeyError(Exception):
    pass


def create_vertex(graph, vertex_name):
    """
	to create a new vertex

	Parameters
	----------
	graph: graph name to be connected
	vertex_name:a name to the new vertex
	
	Returns
	-------
	return the new vertex that is created
	"""
    if graph.has_vertex_collection(vertex_name):
        print("duplicate issue.........")
        raise DuplicateKeyError(
            "vertex {} is already present in {} graph.. ".format(vertex_name, graph)
        )
    vertex = graph.create_vertex_collection(vertex_name)

    return vertex


def get_vertex(graph, vertex_name):
    """
	to get the vertex of given name

	Parameters
	----------
	graph: graph name to be connected
	vertex_name:a name to get the required vertex

	Returns
	-------
	returns the required vertex.

	"""
    if graph.has_vertex_collection(vertex_name):
        vertex = graph.vertex_collection(vertex_name)
    else:
        vertex = create_vertex(graph, vertex_name)

    return vertex


def create_edge(graph, edge_name, source, dest):
    """
	to create a new edge
	edge is a table in graph database that connects two vertices.
	edge mandatorily holds from, to fields to make a relation b/w them.
	And also you can many fields that will clearly disntingush and relates the tables.

	Parameters
	----------
	graph: graph name to be connected
	edge_name:a name to the new edge
	source: one of the vertex to connect from
	dest: one of the vertex to connect to
	
	Returns
	-------
	return the new edge that is created
	"""
    if graph.has_edge_definition(edge_name):
        raise DuplicateKeyError(
            "edge {} is already present in {} graph.. ".format(edge_name, graph)
        )

    edge = graph.create_edge_definition(
        edge_collection=edge_name,
        from_vertex_collections=source,
        to_vertex_collections=dest,
    )

    return edge


def get_edge(graph, edge_name, source=[], dest=[]):
    """
	to get an edge

	Parameters
	----------
	graph: graph name to be connected
	edge_name:a name to the new edge
	source: one of the vertex to connect from
	dest: one of the vertex to connect to
	
	Returns
	-------
	returns the required edge
	"""
    if graph.has_edge_definition(edge_name):
        edge = graph.edge_collection(edge_name)
    else:
        edge = create_edge(graph, edge_name, source, dest)

    return edge


# user= create_vertex(graph, 'account')
# if __name__ == "__main__":
def code_check():

    """
	this function will be invoked by Config.py to check all the tables
	are correctly given or not.
	After adding a new vertex or edge, write its get_vertex or get_edge
	function here.. In such case, even if vertex or edge is not present,
	it will directly created new vertex or edge of the given name
	


	vertices
	"""
    user = get_vertex(graph, "account")
    print(user)
    meetings = get_vertex(graph, "meetings")
    print(meetings)
    agenda = get_vertex(graph, "agenda")
    print(agenda)
    orgs = get_vertex(graph, "organization")
    print(orgs)
    user_calendar = get_vertex(graph, "calendar_access")
    print(user_calendar)
    invitations = get_vertex(graph, "invitations")
    print(invitations)
    notifications = get_vertex(graph, "notifications")
    print(notifications)
    missed_notifications = get_vertex(graph, "missed_notifications")
    print(missed_notifications)
    login_devices = get_vertex(graph, "login_devices")
    print(login_devices)
    busy_slots = get_vertex(graph, "busy_slots")
    print(busy_slots)
    # free_slots = get_vertex(graph, "free_slots")
    # print(free_slots)
    active_slots = get_vertex(graph, "active_slots")
    print(active_slots)
    contact_us = get_vertex(graph, "contact_us")
    print(contact_us)
    teams = get_vertex(graph, "teams")
    print(teams)
    event_history = get_vertex(graph, "history")
    print(event_history)
    check_list = get_vertex(graph, "check_list")

    project = get_vertex(graph, "project")
    print(project)
    action = get_vertex(graph, "action")
    print(action)
    task = get_vertex(graph, "task")
    print(task)
    decision = get_vertex(graph, "decision")
    print(decision)

    Files = get_vertex(graph, "Files")
    print(Files)

    ###########################################################################
    """
	edges
	"""
    user_meeting = get_edge(
        graph, "user_meeting", source=["account"], dest=["meetings"]
    )
    print(user_meeting)
    user_org = get_edge(graph, "works_at", source=["account"], dest=["organization"])
    print(user_org)
    org_team = get_edge(graph, "org_team", source=["organization"], dest=["teams"])
    print(org_team)
    user_third_parties = get_edge(graph, "logged_using", source=["account"], dest=["calendar_access"])
    print(user_third_parties)
    user_invitations = get_edge(graph, "user_invitations", source=["account"], dest=["invitations"])
    print(user_invitations)
    contacts = get_edge(graph, "contacts", source=["account"], dest=["account"])
    print(contacts)

    # user_free_slots = get_edge(
    #     graph, "user_free_slots", source=["account"], dest=["free_slots"]
    # )
    user_busy_slots = get_edge(
        graph, "user_busy_slots", source=["account"], dest=["busy_slots"]
    )
    user_active_slots = get_edge(
        graph, "user_active_slots", source=["account"], dest=["active_slots"]
    )
    user_team_edge = get_edge(
        graph, "user_team_edge", source=["account"], dest=["teams"]
    )
    project_team = get_edge(graph, "project_team", source=["project"], dest=["teams"])
    user_project = get_edge(graph, "user_project", source=["account"], dest=["project"])
    user_action = get_edge(graph, "user_action", source=["account"], dest=["action"])
    meeting_action = get_edge(
        graph, "meeting_action", source=["meetings"], dest=["action"]
    )
    user_task = get_edge(graph, "user_task", source=["account"], dest=["task"])
    action_task = get_edge(graph, "action_task", source=["action"], dest=["task"])
    meeting_decision = get_edge(
        graph, "meeting_decision", source=["meetings"], dest=["decision"]
    )
    meeting_agenda = get_edge(graph, "meeting_agenda", source=["meetings"], dest=["agenda"])
    print(meeting_agenda)
    user_check_list = get_edge(
        graph, "user_check_list", source=["account"], dest=["check_list"]
    )

    project_org = get_edge(
        graph,"project_org",source=["project"],dest=["organization"]
    )

    project_members = get_edge(
        graph,"project_members",source=["project"],dest=["account"]
    )

    project_meetings = get_edge(
        graph,"project_meetings",source=["project"],dest=["meetings"]
    )
    teams_meetings = get_edge(
        graph,"teams_meetings",source=["teams"],dest=["meetings"]
    )
    org_meetings = get_edge(
        graph,"org_meetings",source=["organization"],dest=["meetings"]
    )


"""
vertices
"""
user = get_vertex(graph, "account")
print(user)
meetings = get_vertex(graph, "meetings")
print(meetings)
agenda = get_vertex(graph, "agenda")
print(agenda)
orgs = get_vertex(graph, "organization")
print(orgs)
user_calendar = get_vertex(graph, "calendar_access")
print(user_calendar)
invitations = get_vertex(graph, "invitations")
print(invitations)
notifications = get_vertex(graph, "notifications")
print(notifications)
missed_notifications = get_vertex(graph, "missed_notifications")
print(missed_notifications)
login_devices = get_vertex(graph, "login_devices")
print(login_devices)
improve_us = get_vertex(graph, "improve_us")
print(improve_us)
contact_us = get_vertex(graph, "contact_us")
print(contact_us)
teams = get_vertex(graph, "teams")
print(teams)
event_history = get_vertex(graph, "history")
print(event_history)
busy_slots = get_vertex(graph, "busy_slots")
print(busy_slots)
# free_slots = get_vertex(graph, "free_slots")
# print(free_slots)
active_slots = get_vertex(graph, "active_slots")
print(active_slots)
check_list = get_vertex(graph, "check_list")
print(check_list)

project = get_vertex(graph, "project")
print(project)
action = get_vertex(graph, "action")
print(action)
task = get_vertex(graph, "task")
print(task)
decision = get_vertex(graph, "decision")
print(decision)

Files = get_vertex(graph, "Files")
print(Files)

user_creds = get_vertex(graph, 'user_creds')
print(user_creds)


###########################################################################
"""
edges
"""
user_meeting = get_edge(graph, "user_meeting", source=["account"], dest=["meetings"])
print(user_meeting)
user_org = get_edge(graph, "user_org", source=["account"], dest=["organization"])
print(user_org)
org_team = get_edge(graph, "org_team", source=["organization"], dest=["teams"])
print(org_team)
user_third_parties = get_edge(graph, "user_third_parties", source=["account"], dest=["calendar_access"])
print(user_third_parties)
user_invitations = get_edge(graph, "user_invitations", source=["account"], dest=["invitations"])
print(user_invitations)
contacts = get_edge(graph, "contacts", source=["account"], dest=["account"])
print(contacts)
user_improve_us = get_edge(
    graph, "user_improve_us", source=["account"], dest=["improve_us"]
)
user_team_edge = get_edge(graph, "user_team_edge", source=["account"], dest=["teams"])
user_busy_slots = get_edge(
    graph, "user_busy_slots", source=["account"], dest=["busy_slots"]
)
user_active_slots = get_edge(
    graph, "user_active_slots", source=["account"], dest=["active_slots"]
)
user_check_list = get_edge(
    graph, "user_check_list", source=["account"], dest=["check_list"]
)
user_team_edge = get_edge(graph, "user_team_edge", source=["account"], dest=["teams"])
print(user_team_edge)
project_team = get_edge(graph, "project_team", source=["project"], dest=["teams"])
print(project_team)
user_project = get_edge(graph, "user_project", source=["account"], dest=["project"])
print(user_project)
user_action = get_edge(graph, "user_action", source=["account"], dest=["action"])
print(user_action)
user_task = get_edge(graph, "user_task", source=["account"], dest=["task"])
print(user_task)
meeting_action = get_edge(graph, "meeting_action", source=["meetings"], dest=["action"])
print(meeting_action)
action_task = get_edge(graph, "action_task", source=["action"], dest=["task"])
print(action_task)
meeting_decision = get_edge(graph, "meeting_decision", source=["meetings"], dest=["decision"])
print(meeting_decision)
meeting_agenda = get_edge(graph, "meeting_agenda", source=["meetings"], dest=["agenda"])
print(meeting_agenda)
project_org = get_edge(graph,"project_org",source=["project"],dest=["organization"])
print(project_org)
project_members = get_edge(graph,"project_members",source=["project"],dest=["account"])
print(project_members)
project_meetings = get_edge(graph,"project_meetings",source=["project"],dest=["meetings"])
print(project_meetings)
org_meetings = get_edge(graph,"org_meetings",source=["organization"],dest=["meetings"])
print(org_meetings)
teams_meetings = get_edge(graph,"teams_meetings",source=["teams"],dest=["meetings"])
print(teams_meetings)
