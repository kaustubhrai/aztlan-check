function initiate_notifications(user,instanceid,organization) {
    console.log(instanceid);
    const beamsClient = new PusherPushNotifications.Client({
        instanceId: instanceid,
    });
    const beamsTokenProvider = new PusherPushNotifications.TokenProvider({
    url: '/api/v1/pusher/beams-auth',
    });
    beamsClient.getUserId()
        .then(userId => {
            // Check if the Beams user matches the user that is currently logged in
            console.log(userId);
            if(userId == null){
                beamsClient
                    .start()
                    .then(() => {beamsClient.setUserId(user, beamsTokenProvider);if(organization!="") beamsClient.addDeviceInterest(organization);})
                    .catch(console.error);
                console.log("User registered to beams and organization");
            }
            else{
                console.log("User already registered");
                if (userId != user) {
                    // Unregister for notifications
                    console.log("Client stopped");
                    beamsClient.stop();
                    beamsClient
                        .start()
                        .then(() => {beamsClient.setUserId(user, beamsTokenProvider);if(organization!="") beamsClient.addDeviceInterest(organization);})
                        .catch(console.error);
                    console.log("User registered to beams and organization");
                }
            }
        })
    console.log("bye");
}