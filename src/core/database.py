from core.config import Config
from arango import ArangoClient


config = Config()

db_host = config.DB_HOST
client = ArangoClient(hosts=db_host)

#general_db = client.db(config.HOME_DB_NAME,username=config.DB_USER,password=config.DB_PASS)

#meetings_db

'''
connects to the main datbase '_system'
'''
def conn_main_db(username, password, default_db = config.HOME_DB_NAME):
	sys_db = client.db(default_db,username=username,password=password)

	return sys_db

'''
connects to the database that has to be accessed other than main '_system' database
'''
def conn_db(db_name, username, password, default_db = config.HOME_DB_NAME, admin_access = False):


	sys_db = conn_main_db(username, password)
	if sys_db is not None:
		if not sys_db.has_database(db_name):
			sys_db.create_database(db_name)

	meetings_db = client.db(db_name, username, password)

	if admin_access == True:
		return sys_db, meetings_db

	return meetings_db



'''
using the above database connections...
we will connect to the graph required for the implementation
'''
def get_graph(conn, graph_name):
	if not conn.has_graph(graph_name):
		meetings = conn.create_graph(graph_name)
		#print(False)
	else:
		meetings = conn.graph(graph_name)
		#print(True)
	return meetings



