import os
from dataclasses import dataclass
from dotenv import load_dotenv, dotenv_values
from arango import ArangoClient



BASE_DIR = os.path.dirname(os.path.abspath(__file__))
load_dotenv(os.path.join(BASE_DIR, "constants.env"))
print(dotenv_values(os.path.join(BASE_DIR, "constants.env")))

@dataclass(frozen=True)
class Config:
    ENV: str = os.getenv('ENV')
    DEBUG: bool = os.getenv('DEBUG')
    APP_HOST: str = os.getenv('APP_HOST')
    APP_PORT: str = os.getenv('APP_PORT')
    DB_USER: str = os.getenv('DB_USER')
    DB_PASS: str = os.getenv('DB_PASS')
    DB_HOST: str = os.getenv('DB_HOST')
    HOME_DB_NAME: str = os.getenv('HOME_DB_NAME')
    MEETINGS_DB_NAME: str = os.getenv('MEETINGS_DB_NAME')
    GRAPH_NAME: str = os.getenv('GRAPH_NAME')
    # DB_PASSWORD: str = os.getenv('DB_PASSWORD')
    #DB_URL: str = f'postgresql+psycopg2://{DB_USER}:{DB_PASS}@{DB_HOST}:5432/{DB_NAME}'
    JWT_SECRET_KEY = os.getenv('JWT_SECRET_KEY')
    JWT_ALGORITHM = 'HS256'
    SENTRY_SDN: str = os.getenv('SENTRY_DSN')


@dataclass(frozen=True)
class DevelopmentConfig(Config):
    DEBUG = True


@dataclass(frozen=True)
class TestingConfig(Config):
    DEBUG = True


@dataclass(frozen=True)
class ProductionConfig(Config):
    DEBUG = False


def get_config():
    env = os.getenv('ENV', 'development')
    config_type = {
        'development': DevelopmentConfig,
        'testing': TestingConfig,
        'production': ProductionConfig
    }
    return config_type[env]
